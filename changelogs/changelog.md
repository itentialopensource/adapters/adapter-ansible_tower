
## 2.1.2 [04-06-2023]

* updated adapter-utils version

See merge request itentialopensource/adapters/devops-netops/adapter-ansible_tower!5

---

## 2.1.1 [03-24-2023]

* updated adapter-utils version

See merge request itentialopensource/adapters/devops-netops/adapter-ansible_tower!5

---

## 2.1.0 [05-22-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/devops-netops/adapter-ansible_tower!4

---

## 2.0.4 [02-26-2021]

- Migration to bring up to the latest foundation
  - Change to .eslintignore (adapter_modification directory)
  - Change to README.md (new properties, new scripts, new processes)
  - Changes to adapterBase.js (new methods)
  - Changes to package.json (new scripts, dependencies)
  - Changes to propertiesSchema.json (new properties and changes to existing)
  - Changes to the Unit test
  - Adding several test files, utils files and .generic entity
  - Fix order of scripts and dependencies in package.json
  - Fix order of properties in propertiesSchema.json
  - Update sampleProperties, unit and integration tests to have all new properties.
  - Add all new calls to adapter.js and pronghorn.json
  - Add suspend piece to older methods

See merge request itentialopensource/adapters/devops-netops/adapter-ansible_tower!3

---

## 2.0.3 [07-03-2020]

- Update the adapter to the latest foundation

See merge request itentialopensource/adapters/devops-netops/adapter-ansible_tower!2

---

## 2.0.2 [12-31-2019]

- Update the adapter to the latest adapter foundation

See merge request itentialopensource/adapters/devops-netops/adapter-ansible_tower!1

---

## 2.0.1 [11-26-2019]

- Initial Commit

See commit 3d13877

---
