# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Ansible_tower System. The API that was used to build the adapter for Ansible_tower is usually available in the report directory of this adapter. The adapter utilizes the Ansible_tower API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Ansible Tower adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Red Hat Ansible Tower to enable automation and integration with other tools and systems

With this adapter you have the ability to perform operations with Ansible Tower such as:

- Get Job Template
- Launch Job Template
- Get Job
- Cancel Job
- Create Job Schedule
- Cancel Job Schedule

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
