
## 2.3.6 [11-21-2024]

* Add methods with iapMetadata

See merge request itentialopensource/adapters/adapter-ansible_tower!19

---

## 2.3.5 [10-15-2024]

* Changes made at 2024.10.14_21:22PM

See merge request itentialopensource/adapters/adapter-ansible_tower!18

---

## 2.3.4 [08-22-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-ansible_tower!16

---

## 2.3.3 [08-14-2024]

* Changes made at 2024.08.14_19:39PM

See merge request itentialopensource/adapters/adapter-ansible_tower!15

---

## 2.3.2 [08-07-2024]

* Changes made at 2024.08.06_21:43PM

See merge request itentialopensource/adapters/adapter-ansible_tower!14

---

## 2.3.1 [08-06-2024]

* Changes made at 2024.08.06_09:57AM

See merge request itentialopensource/adapters/adapter-ansible_tower!13

---

## 2.3.0 [05-09-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/devops-netops/adapter-ansible_tower!12

---

## 2.2.3 [03-26-2024]

* Changes made at 2024.03.26_14:27PM

See merge request itentialopensource/adapters/devops-netops/adapter-ansible_tower!11

---

## 2.2.2 [03-11-2024]

* Changes made at 2024.03.11_13:24PM

See merge request itentialopensource/adapters/devops-netops/adapter-ansible_tower!10

---

## 2.2.1 [02-26-2024]

* Changes made at 2024.02.26_13:50PM

See merge request itentialopensource/adapters/devops-netops/adapter-ansible_tower!9

---

## 2.2.0 [12-29-2023]

* Adapter Migration

See merge request itentialopensource/adapters/devops-netops/adapter-ansible_tower!8

---

## 2.1.2 [04-06-2023]

* updated adapter-utils version

See merge request itentialopensource/adapters/devops-netops/adapter-ansible_tower!5

---

## 2.1.1 [03-24-2023]

* updated adapter-utils version

See merge request itentialopensource/adapters/devops-netops/adapter-ansible_tower!5

---

## 2.1.0 [05-22-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/devops-netops/adapter-ansible_tower!4

---

## 2.0.4 [02-26-2021]

- Migration to bring up to the latest foundation
  - Change to .eslintignore (adapter_modification directory)
  - Change to README.md (new properties, new scripts, new processes)
  - Changes to adapterBase.js (new methods)
  - Changes to package.json (new scripts, dependencies)
  - Changes to propertiesSchema.json (new properties and changes to existing)
  - Changes to the Unit test
  - Adding several test files, utils files and .generic entity
  - Fix order of scripts and dependencies in package.json
  - Fix order of properties in propertiesSchema.json
  - Update sampleProperties, unit and integration tests to have all new properties.
  - Add all new calls to adapter.js and pronghorn.json
  - Add suspend piece to older methods

See merge request itentialopensource/adapters/devops-netops/adapter-ansible_tower!3

---

## 2.0.3 [07-03-2020]

- Update the adapter to the latest foundation

See merge request itentialopensource/adapters/devops-netops/adapter-ansible_tower!2

---

## 2.0.2 [12-31-2019]

- Update the adapter to the latest adapter foundation

See merge request itentialopensource/adapters/devops-netops/adapter-ansible_tower!1

---

## 2.0.1 [11-26-2019]

- Initial Commit

See commit 3d13877

---
