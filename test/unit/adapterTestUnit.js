/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint global-require: warn */
/* eslint no-unused-vars: warn */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const path = require('path');
const util = require('util');
const execute = require('child_process').execSync;
const fs = require('fs-extra');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');
const Ajv = require('ajv');

const ajv = new Ajv({ strictSchema: false, allErrors: true, allowUnionTypes: true });
const anything = td.matchers.anything();
let logLevel = 'none';
const isRapidFail = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;
samProps.host = 'replace.hostorip.here';
samProps.authentication.username = 'username';
samProps.authentication.password = 'password';
samProps.protocol = 'http';
samProps.port = 80;
samProps.ssl.enabled = false;
samProps.ssl.accept_invalid_cert = false;
samProps.request.attempt_timeout = 1200000;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-ansible_tower',
      type: 'AnsibleTower',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

// require the adapter that we are going to be using
const AnsibleTower = require('../../adapter');

// delete the .DS_Store directory in entities -- otherwise this will cause errors
const dirPath = path.join(__dirname, '../../entities/.DS_Store');
if (fs.existsSync(dirPath)) {
  try {
    fs.removeSync(dirPath);
    console.log('.DS_Store deleted');
  } catch (e) {
    console.log('Error when deleting .DS_Store:', e);
  }
}

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[unit] Ansible_tower Adapter Test', () => {
  describe('AnsibleTower Class Tests', () => {
    const a = new AnsibleTower(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('adapterBase.js', () => {
      it('should have an adapterBase.js', (done) => {
        try {
          fs.exists('adapterBase.js', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    let wffunctions = [];
    describe('#iapGetAdapterWorkflowFunctions', () => {
      it('should retrieve workflow functions', (done) => {
        try {
          wffunctions = a.iapGetAdapterWorkflowFunctions([]);

          try {
            assert.notEqual(0, wffunctions.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('package.json', () => {
      it('should have a package.json', (done) => {
        try {
          fs.exists('package.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json should be validated', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          // Define the JSON schema for package.json
          const packageJsonSchema = {
            type: 'object',
            properties: {
              name: { type: 'string' },
              version: { type: 'string' }
              // May need to add more properties as needed
            },
            required: ['name', 'version']
          };
          const validate = ajv.compile(packageJsonSchema);
          const isValid = validate(packageDotJson);

          if (isValid === false) {
            log.error('The package.json contains errors');
            assert.equal(true, isValid);
          } else {
            assert.equal(true, isValid);
          }

          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json standard fields should be customized', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(-1, packageDotJson.name.indexOf('ansible_tower'));
          assert.notEqual(undefined, packageDotJson.version);
          assert.notEqual(null, packageDotJson.version);
          assert.notEqual('', packageDotJson.version);
          assert.notEqual(undefined, packageDotJson.description);
          assert.notEqual(null, packageDotJson.description);
          assert.notEqual('', packageDotJson.description);
          assert.equal('adapter.js', packageDotJson.main);
          assert.notEqual(undefined, packageDotJson.wizardVersion);
          assert.notEqual(null, packageDotJson.wizardVersion);
          assert.notEqual('', packageDotJson.wizardVersion);
          assert.notEqual(undefined, packageDotJson.engineVersion);
          assert.notEqual(null, packageDotJson.engineVersion);
          assert.notEqual('', packageDotJson.engineVersion);
          assert.equal('http', packageDotJson.adapterType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper scripts should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.scripts);
          assert.notEqual(null, packageDotJson.scripts);
          assert.notEqual('', packageDotJson.scripts);
          assert.equal('node utils/setup.js', packageDotJson.scripts.preinstall);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js', packageDotJson.scripts.lint);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js --quiet', packageDotJson.scripts['lint:errors']);
          assert.equal('mocha test/unit/adapterBaseTestUnit.js --LOG=error', packageDotJson.scripts['test:baseunit']);
          assert.equal('mocha test/unit/adapterTestUnit.js --LOG=error', packageDotJson.scripts['test:unit']);
          assert.equal('mocha test/integration/adapterTestIntegration.js --LOG=error', packageDotJson.scripts['test:integration']);
          assert.equal('npm run test:baseunit && npm run test:unit && npm run test:integration', packageDotJson.scripts.test);
          assert.equal('npm publish --registry=https://registry.npmjs.org --access=public', packageDotJson.scripts.deploy);
          assert.equal('npm run deploy', packageDotJson.scripts.build);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper directories should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.repository);
          assert.notEqual(null, packageDotJson.repository);
          assert.notEqual('', packageDotJson.repository);
          assert.equal('git', packageDotJson.repository.type);
          assert.equal('git@gitlab.com:itentialopensource/adapters/', packageDotJson.repository.url.substring(0, 43));
          assert.equal('https://gitlab.com/itentialopensource/adapters/', packageDotJson.homepage.substring(0, 47));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.dependencies);
          assert.notEqual(null, packageDotJson.dependencies);
          assert.notEqual('', packageDotJson.dependencies);
          assert.equal('^8.17.1', packageDotJson.dependencies.ajv);
          assert.equal('^1.7.9', packageDotJson.dependencies.axios);
          assert.equal('^11.0.0', packageDotJson.dependencies.commander);
          assert.equal('^11.2.0', packageDotJson.dependencies['fs-extra']);
          assert.equal('^10.8.2', packageDotJson.dependencies.mocha);
          assert.equal('^2.0.1', packageDotJson.dependencies['mocha-param']);
          assert.equal('^0.4.4', packageDotJson.dependencies.ping);
          assert.equal('^1.4.10', packageDotJson.dependencies['readline-sync']);
          assert.equal('^7.6.3', packageDotJson.dependencies.semver);
          assert.equal('^3.17.0', packageDotJson.dependencies.winston);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dev dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.devDependencies);
          assert.notEqual(null, packageDotJson.devDependencies);
          assert.notEqual('', packageDotJson.devDependencies);
          assert.equal('^4.3.7', packageDotJson.devDependencies.chai);
          assert.equal('^8.44.0', packageDotJson.devDependencies.eslint);
          assert.equal('^15.0.0', packageDotJson.devDependencies['eslint-config-airbnb-base']);
          assert.equal('^2.27.5', packageDotJson.devDependencies['eslint-plugin-import']);
          assert.equal('^3.1.0', packageDotJson.devDependencies['eslint-plugin-json']);
          assert.equal('^3.18.0', packageDotJson.devDependencies.testdouble);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('pronghorn.json', () => {
      it('should have a pronghorn.json', (done) => {
        try {
          fs.exists('pronghorn.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should be customized', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(-1, pronghornDotJson.id.indexOf('ansible_tower'));
          assert.equal('Adapter', pronghornDotJson.type);
          assert.equal('AnsibleTower', pronghornDotJson.export);
          assert.equal('Ansible_tower', pronghornDotJson.title);
          assert.equal('adapter.js', pronghornDotJson.src);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should contain generic adapter methods', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(undefined, pronghornDotJson.methods);
          assert.notEqual(null, pronghornDotJson.methods);
          assert.notEqual('', pronghornDotJson.methods);
          assert.equal(true, Array.isArray(pronghornDotJson.methods));
          assert.notEqual(0, pronghornDotJson.methods.length);
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUpdateAdapterConfiguration'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapSuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUnsuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterQueue'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapFindAdapterPath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapTroubleshootAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterHealthcheck'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterConnectivity'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterBasicGet'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapMoveAdapterEntitiesToDB'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapDeactivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapActivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapPopulateEntityCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRetrieveEntitiesCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevice'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevicesFiltered'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'isAlive'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getConfig'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetDeviceCount'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapExpandedGenericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequestNoBasePath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterLint'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterTests'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterInventory'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should only expose workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');

          for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
            let found = false;
            let paramissue = false;

            for (let w = 0; w < wffunctions.length; w += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                const methLine = execute(`grep "  ${wffunctions[w]}(" adapter.js | grep "callback) {"`).toString();
                let wfparams = [];

                if (methLine && methLine.indexOf('(') >= 0 && methLine.indexOf(')') >= 0) {
                  const temp = methLine.substring(methLine.indexOf('(') + 1, methLine.lastIndexOf(')'));
                  wfparams = temp.split(',');

                  for (let t = 0; t < wfparams.length; t += 1) {
                    // remove default value from the parameter name
                    wfparams[t] = wfparams[t].substring(0, wfparams[t].search(/=/) > 0 ? wfparams[t].search(/#|\?|=/) : wfparams[t].length);
                    // remove spaces
                    wfparams[t] = wfparams[t].trim();

                    if (wfparams[t] === 'callback') {
                      wfparams.splice(t, 1);
                    }
                  }
                }

                // if there are inputs defined but not on the method line
                if (wfparams.length === 0 && (pronghornDotJson.methods[m].input
                    && pronghornDotJson.methods[m].input.length > 0)) {
                  paramissue = true;
                } else if (wfparams.length > 0 && (!pronghornDotJson.methods[m].input
                    || pronghornDotJson.methods[m].input.length === 0)) {
                  // if there are no inputs defined but there are on the method line
                  paramissue = true;
                } else {
                  for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                    let pfound = false;
                    for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                  for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                    let pfound = false;
                    for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                }

                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} not found in workflow functions`);
            }
            if (paramissue) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} has a parameter mismatch`);
            }
            assert.equal(true, found);
            assert.equal(false, paramissue);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('pronghorn.json should expose all workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          for (let w = 0; w < wffunctions.length; w += 1) {
            let found = false;

            for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${wffunctions[w]} not found in pronghorn.json`);
            }
            assert.equal(true, found);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json verify input/output schema objects', (done) => {
        const verifySchema = (methodName, schema) => {
          try {
            ajv.compile(schema);
          } catch (error) {
            const errorMessage = `Invalid schema found in '${methodName}' method.
          Schema => ${JSON.stringify(schema)}.
          Details => ${error.message}`;
            throw new Error(errorMessage);
          }
        };

        try {
          const pronghornDotJson = require('../../pronghorn.json');
          const { methods } = pronghornDotJson;
          for (let i = 0; i < methods.length; i += 1) {
            for (let j = 0; j < methods[i].input.length; j += 1) {
              const inputSchema = methods[i].input[j].schema;
              if (inputSchema) {
                verifySchema(methods[i].name, inputSchema);
              }
            }
            const outputSchema = methods[i].output.schema;
            if (outputSchema) {
              verifySchema(methods[i].name, outputSchema);
            }
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('propertiesSchema.json', () => {
      it('should have a propertiesSchema.json', (done) => {
        try {
          fs.exists('propertiesSchema.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should be customized', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.equal('adapter-ansible_tower', propertiesDotJson.$id);
          assert.equal('object', propertiesDotJson.type);
          assert.equal('http://json-schema.org/draft-07/schema#', propertiesDotJson.$schema);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should contain generic adapter properties', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.notEqual(undefined, propertiesDotJson.properties);
          assert.notEqual(null, propertiesDotJson.properties);
          assert.notEqual('', propertiesDotJson.properties);
          assert.equal('string', propertiesDotJson.properties.host.type);
          assert.equal('integer', propertiesDotJson.properties.port.type);
          assert.equal('boolean', propertiesDotJson.properties.stub.type);
          assert.equal('string', propertiesDotJson.properties.protocol.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.authentication);
          assert.notEqual(null, propertiesDotJson.definitions.authentication);
          assert.notEqual('', propertiesDotJson.definitions.authentication);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.auth_method.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.invalid_token_error.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.token_timeout.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token_cache.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field.type));
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field_format.type));
          assert.equal('boolean', propertiesDotJson.definitions.authentication.properties.auth_logging.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_id.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_secret.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.grant_type.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.ssl);
          assert.notEqual(null, propertiesDotJson.definitions.ssl);
          assert.notEqual('', propertiesDotJson.definitions.ssl);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ecdhCurve.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.cert_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.secure_protocol.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ciphers.type);
          assert.equal('string', propertiesDotJson.properties.base_path.type);
          assert.equal('string', propertiesDotJson.properties.version.type);
          assert.equal('string', propertiesDotJson.properties.cache_location.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_pathvars.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_queryvars.type);
          assert.equal(true, Array.isArray(propertiesDotJson.properties.save_metric.type));
          assert.notEqual(undefined, propertiesDotJson.definitions);
          assert.notEqual(null, propertiesDotJson.definitions);
          assert.notEqual('', propertiesDotJson.definitions);
          assert.notEqual(undefined, propertiesDotJson.definitions.healthcheck);
          assert.notEqual(null, propertiesDotJson.definitions.healthcheck);
          assert.notEqual('', propertiesDotJson.definitions.healthcheck);
          assert.equal('string', propertiesDotJson.definitions.healthcheck.properties.type.type);
          assert.equal('integer', propertiesDotJson.definitions.healthcheck.properties.frequency.type);
          assert.equal('object', propertiesDotJson.definitions.healthcheck.properties.query_object.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.throttle);
          assert.notEqual(null, propertiesDotJson.definitions.throttle);
          assert.notEqual('', propertiesDotJson.definitions.throttle);
          assert.equal('boolean', propertiesDotJson.definitions.throttle.properties.throttle_enabled.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.number_pronghorns.type);
          assert.equal('string', propertiesDotJson.definitions.throttle.properties.sync_async.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.max_in_queue.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.concurrent_max.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.expire_timeout.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.avg_runtime.type);
          assert.equal('array', propertiesDotJson.definitions.throttle.properties.priorities.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.request);
          assert.notEqual(null, propertiesDotJson.definitions.request);
          assert.notEqual('', propertiesDotJson.definitions.request);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_redirects.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_retries.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.request.properties.limit_retry_error.type));
          assert.equal('array', propertiesDotJson.definitions.request.properties.failover_codes.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.attempt_timeout.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.payload.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.uriOptions.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.addlHeaders.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.authData.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.healthcheck_on_timeout.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_raw.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.archiving.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_request.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.proxy);
          assert.notEqual(null, propertiesDotJson.definitions.proxy);
          assert.notEqual('', propertiesDotJson.definitions.proxy);
          assert.equal('boolean', propertiesDotJson.definitions.proxy.properties.enabled.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.proxy.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.protocol.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.password.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.mongo);
          assert.notEqual(null, propertiesDotJson.definitions.mongo);
          assert.notEqual('', propertiesDotJson.definitions.mongo);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.mongo.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.database.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.replSet.type);
          assert.equal('object', propertiesDotJson.definitions.mongo.properties.db_ssl.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.cert_file.type);
          assert.notEqual('', propertiesDotJson.definitions.devicebroker);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevice.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevicesFiltered.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.isAlive.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getConfig.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getCount.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('error.json', () => {
      it('should have an error.json', (done) => {
        try {
          fs.exists('error.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error.json should have standard adapter errors', (done) => {
        try {
          const errorDotJson = require('../../error.json');
          assert.notEqual(undefined, errorDotJson.errors);
          assert.notEqual(null, errorDotJson.errors);
          assert.notEqual('', errorDotJson.errors);
          assert.equal(true, Array.isArray(errorDotJson.errors));
          assert.notEqual(0, errorDotJson.errors.length);
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.100'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.101'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.102'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.110'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.111'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.112'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.113'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.114'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.115'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.116'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.300'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.301'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.302'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.303'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.304'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.305'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.310'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.311'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.312'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.320'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.321'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.400'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.401'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.402'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.500'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.501'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.502'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.503'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.600'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.900'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('sampleProperties.json', () => {
      it('should have a sampleProperties.json', (done) => {
        try {
          fs.exists('sampleProperties.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('sampleProperties.json should contain generic adapter properties', (done) => {
        try {
          const sampleDotJson = require('../../sampleProperties.json');
          assert.notEqual(-1, sampleDotJson.id.indexOf('ansible_tower'));
          assert.equal('AnsibleTower', sampleDotJson.type);
          assert.notEqual(undefined, sampleDotJson.properties);
          assert.notEqual(null, sampleDotJson.properties);
          assert.notEqual('', sampleDotJson.properties);
          assert.notEqual(undefined, sampleDotJson.properties.host);
          assert.notEqual(undefined, sampleDotJson.properties.port);
          assert.notEqual(undefined, sampleDotJson.properties.stub);
          assert.notEqual(undefined, sampleDotJson.properties.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.authentication);
          assert.notEqual(null, sampleDotJson.properties.authentication);
          assert.notEqual('', sampleDotJson.properties.authentication);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_method);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.username);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.password);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.invalid_token_error);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_cache);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field_format);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_logging);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_id);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_secret);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.grant_type);
          assert.notEqual(undefined, sampleDotJson.properties.ssl);
          assert.notEqual(null, sampleDotJson.properties.ssl);
          assert.notEqual('', sampleDotJson.properties.ssl);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ecdhCurve);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.secure_protocol);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ciphers);
          assert.notEqual(undefined, sampleDotJson.properties.base_path);
          assert.notEqual(undefined, sampleDotJson.properties.version);
          assert.notEqual(undefined, sampleDotJson.properties.cache_location);
          assert.notEqual(undefined, sampleDotJson.properties.encode_pathvars);
          assert.notEqual(undefined, sampleDotJson.properties.encode_queryvars);
          assert.notEqual(undefined, sampleDotJson.properties.save_metric);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck);
          assert.notEqual(null, sampleDotJson.properties.healthcheck);
          assert.notEqual('', sampleDotJson.properties.healthcheck);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.type);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.frequency);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.query_object);
          assert.notEqual(undefined, sampleDotJson.properties.throttle);
          assert.notEqual(null, sampleDotJson.properties.throttle);
          assert.notEqual('', sampleDotJson.properties.throttle);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.throttle_enabled);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.number_pronghorns);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.sync_async);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.max_in_queue);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.concurrent_max);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.expire_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.avg_runtime);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.priorities);
          assert.notEqual(undefined, sampleDotJson.properties.request);
          assert.notEqual(null, sampleDotJson.properties.request);
          assert.notEqual('', sampleDotJson.properties.request);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_redirects);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_retries);
          assert.notEqual(undefined, sampleDotJson.properties.request.limit_retry_error);
          assert.notEqual(undefined, sampleDotJson.properties.request.failover_codes);
          assert.notEqual(undefined, sampleDotJson.properties.request.attempt_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.payload);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.uriOptions);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.addlHeaders);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.authData);
          assert.notEqual(undefined, sampleDotJson.properties.request.healthcheck_on_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_raw);
          assert.notEqual(undefined, sampleDotJson.properties.request.archiving);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_request);
          assert.notEqual(undefined, sampleDotJson.properties.proxy);
          assert.notEqual(null, sampleDotJson.properties.proxy);
          assert.notEqual('', sampleDotJson.properties.proxy);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.host);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.port);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.username);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo);
          assert.notEqual(null, sampleDotJson.properties.mongo);
          assert.notEqual('', sampleDotJson.properties.mongo);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.host);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.port);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.database);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.username);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.replSet);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevice);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevicesFiltered);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.isAlive);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getConfig);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getCount);
          assert.notEqual(undefined, sampleDotJson.properties.cache);
          assert.notEqual(undefined, sampleDotJson.properties.cache.entities);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkProperties', () => {
      it('should have a checkProperties function', (done) => {
        try {
          assert.equal(true, typeof a.checkProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the sample properties should be good - if failure change the log level', (done) => {
        try {
          const samplePropsJson = require('../../sampleProperties.json');
          const clean = a.checkProperties(samplePropsJson.properties);

          try {
            assert.notEqual(0, Object.keys(clean));
            assert.equal(undefined, clean.exception);
            assert.notEqual(undefined, clean.host);
            assert.notEqual(null, clean.host);
            assert.notEqual('', clean.host);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('README.md', () => {
      it('should have a README', (done) => {
        try {
          fs.exists('README.md', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('README.md should be customized', (done) => {
        try {
          fs.readFile('README.md', 'utf8', (err, data) => {
            assert.equal(-1, data.indexOf('[System]'));
            assert.equal(-1, data.indexOf('[system]'));
            assert.equal(-1, data.indexOf('[version]'));
            assert.equal(-1, data.indexOf('[namespace]'));
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#connect', () => {
      it('should have a connect function', (done) => {
        try {
          assert.equal(true, typeof a.connect === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should have a healthCheck function', (done) => {
        try {
          assert.equal(true, typeof a.healthCheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUpdateAdapterConfiguration', () => {
      it('should have a iapUpdateAdapterConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.iapUpdateAdapterConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapSuspendAdapter', () => {
      it('should have a iapSuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapSuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUnsuspendAdapter', () => {
      it('should have a iapUnsuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapUnsuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterQueue', () => {
      it('should have a iapGetAdapterQueue function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterQueue === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapFindAdapterPath', () => {
      it('should have a iapFindAdapterPath function', (done) => {
        try {
          assert.equal(true, typeof a.iapFindAdapterPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('iapFindAdapterPath should find atleast one path that matches', (done) => {
        try {
          a.iapFindAdapterPath('{base_path}/{version}', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.equal(true, data.found);
              assert.notEqual(undefined, data.foundIn);
              assert.notEqual(null, data.foundIn);
              assert.notEqual(0, data.foundIn.length);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapTroubleshootAdapter', () => {
      it('should have a iapTroubleshootAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapTroubleshootAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterHealthcheck', () => {
      it('should have a iapRunAdapterHealthcheck function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterHealthcheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterConnectivity', () => {
      it('should have a iapRunAdapterConnectivity function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterConnectivity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterBasicGet', () => {
      it('should have a iapRunAdapterBasicGet function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterBasicGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapMoveAdapterEntitiesToDB', () => {
      it('should have a iapMoveAdapterEntitiesToDB function', (done) => {
        try {
          assert.equal(true, typeof a.iapMoveAdapterEntitiesToDB === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkActionFiles', () => {
      it('should have a checkActionFiles function', (done) => {
        try {
          assert.equal(true, typeof a.checkActionFiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the action files should be good - if failure change the log level as most issues are warnings', (done) => {
        try {
          const clean = a.checkActionFiles();

          try {
            for (let c = 0; c < clean.length; c += 1) {
              log.error(clean[c]);
            }
            assert.equal(0, clean.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#encryptProperty', () => {
      it('should have a encryptProperty function', (done) => {
        try {
          assert.equal(true, typeof a.encryptProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should get base64 encoded property', (done) => {
        try {
          a.encryptProperty('testing', 'base64', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{code}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should get encrypted property', (done) => {
        try {
          a.encryptProperty('testing', 'encrypt', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{crypt}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapDeactivateTasks', () => {
      it('should have a iapDeactivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapDeactivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapActivateTasks', () => {
      it('should have a iapActivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapActivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapPopulateEntityCache', () => {
      it('should have a iapPopulateEntityCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapPopulateEntityCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRetrieveEntitiesCache', () => {
      it('should have a iapRetrieveEntitiesCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapRetrieveEntitiesCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#hasEntities', () => {
      it('should have a hasEntities function', (done) => {
        try {
          assert.equal(true, typeof a.hasEntities === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevice', () => {
      it('should have a getDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevicesFiltered', () => {
      it('should have a getDevicesFiltered function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicesFiltered === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#isAlive', () => {
      it('should have a isAlive function', (done) => {
        try {
          assert.equal(true, typeof a.isAlive === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getConfig', () => {
      it('should have a getConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetDeviceCount', () => {
      it('should have a iapGetDeviceCount function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetDeviceCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapExpandedGenericAdapterRequest', () => {
      it('should have a iapExpandedGenericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.iapExpandedGenericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequest', () => {
      it('should have a genericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequestNoBasePath', () => {
      it('should have a genericAdapterRequestNoBasePath function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequestNoBasePath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterLint', () => {
      it('should have a iapRunAdapterLint function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterLint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the lint results', (done) => {
        try {
          a.iapRunAdapterLint((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.status);
              assert.notEqual(null, data.status);
              assert.equal('SUCCESS', data.status);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRunAdapterTests', () => {
      it('should have a iapRunAdapterTests function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterTests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterInventory', () => {
      it('should have a iapGetAdapterInventory function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterInventory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the inventory', (done) => {
        try {
          a.iapGetAdapterInventory((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    describe('metadata.json', () => {
      it('should have a metadata.json', (done) => {
        try {
          fs.exists('metadata.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json is customized', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.equal('adapter-ansible_tower', metadataDotJson.name);
          assert.notEqual(undefined, metadataDotJson.webName);
          assert.notEqual(null, metadataDotJson.webName);
          assert.notEqual('', metadataDotJson.webName);
          assert.equal('Adapter', metadataDotJson.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json contains accurate documentation', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.documentation);
          assert.equal('https://www.npmjs.com/package/@itentialopensource/adapter-ansible_tower', metadataDotJson.documentation.npmLink);
          assert.equal('https://docs.itential.com/opensource/docs/troubleshooting-an-adapter', metadataDotJson.documentation.faqLink);
          assert.equal('https://gitlab.com/itentialopensource/adapters/contributing-guide', metadataDotJson.documentation.contributeLink);
          assert.equal('https://itential.atlassian.net/servicedesk/customer/portals', metadataDotJson.documentation.issueLink);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json has related items', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.relatedItems);
          assert.notEqual(undefined, metadataDotJson.relatedItems.adapters);
          assert.notEqual(undefined, metadataDotJson.relatedItems.integrations);
          assert.notEqual(undefined, metadataDotJson.relatedItems.ecosystemApplications);
          assert.notEqual(undefined, metadataDotJson.relatedItems.workflowProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.transformationProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.exampleProjects);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    describe('#versioningList - errors', () => {
      it('should have a versioningList function', (done) => {
        try {
          assert.equal(true, typeof a.versioningList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#versioningRead - errors', () => {
      it('should have a versioningRead function', (done) => {
        try {
          assert.equal(true, typeof a.versioningRead === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#authenticationOList - errors', () => {
      it('should have a authenticationOList function', (done) => {
        try {
          assert.equal(true, typeof a.authenticationOList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#authenticationApplicationsList0 - errors', () => {
      it('should have a authenticationApplicationsList0 function', (done) => {
        try {
          assert.equal(true, typeof a.authenticationApplicationsList0 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#authenticationApplicationsCreate0 - errors', () => {
      it('should have a authenticationApplicationsCreate0 function', (done) => {
        try {
          assert.equal(true, typeof a.authenticationApplicationsCreate0 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#authenticationApplicationsRead0 - errors', () => {
      it('should have a authenticationApplicationsRead0 function', (done) => {
        try {
          assert.equal(true, typeof a.authenticationApplicationsRead0 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.authenticationApplicationsRead0(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-authenticationApplicationsRead0', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#authenticationApplicationsUpdate0 - errors', () => {
      it('should have a authenticationApplicationsUpdate0 function', (done) => {
        try {
          assert.equal(true, typeof a.authenticationApplicationsUpdate0 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.authenticationApplicationsUpdate0(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-authenticationApplicationsUpdate0', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#authenticationApplicationsDelete0 - errors', () => {
      it('should have a authenticationApplicationsDelete0 function', (done) => {
        try {
          assert.equal(true, typeof a.authenticationApplicationsDelete0 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.authenticationApplicationsDelete0(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-authenticationApplicationsDelete0', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#authenticationApplicationsPartialUpdate0 - errors', () => {
      it('should have a authenticationApplicationsPartialUpdate0 function', (done) => {
        try {
          assert.equal(true, typeof a.authenticationApplicationsPartialUpdate0 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.authenticationApplicationsPartialUpdate0(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-authenticationApplicationsPartialUpdate0', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#authenticationApplicationsActivityStreamList - errors', () => {
      it('should have a authenticationApplicationsActivityStreamList function', (done) => {
        try {
          assert.equal(true, typeof a.authenticationApplicationsActivityStreamList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.authenticationApplicationsActivityStreamList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-authenticationApplicationsActivityStreamList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#authenticationApplicationsTokensList0 - errors', () => {
      it('should have a authenticationApplicationsTokensList0 function', (done) => {
        try {
          assert.equal(true, typeof a.authenticationApplicationsTokensList0 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.authenticationApplicationsTokensList0(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-authenticationApplicationsTokensList0', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#authenticationApplicationsTokensCreate0 - errors', () => {
      it('should have a authenticationApplicationsTokensCreate0 function', (done) => {
        try {
          assert.equal(true, typeof a.authenticationApplicationsTokensCreate0 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.authenticationApplicationsTokensCreate0(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-authenticationApplicationsTokensCreate0', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#authenticationTokensList0 - errors', () => {
      it('should have a authenticationTokensList0 function', (done) => {
        try {
          assert.equal(true, typeof a.authenticationTokensList0 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#authenticationTokensCreate0 - errors', () => {
      it('should have a authenticationTokensCreate0 function', (done) => {
        try {
          assert.equal(true, typeof a.authenticationTokensCreate0 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#authenticationTokensRead - errors', () => {
      it('should have a authenticationTokensRead function', (done) => {
        try {
          assert.equal(true, typeof a.authenticationTokensRead === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.authenticationTokensRead(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-authenticationTokensRead', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#authenticationTokensUpdate - errors', () => {
      it('should have a authenticationTokensUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.authenticationTokensUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.authenticationTokensUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-authenticationTokensUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#authenticationTokensDelete - errors', () => {
      it('should have a authenticationTokensDelete function', (done) => {
        try {
          assert.equal(true, typeof a.authenticationTokensDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.authenticationTokensDelete(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-authenticationTokensDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#authenticationTokensPartialUpdate - errors', () => {
      it('should have a authenticationTokensPartialUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.authenticationTokensPartialUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.authenticationTokensPartialUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-authenticationTokensPartialUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#authenticationTokensActivityStreamList - errors', () => {
      it('should have a authenticationTokensActivityStreamList function', (done) => {
        try {
          assert.equal(true, typeof a.authenticationTokensActivityStreamList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.authenticationTokensActivityStreamList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-authenticationTokensActivityStreamList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#activityStreamsActivityStreamList - errors', () => {
      it('should have a activityStreamsActivityStreamList function', (done) => {
        try {
          assert.equal(true, typeof a.activityStreamsActivityStreamList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#activityStreamsActivityStreamRead - errors', () => {
      it('should have a activityStreamsActivityStreamRead function', (done) => {
        try {
          assert.equal(true, typeof a.activityStreamsActivityStreamRead === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.activityStreamsActivityStreamRead(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-activityStreamsActivityStreamRead', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#adHocCommandEventsAdHocCommandEventsList - errors', () => {
      it('should have a adHocCommandEventsAdHocCommandEventsList function', (done) => {
        try {
          assert.equal(true, typeof a.adHocCommandEventsAdHocCommandEventsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#adHocCommandEventsAdHocCommandEventsRead - errors', () => {
      it('should have a adHocCommandEventsAdHocCommandEventsRead function', (done) => {
        try {
          assert.equal(true, typeof a.adHocCommandEventsAdHocCommandEventsRead === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.adHocCommandEventsAdHocCommandEventsRead(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-adHocCommandEventsAdHocCommandEventsRead', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#adHocCommandsAdHocCommandsList - errors', () => {
      it('should have a adHocCommandsAdHocCommandsList function', (done) => {
        try {
          assert.equal(true, typeof a.adHocCommandsAdHocCommandsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#adHocCommandsAdHocCommandsCreate - errors', () => {
      it('should have a adHocCommandsAdHocCommandsCreate function', (done) => {
        try {
          assert.equal(true, typeof a.adHocCommandsAdHocCommandsCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#adHocCommandsAdHocCommandsRead - errors', () => {
      it('should have a adHocCommandsAdHocCommandsRead function', (done) => {
        try {
          assert.equal(true, typeof a.adHocCommandsAdHocCommandsRead === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.adHocCommandsAdHocCommandsRead(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-adHocCommandsAdHocCommandsRead', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#adHocCommandsAdHocCommandsDelete - errors', () => {
      it('should have a adHocCommandsAdHocCommandsDelete function', (done) => {
        try {
          assert.equal(true, typeof a.adHocCommandsAdHocCommandsDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.adHocCommandsAdHocCommandsDelete(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-adHocCommandsAdHocCommandsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#adHocCommandsAdHocCommandsActivityStreamList - errors', () => {
      it('should have a adHocCommandsAdHocCommandsActivityStreamList function', (done) => {
        try {
          assert.equal(true, typeof a.adHocCommandsAdHocCommandsActivityStreamList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.adHocCommandsAdHocCommandsActivityStreamList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-adHocCommandsAdHocCommandsActivityStreamListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#adHocCommandsAdHocCommandsCancelRead - errors', () => {
      it('should have a adHocCommandsAdHocCommandsCancelRead function', (done) => {
        try {
          assert.equal(true, typeof a.adHocCommandsAdHocCommandsCancelRead === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.adHocCommandsAdHocCommandsCancelRead(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-adHocCommandsAdHocCommandsCancelRead', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#adHocCommandsAdHocCommandsCancelCreate - errors', () => {
      it('should have a adHocCommandsAdHocCommandsCancelCreate function', (done) => {
        try {
          assert.equal(true, typeof a.adHocCommandsAdHocCommandsCancelCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.adHocCommandsAdHocCommandsCancelCreate(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-adHocCommandsAdHocCommandsCancelCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#adHocCommandsAdHocCommandsEventsList - errors', () => {
      it('should have a adHocCommandsAdHocCommandsEventsList function', (done) => {
        try {
          assert.equal(true, typeof a.adHocCommandsAdHocCommandsEventsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.adHocCommandsAdHocCommandsEventsList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-adHocCommandsAdHocCommandsEventsListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#adHocCommandsAdHocCommandsNotificationsList - errors', () => {
      it('should have a adHocCommandsAdHocCommandsNotificationsList function', (done) => {
        try {
          assert.equal(true, typeof a.adHocCommandsAdHocCommandsNotificationsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.adHocCommandsAdHocCommandsNotificationsList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-adHocCommandsAdHocCommandsNotificationsListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#adHocCommandsAdHocCommandsRelaunchList - errors', () => {
      it('should have a adHocCommandsAdHocCommandsRelaunchList function', (done) => {
        try {
          assert.equal(true, typeof a.adHocCommandsAdHocCommandsRelaunchList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.adHocCommandsAdHocCommandsRelaunchList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-adHocCommandsAdHocCommandsRelaunchListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#adHocCommandsAdHocCommandsRelaunchCreate - errors', () => {
      it('should have a adHocCommandsAdHocCommandsRelaunchCreate function', (done) => {
        try {
          assert.equal(true, typeof a.adHocCommandsAdHocCommandsRelaunchCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.adHocCommandsAdHocCommandsRelaunchCreate(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-adHocCommandsAdHocCommandsRelaunchCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#adHocCommandsAdHocCommandsStdoutRead - errors', () => {
      it('should have a adHocCommandsAdHocCommandsStdoutRead function', (done) => {
        try {
          assert.equal(true, typeof a.adHocCommandsAdHocCommandsStdoutRead === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.adHocCommandsAdHocCommandsStdoutRead(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-adHocCommandsAdHocCommandsStdoutRead', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#systemConfigurationAuthList - errors', () => {
      it('should have a systemConfigurationAuthList function', (done) => {
        try {
          assert.equal(true, typeof a.systemConfigurationAuthList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#systemConfigurationConfigList - errors', () => {
      it('should have a systemConfigurationConfigList function', (done) => {
        try {
          assert.equal(true, typeof a.systemConfigurationConfigList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#systemConfigurationConfigCreate - errors', () => {
      it('should have a systemConfigurationConfigCreate function', (done) => {
        try {
          assert.equal(true, typeof a.systemConfigurationConfigCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#systemConfigurationConfigDelete - errors', () => {
      it('should have a systemConfigurationConfigDelete function', (done) => {
        try {
          assert.equal(true, typeof a.systemConfigurationConfigDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#systemConfigurationPingList - errors', () => {
      it('should have a systemConfigurationPingList function', (done) => {
        try {
          assert.equal(true, typeof a.systemConfigurationPingList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#credentialInputSourcesCredentialInputSourcesList - errors', () => {
      it('should have a credentialInputSourcesCredentialInputSourcesList function', (done) => {
        try {
          assert.equal(true, typeof a.credentialInputSourcesCredentialInputSourcesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#credentialInputSourcesCredentialInputSourcesCreate - errors', () => {
      it('should have a credentialInputSourcesCredentialInputSourcesCreate function', (done) => {
        try {
          assert.equal(true, typeof a.credentialInputSourcesCredentialInputSourcesCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#credentialInputSourcesCredentialInputSourcesRead - errors', () => {
      it('should have a credentialInputSourcesCredentialInputSourcesRead function', (done) => {
        try {
          assert.equal(true, typeof a.credentialInputSourcesCredentialInputSourcesRead === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.credentialInputSourcesCredentialInputSourcesRead(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-credentialInputSourcesCredentialInputSourcesRead', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#credentialInputSourcesCredentialInputSourcesUpdate - errors', () => {
      it('should have a credentialInputSourcesCredentialInputSourcesUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.credentialInputSourcesCredentialInputSourcesUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.credentialInputSourcesCredentialInputSourcesUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-credentialInputSourcesCredentialInputSourcesUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#credentialInputSourcesCredentialInputSourcesDelete - errors', () => {
      it('should have a credentialInputSourcesCredentialInputSourcesDelete function', (done) => {
        try {
          assert.equal(true, typeof a.credentialInputSourcesCredentialInputSourcesDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.credentialInputSourcesCredentialInputSourcesDelete(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-credentialInputSourcesCredentialInputSourcesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#credentialInputSourcesCredentialInputSourcesPartialUpdate - errors', () => {
      it('should have a credentialInputSourcesCredentialInputSourcesPartialUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.credentialInputSourcesCredentialInputSourcesPartialUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.credentialInputSourcesCredentialInputSourcesPartialUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-credentialInputSourcesCredentialInputSourcesPartialUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#credentialTypesCredentialTypesList - errors', () => {
      it('should have a credentialTypesCredentialTypesList function', (done) => {
        try {
          assert.equal(true, typeof a.credentialTypesCredentialTypesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#credentialTypesCredentialTypesCreate - errors', () => {
      it('should have a credentialTypesCredentialTypesCreate function', (done) => {
        try {
          assert.equal(true, typeof a.credentialTypesCredentialTypesCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#credentialTypesCredentialTypesRead - errors', () => {
      it('should have a credentialTypesCredentialTypesRead function', (done) => {
        try {
          assert.equal(true, typeof a.credentialTypesCredentialTypesRead === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.credentialTypesCredentialTypesRead(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-credentialTypesCredentialTypesRead', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#credentialTypesCredentialTypesUpdate - errors', () => {
      it('should have a credentialTypesCredentialTypesUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.credentialTypesCredentialTypesUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.credentialTypesCredentialTypesUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-credentialTypesCredentialTypesUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#credentialTypesCredentialTypesDelete - errors', () => {
      it('should have a credentialTypesCredentialTypesDelete function', (done) => {
        try {
          assert.equal(true, typeof a.credentialTypesCredentialTypesDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.credentialTypesCredentialTypesDelete(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-credentialTypesCredentialTypesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#credentialTypesCredentialTypesPartialUpdate - errors', () => {
      it('should have a credentialTypesCredentialTypesPartialUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.credentialTypesCredentialTypesPartialUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.credentialTypesCredentialTypesPartialUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-credentialTypesCredentialTypesPartialUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#credentialTypesCredentialTypesActivityStreamList - errors', () => {
      it('should have a credentialTypesCredentialTypesActivityStreamList function', (done) => {
        try {
          assert.equal(true, typeof a.credentialTypesCredentialTypesActivityStreamList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.credentialTypesCredentialTypesActivityStreamList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-credentialTypesCredentialTypesActivityStreamListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#credentialTypesCredentialTypesCredentialsList - errors', () => {
      it('should have a credentialTypesCredentialTypesCredentialsList function', (done) => {
        try {
          assert.equal(true, typeof a.credentialTypesCredentialTypesCredentialsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.credentialTypesCredentialTypesCredentialsList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-credentialTypesCredentialTypesCredentialsListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#credentialTypesCredentialTypesCredentialsCreate - errors', () => {
      it('should have a credentialTypesCredentialTypesCredentialsCreate function', (done) => {
        try {
          assert.equal(true, typeof a.credentialTypesCredentialTypesCredentialsCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.credentialTypesCredentialTypesCredentialsCreate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-credentialTypesCredentialTypesCredentialsCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#credentialTypesCredentialTypesTestRead - errors', () => {
      it('should have a credentialTypesCredentialTypesTestRead function', (done) => {
        try {
          assert.equal(true, typeof a.credentialTypesCredentialTypesTestRead === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.credentialTypesCredentialTypesTestRead(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-credentialTypesCredentialTypesTestRead', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#credentialTypesCredentialTypesTestCreate - errors', () => {
      it('should have a credentialTypesCredentialTypesTestCreate function', (done) => {
        try {
          assert.equal(true, typeof a.credentialTypesCredentialTypesTestCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.credentialTypesCredentialTypesTestCreate(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-credentialTypesCredentialTypesTestCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#credentialsCredentialsList - errors', () => {
      it('should have a credentialsCredentialsList function', (done) => {
        try {
          assert.equal(true, typeof a.credentialsCredentialsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#credentialsCredentialsCreate - errors', () => {
      it('should have a credentialsCredentialsCreate function', (done) => {
        try {
          assert.equal(true, typeof a.credentialsCredentialsCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#credentialsCredentialsRead - errors', () => {
      it('should have a credentialsCredentialsRead function', (done) => {
        try {
          assert.equal(true, typeof a.credentialsCredentialsRead === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.credentialsCredentialsRead(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-credentialsCredentialsRead', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#credentialsCredentialsUpdate - errors', () => {
      it('should have a credentialsCredentialsUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.credentialsCredentialsUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.credentialsCredentialsUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-credentialsCredentialsUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#credentialsCredentialsDelete - errors', () => {
      it('should have a credentialsCredentialsDelete function', (done) => {
        try {
          assert.equal(true, typeof a.credentialsCredentialsDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.credentialsCredentialsDelete(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-credentialsCredentialsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#credentialsCredentialsPartialUpdate - errors', () => {
      it('should have a credentialsCredentialsPartialUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.credentialsCredentialsPartialUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.credentialsCredentialsPartialUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-credentialsCredentialsPartialUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#credentialsCredentialsAccessListList - errors', () => {
      it('should have a credentialsCredentialsAccessListList function', (done) => {
        try {
          assert.equal(true, typeof a.credentialsCredentialsAccessListList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.credentialsCredentialsAccessListList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-credentialsCredentialsAccessListListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#credentialsCredentialsActivityStreamList - errors', () => {
      it('should have a credentialsCredentialsActivityStreamList function', (done) => {
        try {
          assert.equal(true, typeof a.credentialsCredentialsActivityStreamList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.credentialsCredentialsActivityStreamList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-credentialsCredentialsActivityStreamListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#credentialsCredentialsCopyList - errors', () => {
      it('should have a credentialsCredentialsCopyList function', (done) => {
        try {
          assert.equal(true, typeof a.credentialsCredentialsCopyList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.credentialsCredentialsCopyList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-credentialsCredentialsCopyListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#credentialsCredentialsCopyCreate - errors', () => {
      it('should have a credentialsCredentialsCopyCreate function', (done) => {
        try {
          assert.equal(true, typeof a.credentialsCredentialsCopyCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.credentialsCredentialsCopyCreate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-credentialsCredentialsCopyCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#credentialsCredentialsInputSourcesList - errors', () => {
      it('should have a credentialsCredentialsInputSourcesList function', (done) => {
        try {
          assert.equal(true, typeof a.credentialsCredentialsInputSourcesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.credentialsCredentialsInputSourcesList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-credentialsCredentialsInputSourcesListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#credentialsCredentialsInputSourcesCreate - errors', () => {
      it('should have a credentialsCredentialsInputSourcesCreate function', (done) => {
        try {
          assert.equal(true, typeof a.credentialsCredentialsInputSourcesCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.credentialsCredentialsInputSourcesCreate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-credentialsCredentialsInputSourcesCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#credentialsCredentialsObjectRolesList - errors', () => {
      it('should have a credentialsCredentialsObjectRolesList function', (done) => {
        try {
          assert.equal(true, typeof a.credentialsCredentialsObjectRolesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.credentialsCredentialsObjectRolesList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-credentialsCredentialsObjectRolesListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#credentialsCredentialsOwnerTeamsList - errors', () => {
      it('should have a credentialsCredentialsOwnerTeamsList function', (done) => {
        try {
          assert.equal(true, typeof a.credentialsCredentialsOwnerTeamsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.credentialsCredentialsOwnerTeamsList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-credentialsCredentialsOwnerTeamsListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#credentialsCredentialsOwnerUsersList - errors', () => {
      it('should have a credentialsCredentialsOwnerUsersList function', (done) => {
        try {
          assert.equal(true, typeof a.credentialsCredentialsOwnerUsersList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.credentialsCredentialsOwnerUsersList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-credentialsCredentialsOwnerUsersListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#credentialsCredentialsTestRead - errors', () => {
      it('should have a credentialsCredentialsTestRead function', (done) => {
        try {
          assert.equal(true, typeof a.credentialsCredentialsTestRead === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.credentialsCredentialsTestRead(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-credentialsCredentialsTestRead', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#credentialsCredentialsTestCreate - errors', () => {
      it('should have a credentialsCredentialsTestCreate function', (done) => {
        try {
          assert.equal(true, typeof a.credentialsCredentialsTestCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.credentialsCredentialsTestCreate(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-credentialsCredentialsTestCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#dashboardDashboardList - errors', () => {
      it('should have a dashboardDashboardList function', (done) => {
        try {
          assert.equal(true, typeof a.dashboardDashboardList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#dashboardDashboardGraphsJobsList - errors', () => {
      it('should have a dashboardDashboardGraphsJobsList function', (done) => {
        try {
          assert.equal(true, typeof a.dashboardDashboardGraphsJobsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#groupsGroupsList - errors', () => {
      it('should have a groupsGroupsList function', (done) => {
        try {
          assert.equal(true, typeof a.groupsGroupsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#groupsGroupsCreate - errors', () => {
      it('should have a groupsGroupsCreate function', (done) => {
        try {
          assert.equal(true, typeof a.groupsGroupsCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#groupsGroupsRead - errors', () => {
      it('should have a groupsGroupsRead function', (done) => {
        try {
          assert.equal(true, typeof a.groupsGroupsRead === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.groupsGroupsRead(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-groupsGroupsRead', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#groupsGroupsUpdate - errors', () => {
      it('should have a groupsGroupsUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.groupsGroupsUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.groupsGroupsUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-groupsGroupsUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#groupsGroupsDelete - errors', () => {
      it('should have a groupsGroupsDelete function', (done) => {
        try {
          assert.equal(true, typeof a.groupsGroupsDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.groupsGroupsDelete(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-groupsGroupsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#groupsGroupsPartialUpdate - errors', () => {
      it('should have a groupsGroupsPartialUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.groupsGroupsPartialUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.groupsGroupsPartialUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-groupsGroupsPartialUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#groupsGroupsActivityStreamList - errors', () => {
      it('should have a groupsGroupsActivityStreamList function', (done) => {
        try {
          assert.equal(true, typeof a.groupsGroupsActivityStreamList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.groupsGroupsActivityStreamList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-groupsGroupsActivityStreamListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#groupsGroupsAdHocCommandsList - errors', () => {
      it('should have a groupsGroupsAdHocCommandsList function', (done) => {
        try {
          assert.equal(true, typeof a.groupsGroupsAdHocCommandsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.groupsGroupsAdHocCommandsList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-groupsGroupsAdHocCommandsListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#groupsGroupsAdHocCommandsCreate - errors', () => {
      it('should have a groupsGroupsAdHocCommandsCreate function', (done) => {
        try {
          assert.equal(true, typeof a.groupsGroupsAdHocCommandsCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.groupsGroupsAdHocCommandsCreate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-groupsGroupsAdHocCommandsCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#groupsGroupsAllHostsList - errors', () => {
      it('should have a groupsGroupsAllHostsList function', (done) => {
        try {
          assert.equal(true, typeof a.groupsGroupsAllHostsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.groupsGroupsAllHostsList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-groupsGroupsAllHostsListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#groupsGroupsChildrenList - errors', () => {
      it('should have a groupsGroupsChildrenList function', (done) => {
        try {
          assert.equal(true, typeof a.groupsGroupsChildrenList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.groupsGroupsChildrenList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-groupsGroupsChildrenListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#groupsGroupsChildrenCreate - errors', () => {
      it('should have a groupsGroupsChildrenCreate function', (done) => {
        try {
          assert.equal(true, typeof a.groupsGroupsChildrenCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.groupsGroupsChildrenCreate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-groupsGroupsChildrenCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#groupsGroupsHostsList - errors', () => {
      it('should have a groupsGroupsHostsList function', (done) => {
        try {
          assert.equal(true, typeof a.groupsGroupsHostsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.groupsGroupsHostsList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-groupsGroupsHostsListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#groupsGroupsHostsCreate - errors', () => {
      it('should have a groupsGroupsHostsCreate function', (done) => {
        try {
          assert.equal(true, typeof a.groupsGroupsHostsCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.groupsGroupsHostsCreate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-groupsGroupsHostsCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#groupsGroupsInventorySourcesList - errors', () => {
      it('should have a groupsGroupsInventorySourcesList function', (done) => {
        try {
          assert.equal(true, typeof a.groupsGroupsInventorySourcesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.groupsGroupsInventorySourcesList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-groupsGroupsInventorySourcesListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#groupsGroupsJobEventsList - errors', () => {
      it('should have a groupsGroupsJobEventsList function', (done) => {
        try {
          assert.equal(true, typeof a.groupsGroupsJobEventsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.groupsGroupsJobEventsList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-groupsGroupsJobEventsListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#groupsGroupsJobHostSummariesList - errors', () => {
      it('should have a groupsGroupsJobHostSummariesList function', (done) => {
        try {
          assert.equal(true, typeof a.groupsGroupsJobHostSummariesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.groupsGroupsJobHostSummariesList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-groupsGroupsJobHostSummariesListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#groupsGroupsPotentialChildrenList - errors', () => {
      it('should have a groupsGroupsPotentialChildrenList function', (done) => {
        try {
          assert.equal(true, typeof a.groupsGroupsPotentialChildrenList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.groupsGroupsPotentialChildrenList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-groupsGroupsPotentialChildrenListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#groupsGroupsVariableDataRead - errors', () => {
      it('should have a groupsGroupsVariableDataRead function', (done) => {
        try {
          assert.equal(true, typeof a.groupsGroupsVariableDataRead === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.groupsGroupsVariableDataRead(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-groupsGroupsVariableDataRead', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#groupsGroupsVariableDataUpdate - errors', () => {
      it('should have a groupsGroupsVariableDataUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.groupsGroupsVariableDataUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.groupsGroupsVariableDataUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-groupsGroupsVariableDataUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#groupsGroupsVariableDataPartialUpdate - errors', () => {
      it('should have a groupsGroupsVariableDataPartialUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.groupsGroupsVariableDataPartialUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.groupsGroupsVariableDataPartialUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-groupsGroupsVariableDataPartialUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#hostsHostsList - errors', () => {
      it('should have a hostsHostsList function', (done) => {
        try {
          assert.equal(true, typeof a.hostsHostsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#hostsHostsCreate - errors', () => {
      it('should have a hostsHostsCreate function', (done) => {
        try {
          assert.equal(true, typeof a.hostsHostsCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#hostsHostsRead - errors', () => {
      it('should have a hostsHostsRead function', (done) => {
        try {
          assert.equal(true, typeof a.hostsHostsRead === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.hostsHostsRead(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-hostsHostsRead', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#hostsHostsUpdate - errors', () => {
      it('should have a hostsHostsUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.hostsHostsUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.hostsHostsUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-hostsHostsUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#hostsHostsDelete - errors', () => {
      it('should have a hostsHostsDelete function', (done) => {
        try {
          assert.equal(true, typeof a.hostsHostsDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.hostsHostsDelete(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-hostsHostsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#hostsHostsPartialUpdate - errors', () => {
      it('should have a hostsHostsPartialUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.hostsHostsPartialUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.hostsHostsPartialUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-hostsHostsPartialUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#hostsHostsActivityStreamList - errors', () => {
      it('should have a hostsHostsActivityStreamList function', (done) => {
        try {
          assert.equal(true, typeof a.hostsHostsActivityStreamList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.hostsHostsActivityStreamList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-hostsHostsActivityStreamListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#hostsHostsAdHocCommandEventsList - errors', () => {
      it('should have a hostsHostsAdHocCommandEventsList function', (done) => {
        try {
          assert.equal(true, typeof a.hostsHostsAdHocCommandEventsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.hostsHostsAdHocCommandEventsList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-hostsHostsAdHocCommandEventsListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#hostsHostsAdHocCommandsList - errors', () => {
      it('should have a hostsHostsAdHocCommandsList function', (done) => {
        try {
          assert.equal(true, typeof a.hostsHostsAdHocCommandsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.hostsHostsAdHocCommandsList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-hostsHostsAdHocCommandsListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#hostsHostsAdHocCommandsCreate - errors', () => {
      it('should have a hostsHostsAdHocCommandsCreate function', (done) => {
        try {
          assert.equal(true, typeof a.hostsHostsAdHocCommandsCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.hostsHostsAdHocCommandsCreate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-hostsHostsAdHocCommandsCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#hostsHostsAllGroupsList - errors', () => {
      it('should have a hostsHostsAllGroupsList function', (done) => {
        try {
          assert.equal(true, typeof a.hostsHostsAllGroupsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.hostsHostsAllGroupsList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-hostsHostsAllGroupsListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#hostsHostsAnsibleFactsRead - errors', () => {
      it('should have a hostsHostsAnsibleFactsRead function', (done) => {
        try {
          assert.equal(true, typeof a.hostsHostsAnsibleFactsRead === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.hostsHostsAnsibleFactsRead(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-hostsHostsAnsibleFactsRead', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#hostsHostsGroupsList - errors', () => {
      it('should have a hostsHostsGroupsList function', (done) => {
        try {
          assert.equal(true, typeof a.hostsHostsGroupsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.hostsHostsGroupsList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-hostsHostsGroupsListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#hostsHostsGroupsCreate - errors', () => {
      it('should have a hostsHostsGroupsCreate function', (done) => {
        try {
          assert.equal(true, typeof a.hostsHostsGroupsCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.hostsHostsGroupsCreate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-hostsHostsGroupsCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#hostsHostsInsightsList - errors', () => {
      it('should have a hostsHostsInsightsList function', (done) => {
        try {
          assert.equal(true, typeof a.hostsHostsInsightsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.hostsHostsInsightsList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-hostsHostsInsightsListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#hostsHostsInventorySourcesList - errors', () => {
      it('should have a hostsHostsInventorySourcesList function', (done) => {
        try {
          assert.equal(true, typeof a.hostsHostsInventorySourcesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.hostsHostsInventorySourcesList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-hostsHostsInventorySourcesListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#hostsHostsJobEventsList - errors', () => {
      it('should have a hostsHostsJobEventsList function', (done) => {
        try {
          assert.equal(true, typeof a.hostsHostsJobEventsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.hostsHostsJobEventsList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-hostsHostsJobEventsListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#hostsHostsJobHostSummariesList - errors', () => {
      it('should have a hostsHostsJobHostSummariesList function', (done) => {
        try {
          assert.equal(true, typeof a.hostsHostsJobHostSummariesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.hostsHostsJobHostSummariesList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-hostsHostsJobHostSummariesListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#hostsHostsSmartInventoriesList - errors', () => {
      it('should have a hostsHostsSmartInventoriesList function', (done) => {
        try {
          assert.equal(true, typeof a.hostsHostsSmartInventoriesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.hostsHostsSmartInventoriesList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-hostsHostsSmartInventoriesListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#hostsHostsVariableDataRead - errors', () => {
      it('should have a hostsHostsVariableDataRead function', (done) => {
        try {
          assert.equal(true, typeof a.hostsHostsVariableDataRead === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.hostsHostsVariableDataRead(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-hostsHostsVariableDataRead', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#hostsHostsVariableDataUpdate - errors', () => {
      it('should have a hostsHostsVariableDataUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.hostsHostsVariableDataUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.hostsHostsVariableDataUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-hostsHostsVariableDataUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#hostsHostsVariableDataPartialUpdate - errors', () => {
      it('should have a hostsHostsVariableDataPartialUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.hostsHostsVariableDataPartialUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.hostsHostsVariableDataPartialUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-hostsHostsVariableDataPartialUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#instanceGroupsInstanceGroupsList - errors', () => {
      it('should have a instanceGroupsInstanceGroupsList function', (done) => {
        try {
          assert.equal(true, typeof a.instanceGroupsInstanceGroupsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#instanceGroupsInstanceGroupsCreate - errors', () => {
      it('should have a instanceGroupsInstanceGroupsCreate function', (done) => {
        try {
          assert.equal(true, typeof a.instanceGroupsInstanceGroupsCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#instanceGroupsInstanceGroupsRead - errors', () => {
      it('should have a instanceGroupsInstanceGroupsRead function', (done) => {
        try {
          assert.equal(true, typeof a.instanceGroupsInstanceGroupsRead === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.instanceGroupsInstanceGroupsRead(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-instanceGroupsInstanceGroupsRead', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#instanceGroupsInstanceGroupsUpdate - errors', () => {
      it('should have a instanceGroupsInstanceGroupsUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.instanceGroupsInstanceGroupsUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.instanceGroupsInstanceGroupsUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-instanceGroupsInstanceGroupsUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#instanceGroupsInstanceGroupsDelete - errors', () => {
      it('should have a instanceGroupsInstanceGroupsDelete function', (done) => {
        try {
          assert.equal(true, typeof a.instanceGroupsInstanceGroupsDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.instanceGroupsInstanceGroupsDelete(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-instanceGroupsInstanceGroupsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#instanceGroupsInstanceGroupsPartialUpdate - errors', () => {
      it('should have a instanceGroupsInstanceGroupsPartialUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.instanceGroupsInstanceGroupsPartialUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.instanceGroupsInstanceGroupsPartialUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-instanceGroupsInstanceGroupsPartialUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#instanceGroupsInstanceGroupsInstancesList - errors', () => {
      it('should have a instanceGroupsInstanceGroupsInstancesList function', (done) => {
        try {
          assert.equal(true, typeof a.instanceGroupsInstanceGroupsInstancesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.instanceGroupsInstanceGroupsInstancesList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-instanceGroupsInstanceGroupsInstancesListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#instanceGroupsInstanceGroupsInstancesCreate - errors', () => {
      it('should have a instanceGroupsInstanceGroupsInstancesCreate function', (done) => {
        try {
          assert.equal(true, typeof a.instanceGroupsInstanceGroupsInstancesCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.instanceGroupsInstanceGroupsInstancesCreate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-instanceGroupsInstanceGroupsInstancesCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#instanceGroupsInstanceGroupsJobsList - errors', () => {
      it('should have a instanceGroupsInstanceGroupsJobsList function', (done) => {
        try {
          assert.equal(true, typeof a.instanceGroupsInstanceGroupsJobsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.instanceGroupsInstanceGroupsJobsList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-instanceGroupsInstanceGroupsJobsListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#instancesInstancesList - errors', () => {
      it('should have a instancesInstancesList function', (done) => {
        try {
          assert.equal(true, typeof a.instancesInstancesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#instancesInstancesRead - errors', () => {
      it('should have a instancesInstancesRead function', (done) => {
        try {
          assert.equal(true, typeof a.instancesInstancesRead === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.instancesInstancesRead(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-instancesInstancesRead', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#instancesInstancesUpdate - errors', () => {
      it('should have a instancesInstancesUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.instancesInstancesUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.instancesInstancesUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-instancesInstancesUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#instancesInstancesPartialUpdate - errors', () => {
      it('should have a instancesInstancesPartialUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.instancesInstancesPartialUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.instancesInstancesPartialUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-instancesInstancesPartialUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#instancesInstancesInstanceGroupsList - errors', () => {
      it('should have a instancesInstancesInstanceGroupsList function', (done) => {
        try {
          assert.equal(true, typeof a.instancesInstancesInstanceGroupsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.instancesInstancesInstanceGroupsList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-instancesInstancesInstanceGroupsListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#instancesInstancesInstanceGroupsCreate - errors', () => {
      it('should have a instancesInstancesInstanceGroupsCreate function', (done) => {
        try {
          assert.equal(true, typeof a.instancesInstancesInstanceGroupsCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.instancesInstancesInstanceGroupsCreate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-instancesInstancesInstanceGroupsCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#instancesInstancesJobsList - errors', () => {
      it('should have a instancesInstancesJobsList function', (done) => {
        try {
          assert.equal(true, typeof a.instancesInstancesJobsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.instancesInstancesJobsList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-instancesInstancesJobsListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventoriesInventoriesList - errors', () => {
      it('should have a inventoriesInventoriesList function', (done) => {
        try {
          assert.equal(true, typeof a.inventoriesInventoriesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventoriesInventoriesCreate - errors', () => {
      it('should have a inventoriesInventoriesCreate function', (done) => {
        try {
          assert.equal(true, typeof a.inventoriesInventoriesCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventoriesInventoriesRead - errors', () => {
      it('should have a inventoriesInventoriesRead function', (done) => {
        try {
          assert.equal(true, typeof a.inventoriesInventoriesRead === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.inventoriesInventoriesRead(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-inventoriesInventoriesRead', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventoriesInventoriesUpdate - errors', () => {
      it('should have a inventoriesInventoriesUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.inventoriesInventoriesUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.inventoriesInventoriesUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-inventoriesInventoriesUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventoriesInventoriesDelete - errors', () => {
      it('should have a inventoriesInventoriesDelete function', (done) => {
        try {
          assert.equal(true, typeof a.inventoriesInventoriesDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.inventoriesInventoriesDelete(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-inventoriesInventoriesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventoriesInventoriesPartialUpdate - errors', () => {
      it('should have a inventoriesInventoriesPartialUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.inventoriesInventoriesPartialUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.inventoriesInventoriesPartialUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-inventoriesInventoriesPartialUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventoriesInventoriesAccessListList - errors', () => {
      it('should have a inventoriesInventoriesAccessListList function', (done) => {
        try {
          assert.equal(true, typeof a.inventoriesInventoriesAccessListList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.inventoriesInventoriesAccessListList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-inventoriesInventoriesAccessListListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventoriesInventoriesActivityStreamList - errors', () => {
      it('should have a inventoriesInventoriesActivityStreamList function', (done) => {
        try {
          assert.equal(true, typeof a.inventoriesInventoriesActivityStreamList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.inventoriesInventoriesActivityStreamList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-inventoriesInventoriesActivityStreamListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventoriesInventoriesAdHocCommandsList - errors', () => {
      it('should have a inventoriesInventoriesAdHocCommandsList function', (done) => {
        try {
          assert.equal(true, typeof a.inventoriesInventoriesAdHocCommandsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.inventoriesInventoriesAdHocCommandsList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-inventoriesInventoriesAdHocCommandsListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventoriesInventoriesAdHocCommandsCreate - errors', () => {
      it('should have a inventoriesInventoriesAdHocCommandsCreate function', (done) => {
        try {
          assert.equal(true, typeof a.inventoriesInventoriesAdHocCommandsCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.inventoriesInventoriesAdHocCommandsCreate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-inventoriesInventoriesAdHocCommandsCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventoriesInventoriesCopyList - errors', () => {
      it('should have a inventoriesInventoriesCopyList function', (done) => {
        try {
          assert.equal(true, typeof a.inventoriesInventoriesCopyList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.inventoriesInventoriesCopyList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-inventoriesInventoriesCopyListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventoriesInventoriesCopyCreate - errors', () => {
      it('should have a inventoriesInventoriesCopyCreate function', (done) => {
        try {
          assert.equal(true, typeof a.inventoriesInventoriesCopyCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.inventoriesInventoriesCopyCreate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-inventoriesInventoriesCopyCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventoriesInventoriesGroupsList - errors', () => {
      it('should have a inventoriesInventoriesGroupsList function', (done) => {
        try {
          assert.equal(true, typeof a.inventoriesInventoriesGroupsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.inventoriesInventoriesGroupsList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-inventoriesInventoriesGroupsListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventoriesInventoriesGroupsCreate - errors', () => {
      it('should have a inventoriesInventoriesGroupsCreate function', (done) => {
        try {
          assert.equal(true, typeof a.inventoriesInventoriesGroupsCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.inventoriesInventoriesGroupsCreate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-inventoriesInventoriesGroupsCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventoriesInventoriesHostsList - errors', () => {
      it('should have a inventoriesInventoriesHostsList function', (done) => {
        try {
          assert.equal(true, typeof a.inventoriesInventoriesHostsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.inventoriesInventoriesHostsList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-inventoriesInventoriesHostsListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventoriesInventoriesHostsCreate - errors', () => {
      it('should have a inventoriesInventoriesHostsCreate function', (done) => {
        try {
          assert.equal(true, typeof a.inventoriesInventoriesHostsCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.inventoriesInventoriesHostsCreate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-inventoriesInventoriesHostsCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventoriesInventoriesInstanceGroupsList - errors', () => {
      it('should have a inventoriesInventoriesInstanceGroupsList function', (done) => {
        try {
          assert.equal(true, typeof a.inventoriesInventoriesInstanceGroupsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.inventoriesInventoriesInstanceGroupsList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-inventoriesInventoriesInstanceGroupsListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventoriesInventoriesInstanceGroupsCreate - errors', () => {
      it('should have a inventoriesInventoriesInstanceGroupsCreate function', (done) => {
        try {
          assert.equal(true, typeof a.inventoriesInventoriesInstanceGroupsCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.inventoriesInventoriesInstanceGroupsCreate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-inventoriesInventoriesInstanceGroupsCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventoriesInventoriesInventorySourcesList - errors', () => {
      it('should have a inventoriesInventoriesInventorySourcesList function', (done) => {
        try {
          assert.equal(true, typeof a.inventoriesInventoriesInventorySourcesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.inventoriesInventoriesInventorySourcesList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-inventoriesInventoriesInventorySourcesListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventoriesInventoriesInventorySourcesCreate - errors', () => {
      it('should have a inventoriesInventoriesInventorySourcesCreate function', (done) => {
        try {
          assert.equal(true, typeof a.inventoriesInventoriesInventorySourcesCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.inventoriesInventoriesInventorySourcesCreate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-inventoriesInventoriesInventorySourcesCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventoriesInventoriesJobTemplatesList - errors', () => {
      it('should have a inventoriesInventoriesJobTemplatesList function', (done) => {
        try {
          assert.equal(true, typeof a.inventoriesInventoriesJobTemplatesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.inventoriesInventoriesJobTemplatesList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-inventoriesInventoriesJobTemplatesListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventoriesInventoriesObjectRolesList - errors', () => {
      it('should have a inventoriesInventoriesObjectRolesList function', (done) => {
        try {
          assert.equal(true, typeof a.inventoriesInventoriesObjectRolesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.inventoriesInventoriesObjectRolesList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-inventoriesInventoriesObjectRolesListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventoriesInventoriesRootGroupsList - errors', () => {
      it('should have a inventoriesInventoriesRootGroupsList function', (done) => {
        try {
          assert.equal(true, typeof a.inventoriesInventoriesRootGroupsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.inventoriesInventoriesRootGroupsList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-inventoriesInventoriesRootGroupsListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventoriesInventoriesRootGroupsCreate - errors', () => {
      it('should have a inventoriesInventoriesRootGroupsCreate function', (done) => {
        try {
          assert.equal(true, typeof a.inventoriesInventoriesRootGroupsCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.inventoriesInventoriesRootGroupsCreate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-inventoriesInventoriesRootGroupsCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventoriesInventoriesScriptRead - errors', () => {
      it('should have a inventoriesInventoriesScriptRead function', (done) => {
        try {
          assert.equal(true, typeof a.inventoriesInventoriesScriptRead === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.inventoriesInventoriesScriptRead(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-inventoriesInventoriesScriptRead', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventoriesInventoriesTreeRead - errors', () => {
      it('should have a inventoriesInventoriesTreeRead function', (done) => {
        try {
          assert.equal(true, typeof a.inventoriesInventoriesTreeRead === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.inventoriesInventoriesTreeRead(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-inventoriesInventoriesTreeRead', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventoriesInventoriesUpdateInventorySourcesRead - errors', () => {
      it('should have a inventoriesInventoriesUpdateInventorySourcesRead function', (done) => {
        try {
          assert.equal(true, typeof a.inventoriesInventoriesUpdateInventorySourcesRead === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.inventoriesInventoriesUpdateInventorySourcesRead(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-inventoriesInventoriesUpdateInventorySourcesRead', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventoriesInventoriesUpdateInventorySourcesCreate - errors', () => {
      it('should have a inventoriesInventoriesUpdateInventorySourcesCreate function', (done) => {
        try {
          assert.equal(true, typeof a.inventoriesInventoriesUpdateInventorySourcesCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.inventoriesInventoriesUpdateInventorySourcesCreate(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-inventoriesInventoriesUpdateInventorySourcesCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventoriesInventoriesVariableDataRead - errors', () => {
      it('should have a inventoriesInventoriesVariableDataRead function', (done) => {
        try {
          assert.equal(true, typeof a.inventoriesInventoriesVariableDataRead === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.inventoriesInventoriesVariableDataRead(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-inventoriesInventoriesVariableDataRead', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventoriesInventoriesVariableDataUpdate - errors', () => {
      it('should have a inventoriesInventoriesVariableDataUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.inventoriesInventoriesVariableDataUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.inventoriesInventoriesVariableDataUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-inventoriesInventoriesVariableDataUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventoriesInventoriesVariableDataPartialUpdate - errors', () => {
      it('should have a inventoriesInventoriesVariableDataPartialUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.inventoriesInventoriesVariableDataPartialUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.inventoriesInventoriesVariableDataPartialUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-inventoriesInventoriesVariableDataPartialUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#customInventoryScriptsInventoryScriptsList - errors', () => {
      it('should have a customInventoryScriptsInventoryScriptsList function', (done) => {
        try {
          assert.equal(true, typeof a.customInventoryScriptsInventoryScriptsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#customInventoryScriptsInventoryScriptsCreate - errors', () => {
      it('should have a customInventoryScriptsInventoryScriptsCreate function', (done) => {
        try {
          assert.equal(true, typeof a.customInventoryScriptsInventoryScriptsCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#customInventoryScriptsInventoryScriptsRead - errors', () => {
      it('should have a customInventoryScriptsInventoryScriptsRead function', (done) => {
        try {
          assert.equal(true, typeof a.customInventoryScriptsInventoryScriptsRead === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.customInventoryScriptsInventoryScriptsRead(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-customInventoryScriptsInventoryScriptsRead', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#customInventoryScriptsInventoryScriptsUpdate - errors', () => {
      it('should have a customInventoryScriptsInventoryScriptsUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.customInventoryScriptsInventoryScriptsUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.customInventoryScriptsInventoryScriptsUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-customInventoryScriptsInventoryScriptsUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#customInventoryScriptsInventoryScriptsDelete - errors', () => {
      it('should have a customInventoryScriptsInventoryScriptsDelete function', (done) => {
        try {
          assert.equal(true, typeof a.customInventoryScriptsInventoryScriptsDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.customInventoryScriptsInventoryScriptsDelete(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-customInventoryScriptsInventoryScriptsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#customInventoryScriptsInventoryScriptsPartialUpdate - errors', () => {
      it('should have a customInventoryScriptsInventoryScriptsPartialUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.customInventoryScriptsInventoryScriptsPartialUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.customInventoryScriptsInventoryScriptsPartialUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-customInventoryScriptsInventoryScriptsPartialUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#customInventoryScriptsInventoryScriptsCopyList - errors', () => {
      it('should have a customInventoryScriptsInventoryScriptsCopyList function', (done) => {
        try {
          assert.equal(true, typeof a.customInventoryScriptsInventoryScriptsCopyList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.customInventoryScriptsInventoryScriptsCopyList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-customInventoryScriptsInventoryScriptsCopyListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#customInventoryScriptsInventoryScriptsCopyCreate - errors', () => {
      it('should have a customInventoryScriptsInventoryScriptsCopyCreate function', (done) => {
        try {
          assert.equal(true, typeof a.customInventoryScriptsInventoryScriptsCopyCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.customInventoryScriptsInventoryScriptsCopyCreate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-customInventoryScriptsInventoryScriptsCopyCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#customInventoryScriptsInventoryScriptsObjectRolesList - errors', () => {
      it('should have a customInventoryScriptsInventoryScriptsObjectRolesList function', (done) => {
        try {
          assert.equal(true, typeof a.customInventoryScriptsInventoryScriptsObjectRolesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.customInventoryScriptsInventoryScriptsObjectRolesList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-customInventoryScriptsInventoryScriptsObjectRolesListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventorySourcesInventorySourcesList - errors', () => {
      it('should have a inventorySourcesInventorySourcesList function', (done) => {
        try {
          assert.equal(true, typeof a.inventorySourcesInventorySourcesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventorySourcesInventorySourcesCreate - errors', () => {
      it('should have a inventorySourcesInventorySourcesCreate function', (done) => {
        try {
          assert.equal(true, typeof a.inventorySourcesInventorySourcesCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventorySourcesInventorySourcesRead - errors', () => {
      it('should have a inventorySourcesInventorySourcesRead function', (done) => {
        try {
          assert.equal(true, typeof a.inventorySourcesInventorySourcesRead === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.inventorySourcesInventorySourcesRead(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-inventorySourcesInventorySourcesRead', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventorySourcesInventorySourcesUpdate0 - errors', () => {
      it('should have a inventorySourcesInventorySourcesUpdate0 function', (done) => {
        try {
          assert.equal(true, typeof a.inventorySourcesInventorySourcesUpdate0 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.inventorySourcesInventorySourcesUpdate0(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-inventorySourcesInventorySourcesUpdate0', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventorySourcesInventorySourcesDelete - errors', () => {
      it('should have a inventorySourcesInventorySourcesDelete function', (done) => {
        try {
          assert.equal(true, typeof a.inventorySourcesInventorySourcesDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.inventorySourcesInventorySourcesDelete(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-inventorySourcesInventorySourcesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventorySourcesInventorySourcesPartialUpdate - errors', () => {
      it('should have a inventorySourcesInventorySourcesPartialUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.inventorySourcesInventorySourcesPartialUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.inventorySourcesInventorySourcesPartialUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-inventorySourcesInventorySourcesPartialUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventorySourcesInventorySourcesActivityStreamList - errors', () => {
      it('should have a inventorySourcesInventorySourcesActivityStreamList function', (done) => {
        try {
          assert.equal(true, typeof a.inventorySourcesInventorySourcesActivityStreamList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.inventorySourcesInventorySourcesActivityStreamList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-inventorySourcesInventorySourcesActivityStreamListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventorySourcesInventorySourcesCredentialsList - errors', () => {
      it('should have a inventorySourcesInventorySourcesCredentialsList function', (done) => {
        try {
          assert.equal(true, typeof a.inventorySourcesInventorySourcesCredentialsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.inventorySourcesInventorySourcesCredentialsList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-inventorySourcesInventorySourcesCredentialsListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventorySourcesInventorySourcesCredentialsCreate - errors', () => {
      it('should have a inventorySourcesInventorySourcesCredentialsCreate function', (done) => {
        try {
          assert.equal(true, typeof a.inventorySourcesInventorySourcesCredentialsCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.inventorySourcesInventorySourcesCredentialsCreate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-inventorySourcesInventorySourcesCredentialsCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventorySourcesInventorySourcesGroupsList - errors', () => {
      it('should have a inventorySourcesInventorySourcesGroupsList function', (done) => {
        try {
          assert.equal(true, typeof a.inventorySourcesInventorySourcesGroupsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.inventorySourcesInventorySourcesGroupsList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-inventorySourcesInventorySourcesGroupsListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventorySourcesInventorySourcesGroupsDelete - errors', () => {
      it('should have a inventorySourcesInventorySourcesGroupsDelete function', (done) => {
        try {
          assert.equal(true, typeof a.inventorySourcesInventorySourcesGroupsDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.inventorySourcesInventorySourcesGroupsDelete(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-inventorySourcesInventorySourcesGroupsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventorySourcesInventorySourcesHostsList - errors', () => {
      it('should have a inventorySourcesInventorySourcesHostsList function', (done) => {
        try {
          assert.equal(true, typeof a.inventorySourcesInventorySourcesHostsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.inventorySourcesInventorySourcesHostsList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-inventorySourcesInventorySourcesHostsListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventorySourcesInventorySourcesHostsDelete - errors', () => {
      it('should have a inventorySourcesInventorySourcesHostsDelete function', (done) => {
        try {
          assert.equal(true, typeof a.inventorySourcesInventorySourcesHostsDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.inventorySourcesInventorySourcesHostsDelete(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-inventorySourcesInventorySourcesHostsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventorySourcesInventorySourcesInventoryUpdatesList - errors', () => {
      it('should have a inventorySourcesInventorySourcesInventoryUpdatesList function', (done) => {
        try {
          assert.equal(true, typeof a.inventorySourcesInventorySourcesInventoryUpdatesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.inventorySourcesInventorySourcesInventoryUpdatesList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-inventorySourcesInventorySourcesInventoryUpdatesListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventorySourcesInventorySourcesNotificationTemplatesErrorList - errors', () => {
      it('should have a inventorySourcesInventorySourcesNotificationTemplatesErrorList function', (done) => {
        try {
          assert.equal(true, typeof a.inventorySourcesInventorySourcesNotificationTemplatesErrorList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.inventorySourcesInventorySourcesNotificationTemplatesErrorList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-inventorySourcesInventorySourcesNotificationTemplatesErrorListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventorySourcesInventorySourcesNotificationTemplatesErrorCreate - errors', () => {
      it('should have a inventorySourcesInventorySourcesNotificationTemplatesErrorCreate function', (done) => {
        try {
          assert.equal(true, typeof a.inventorySourcesInventorySourcesNotificationTemplatesErrorCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.inventorySourcesInventorySourcesNotificationTemplatesErrorCreate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-inventorySourcesInventorySourcesNotificationTemplatesErrorCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventorySourcesInventorySourcesNotificationTemplatesStartedList - errors', () => {
      it('should have a inventorySourcesInventorySourcesNotificationTemplatesStartedList function', (done) => {
        try {
          assert.equal(true, typeof a.inventorySourcesInventorySourcesNotificationTemplatesStartedList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.inventorySourcesInventorySourcesNotificationTemplatesStartedList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-inventorySourcesInventorySourcesNotificationTemplatesStartedListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventorySourcesInventorySourcesNotificationTemplatesStartedCreate - errors', () => {
      it('should have a inventorySourcesInventorySourcesNotificationTemplatesStartedCreate function', (done) => {
        try {
          assert.equal(true, typeof a.inventorySourcesInventorySourcesNotificationTemplatesStartedCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.inventorySourcesInventorySourcesNotificationTemplatesStartedCreate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-inventorySourcesInventorySourcesNotificationTemplatesStartedCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventorySourcesInventorySourcesNotificationTemplatesSuccessList - errors', () => {
      it('should have a inventorySourcesInventorySourcesNotificationTemplatesSuccessList function', (done) => {
        try {
          assert.equal(true, typeof a.inventorySourcesInventorySourcesNotificationTemplatesSuccessList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.inventorySourcesInventorySourcesNotificationTemplatesSuccessList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-inventorySourcesInventorySourcesNotificationTemplatesSuccessListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventorySourcesInventorySourcesNotificationTemplatesSuccessCreate - errors', () => {
      it('should have a inventorySourcesInventorySourcesNotificationTemplatesSuccessCreate function', (done) => {
        try {
          assert.equal(true, typeof a.inventorySourcesInventorySourcesNotificationTemplatesSuccessCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.inventorySourcesInventorySourcesNotificationTemplatesSuccessCreate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-inventorySourcesInventorySourcesNotificationTemplatesSuccessCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventorySourcesInventorySourcesSchedulesList - errors', () => {
      it('should have a inventorySourcesInventorySourcesSchedulesList function', (done) => {
        try {
          assert.equal(true, typeof a.inventorySourcesInventorySourcesSchedulesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.inventorySourcesInventorySourcesSchedulesList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-inventorySourcesInventorySourcesSchedulesListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventorySourcesInventorySourcesSchedulesCreate - errors', () => {
      it('should have a inventorySourcesInventorySourcesSchedulesCreate function', (done) => {
        try {
          assert.equal(true, typeof a.inventorySourcesInventorySourcesSchedulesCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.inventorySourcesInventorySourcesSchedulesCreate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-inventorySourcesInventorySourcesSchedulesCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventorySourcesInventorySourcesUpdateRead - errors', () => {
      it('should have a inventorySourcesInventorySourcesUpdateRead function', (done) => {
        try {
          assert.equal(true, typeof a.inventorySourcesInventorySourcesUpdateRead === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.inventorySourcesInventorySourcesUpdateRead(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-inventorySourcesInventorySourcesUpdateRead', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventorySourcesInventorySourcesUpdateCreate - errors', () => {
      it('should have a inventorySourcesInventorySourcesUpdateCreate function', (done) => {
        try {
          assert.equal(true, typeof a.inventorySourcesInventorySourcesUpdateCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.inventorySourcesInventorySourcesUpdateCreate(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-inventorySourcesInventorySourcesUpdateCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventoryUpdatesInventoryUpdatesList - errors', () => {
      it('should have a inventoryUpdatesInventoryUpdatesList function', (done) => {
        try {
          assert.equal(true, typeof a.inventoryUpdatesInventoryUpdatesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventoryUpdatesInventoryUpdatesRead - errors', () => {
      it('should have a inventoryUpdatesInventoryUpdatesRead function', (done) => {
        try {
          assert.equal(true, typeof a.inventoryUpdatesInventoryUpdatesRead === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.inventoryUpdatesInventoryUpdatesRead(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-inventoryUpdatesInventoryUpdatesRead', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventoryUpdatesInventoryUpdatesDelete - errors', () => {
      it('should have a inventoryUpdatesInventoryUpdatesDelete function', (done) => {
        try {
          assert.equal(true, typeof a.inventoryUpdatesInventoryUpdatesDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.inventoryUpdatesInventoryUpdatesDelete(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-inventoryUpdatesInventoryUpdatesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventoryUpdatesInventoryUpdatesCancelRead - errors', () => {
      it('should have a inventoryUpdatesInventoryUpdatesCancelRead function', (done) => {
        try {
          assert.equal(true, typeof a.inventoryUpdatesInventoryUpdatesCancelRead === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.inventoryUpdatesInventoryUpdatesCancelRead(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-inventoryUpdatesInventoryUpdatesCancelRead', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventoryUpdatesInventoryUpdatesCancelCreate - errors', () => {
      it('should have a inventoryUpdatesInventoryUpdatesCancelCreate function', (done) => {
        try {
          assert.equal(true, typeof a.inventoryUpdatesInventoryUpdatesCancelCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.inventoryUpdatesInventoryUpdatesCancelCreate(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-inventoryUpdatesInventoryUpdatesCancelCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventoryUpdatesInventoryUpdatesCredentialsList - errors', () => {
      it('should have a inventoryUpdatesInventoryUpdatesCredentialsList function', (done) => {
        try {
          assert.equal(true, typeof a.inventoryUpdatesInventoryUpdatesCredentialsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.inventoryUpdatesInventoryUpdatesCredentialsList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-inventoryUpdatesInventoryUpdatesCredentialsListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventoryUpdatesInventoryUpdatesEventsList - errors', () => {
      it('should have a inventoryUpdatesInventoryUpdatesEventsList function', (done) => {
        try {
          assert.equal(true, typeof a.inventoryUpdatesInventoryUpdatesEventsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.inventoryUpdatesInventoryUpdatesEventsList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-inventoryUpdatesInventoryUpdatesEventsListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventoryUpdatesInventoryUpdatesNotificationsList - errors', () => {
      it('should have a inventoryUpdatesInventoryUpdatesNotificationsList function', (done) => {
        try {
          assert.equal(true, typeof a.inventoryUpdatesInventoryUpdatesNotificationsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.inventoryUpdatesInventoryUpdatesNotificationsList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-inventoryUpdatesInventoryUpdatesNotificationsListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventoryUpdatesInventoryUpdatesStdoutRead - errors', () => {
      it('should have a inventoryUpdatesInventoryUpdatesStdoutRead function', (done) => {
        try {
          assert.equal(true, typeof a.inventoryUpdatesInventoryUpdatesStdoutRead === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.inventoryUpdatesInventoryUpdatesStdoutRead(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-inventoryUpdatesInventoryUpdatesStdoutRead', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobEventsJobEventsList - errors', () => {
      it('should have a jobEventsJobEventsList function', (done) => {
        try {
          assert.equal(true, typeof a.jobEventsJobEventsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobEventsJobEventsRead - errors', () => {
      it('should have a jobEventsJobEventsRead function', (done) => {
        try {
          assert.equal(true, typeof a.jobEventsJobEventsRead === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.jobEventsJobEventsRead(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-jobEventsJobEventsRead', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobEventsJobEventsChildrenList - errors', () => {
      it('should have a jobEventsJobEventsChildrenList function', (done) => {
        try {
          assert.equal(true, typeof a.jobEventsJobEventsChildrenList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.jobEventsJobEventsChildrenList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-jobEventsJobEventsChildrenListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobEventsJobEventsHostsList - errors', () => {
      it('should have a jobEventsJobEventsHostsList function', (done) => {
        try {
          assert.equal(true, typeof a.jobEventsJobEventsHostsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.jobEventsJobEventsHostsList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-jobEventsJobEventsHostsListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobHostSummariesJobHostSummariesRead - errors', () => {
      it('should have a jobHostSummariesJobHostSummariesRead function', (done) => {
        try {
          assert.equal(true, typeof a.jobHostSummariesJobHostSummariesRead === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.jobHostSummariesJobHostSummariesRead(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-jobHostSummariesJobHostSummariesRead', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobTemplatesJobTemplatesList - errors', () => {
      it('should have a jobTemplatesJobTemplatesList function', (done) => {
        try {
          assert.equal(true, typeof a.jobTemplatesJobTemplatesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobTemplatesJobTemplatesCreate - errors', () => {
      it('should have a jobTemplatesJobTemplatesCreate function', (done) => {
        try {
          assert.equal(true, typeof a.jobTemplatesJobTemplatesCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobTemplatesJobTemplatesRead - errors', () => {
      it('should have a jobTemplatesJobTemplatesRead function', (done) => {
        try {
          assert.equal(true, typeof a.jobTemplatesJobTemplatesRead === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.jobTemplatesJobTemplatesRead(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-jobTemplatesJobTemplatesRead', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobTemplatesJobTemplatesUpdate - errors', () => {
      it('should have a jobTemplatesJobTemplatesUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.jobTemplatesJobTemplatesUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.jobTemplatesJobTemplatesUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-jobTemplatesJobTemplatesUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobTemplatesJobTemplatesDelete - errors', () => {
      it('should have a jobTemplatesJobTemplatesDelete function', (done) => {
        try {
          assert.equal(true, typeof a.jobTemplatesJobTemplatesDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.jobTemplatesJobTemplatesDelete(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-jobTemplatesJobTemplatesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobTemplatesJobTemplatesPartialUpdate - errors', () => {
      it('should have a jobTemplatesJobTemplatesPartialUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.jobTemplatesJobTemplatesPartialUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.jobTemplatesJobTemplatesPartialUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-jobTemplatesJobTemplatesPartialUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobTemplatesJobTemplatesAccessListList - errors', () => {
      it('should have a jobTemplatesJobTemplatesAccessListList function', (done) => {
        try {
          assert.equal(true, typeof a.jobTemplatesJobTemplatesAccessListList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.jobTemplatesJobTemplatesAccessListList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-jobTemplatesJobTemplatesAccessListListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobTemplatesJobTemplatesActivityStreamList - errors', () => {
      it('should have a jobTemplatesJobTemplatesActivityStreamList function', (done) => {
        try {
          assert.equal(true, typeof a.jobTemplatesJobTemplatesActivityStreamList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.jobTemplatesJobTemplatesActivityStreamList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-jobTemplatesJobTemplatesActivityStreamListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobTemplatesJobTemplatesCallbackList - errors', () => {
      it('should have a jobTemplatesJobTemplatesCallbackList function', (done) => {
        try {
          assert.equal(true, typeof a.jobTemplatesJobTemplatesCallbackList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.jobTemplatesJobTemplatesCallbackList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-jobTemplatesJobTemplatesCallbackListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobTemplatesJobTemplatesCallbackCreate - errors', () => {
      it('should have a jobTemplatesJobTemplatesCallbackCreate function', (done) => {
        try {
          assert.equal(true, typeof a.jobTemplatesJobTemplatesCallbackCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.jobTemplatesJobTemplatesCallbackCreate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-jobTemplatesJobTemplatesCallbackCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobTemplatesJobTemplatesCopyList - errors', () => {
      it('should have a jobTemplatesJobTemplatesCopyList function', (done) => {
        try {
          assert.equal(true, typeof a.jobTemplatesJobTemplatesCopyList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.jobTemplatesJobTemplatesCopyList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-jobTemplatesJobTemplatesCopyListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobTemplatesJobTemplatesCopyCreate - errors', () => {
      it('should have a jobTemplatesJobTemplatesCopyCreate function', (done) => {
        try {
          assert.equal(true, typeof a.jobTemplatesJobTemplatesCopyCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.jobTemplatesJobTemplatesCopyCreate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-jobTemplatesJobTemplatesCopyCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobTemplatesJobTemplatesCredentialsList - errors', () => {
      it('should have a jobTemplatesJobTemplatesCredentialsList function', (done) => {
        try {
          assert.equal(true, typeof a.jobTemplatesJobTemplatesCredentialsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.jobTemplatesJobTemplatesCredentialsList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-jobTemplatesJobTemplatesCredentialsListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobTemplatesJobTemplatesCredentialsCreate - errors', () => {
      it('should have a jobTemplatesJobTemplatesCredentialsCreate function', (done) => {
        try {
          assert.equal(true, typeof a.jobTemplatesJobTemplatesCredentialsCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.jobTemplatesJobTemplatesCredentialsCreate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-jobTemplatesJobTemplatesCredentialsCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobTemplatesJobTemplatesExtraCredentialsList - errors', () => {
      it('should have a jobTemplatesJobTemplatesExtraCredentialsList function', (done) => {
        try {
          assert.equal(true, typeof a.jobTemplatesJobTemplatesExtraCredentialsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.jobTemplatesJobTemplatesExtraCredentialsList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-jobTemplatesJobTemplatesExtraCredentialsListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobTemplatesJobTemplatesExtraCredentialsCreate - errors', () => {
      it('should have a jobTemplatesJobTemplatesExtraCredentialsCreate function', (done) => {
        try {
          assert.equal(true, typeof a.jobTemplatesJobTemplatesExtraCredentialsCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.jobTemplatesJobTemplatesExtraCredentialsCreate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-jobTemplatesJobTemplatesExtraCredentialsCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobTemplatesJobTemplatesInstanceGroupsList - errors', () => {
      it('should have a jobTemplatesJobTemplatesInstanceGroupsList function', (done) => {
        try {
          assert.equal(true, typeof a.jobTemplatesJobTemplatesInstanceGroupsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.jobTemplatesJobTemplatesInstanceGroupsList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-jobTemplatesJobTemplatesInstanceGroupsListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobTemplatesJobTemplatesInstanceGroupsCreate - errors', () => {
      it('should have a jobTemplatesJobTemplatesInstanceGroupsCreate function', (done) => {
        try {
          assert.equal(true, typeof a.jobTemplatesJobTemplatesInstanceGroupsCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.jobTemplatesJobTemplatesInstanceGroupsCreate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-jobTemplatesJobTemplatesInstanceGroupsCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobTemplatesJobTemplatesJobsList - errors', () => {
      it('should have a jobTemplatesJobTemplatesJobsList function', (done) => {
        try {
          assert.equal(true, typeof a.jobTemplatesJobTemplatesJobsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.jobTemplatesJobTemplatesJobsList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-jobTemplatesJobTemplatesJobsListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobTemplatesJobTemplatesLabelsList - errors', () => {
      it('should have a jobTemplatesJobTemplatesLabelsList function', (done) => {
        try {
          assert.equal(true, typeof a.jobTemplatesJobTemplatesLabelsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.jobTemplatesJobTemplatesLabelsList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-jobTemplatesJobTemplatesLabelsListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobTemplatesJobTemplatesLabelsCreate - errors', () => {
      it('should have a jobTemplatesJobTemplatesLabelsCreate function', (done) => {
        try {
          assert.equal(true, typeof a.jobTemplatesJobTemplatesLabelsCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.jobTemplatesJobTemplatesLabelsCreate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-jobTemplatesJobTemplatesLabelsCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobTemplatesJobTemplatesLaunchRead - errors', () => {
      it('should have a jobTemplatesJobTemplatesLaunchRead function', (done) => {
        try {
          assert.equal(true, typeof a.jobTemplatesJobTemplatesLaunchRead === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.jobTemplatesJobTemplatesLaunchRead(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-jobTemplatesJobTemplatesLaunchRead', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobTemplatesJobTemplatesLaunchCreate - errors', () => {
      it('should have a jobTemplatesJobTemplatesLaunchCreate function', (done) => {
        try {
          assert.equal(true, typeof a.jobTemplatesJobTemplatesLaunchCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.jobTemplatesJobTemplatesLaunchCreate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-jobTemplatesJobTemplatesLaunchCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobTemplatesJobTemplatesNotificationTemplatesErrorList - errors', () => {
      it('should have a jobTemplatesJobTemplatesNotificationTemplatesErrorList function', (done) => {
        try {
          assert.equal(true, typeof a.jobTemplatesJobTemplatesNotificationTemplatesErrorList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.jobTemplatesJobTemplatesNotificationTemplatesErrorList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-jobTemplatesJobTemplatesNotificationTemplatesErrorListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobTemplatesJobTemplatesNotificationTemplatesErrorCreate - errors', () => {
      it('should have a jobTemplatesJobTemplatesNotificationTemplatesErrorCreate function', (done) => {
        try {
          assert.equal(true, typeof a.jobTemplatesJobTemplatesNotificationTemplatesErrorCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.jobTemplatesJobTemplatesNotificationTemplatesErrorCreate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-jobTemplatesJobTemplatesNotificationTemplatesErrorCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobTemplatesJobTemplatesNotificationTemplatesStartedList - errors', () => {
      it('should have a jobTemplatesJobTemplatesNotificationTemplatesStartedList function', (done) => {
        try {
          assert.equal(true, typeof a.jobTemplatesJobTemplatesNotificationTemplatesStartedList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.jobTemplatesJobTemplatesNotificationTemplatesStartedList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-jobTemplatesJobTemplatesNotificationTemplatesStartedListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobTemplatesJobTemplatesNotificationTemplatesStartedCreate - errors', () => {
      it('should have a jobTemplatesJobTemplatesNotificationTemplatesStartedCreate function', (done) => {
        try {
          assert.equal(true, typeof a.jobTemplatesJobTemplatesNotificationTemplatesStartedCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.jobTemplatesJobTemplatesNotificationTemplatesStartedCreate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-jobTemplatesJobTemplatesNotificationTemplatesStartedCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobTemplatesJobTemplatesNotificationTemplatesSuccessList - errors', () => {
      it('should have a jobTemplatesJobTemplatesNotificationTemplatesSuccessList function', (done) => {
        try {
          assert.equal(true, typeof a.jobTemplatesJobTemplatesNotificationTemplatesSuccessList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.jobTemplatesJobTemplatesNotificationTemplatesSuccessList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-jobTemplatesJobTemplatesNotificationTemplatesSuccessListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobTemplatesJobTemplatesNotificationTemplatesSuccessCreate - errors', () => {
      it('should have a jobTemplatesJobTemplatesNotificationTemplatesSuccessCreate function', (done) => {
        try {
          assert.equal(true, typeof a.jobTemplatesJobTemplatesNotificationTemplatesSuccessCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.jobTemplatesJobTemplatesNotificationTemplatesSuccessCreate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-jobTemplatesJobTemplatesNotificationTemplatesSuccessCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobTemplatesJobTemplatesObjectRolesList - errors', () => {
      it('should have a jobTemplatesJobTemplatesObjectRolesList function', (done) => {
        try {
          assert.equal(true, typeof a.jobTemplatesJobTemplatesObjectRolesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.jobTemplatesJobTemplatesObjectRolesList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-jobTemplatesJobTemplatesObjectRolesListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobTemplatesJobTemplatesSchedulesList - errors', () => {
      it('should have a jobTemplatesJobTemplatesSchedulesList function', (done) => {
        try {
          assert.equal(true, typeof a.jobTemplatesJobTemplatesSchedulesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.jobTemplatesJobTemplatesSchedulesList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-jobTemplatesJobTemplatesSchedulesListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobTemplatesJobTemplatesSchedulesCreate - errors', () => {
      it('should have a jobTemplatesJobTemplatesSchedulesCreate function', (done) => {
        try {
          assert.equal(true, typeof a.jobTemplatesJobTemplatesSchedulesCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.jobTemplatesJobTemplatesSchedulesCreate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-jobTemplatesJobTemplatesSchedulesCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobTemplatesJobTemplatesSliceWorkflowJobsList - errors', () => {
      it('should have a jobTemplatesJobTemplatesSliceWorkflowJobsList function', (done) => {
        try {
          assert.equal(true, typeof a.jobTemplatesJobTemplatesSliceWorkflowJobsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.jobTemplatesJobTemplatesSliceWorkflowJobsList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-jobTemplatesJobTemplatesSliceWorkflowJobsListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobTemplatesJobTemplatesSliceWorkflowJobsCreate - errors', () => {
      it('should have a jobTemplatesJobTemplatesSliceWorkflowJobsCreate function', (done) => {
        try {
          assert.equal(true, typeof a.jobTemplatesJobTemplatesSliceWorkflowJobsCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.jobTemplatesJobTemplatesSliceWorkflowJobsCreate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-jobTemplatesJobTemplatesSliceWorkflowJobsCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobTemplatesJobTemplatesSurveySpecList - errors', () => {
      it('should have a jobTemplatesJobTemplatesSurveySpecList function', (done) => {
        try {
          assert.equal(true, typeof a.jobTemplatesJobTemplatesSurveySpecList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.jobTemplatesJobTemplatesSurveySpecList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-jobTemplatesJobTemplatesSurveySpecListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobTemplatesJobTemplatesSurveySpecCreate - errors', () => {
      it('should have a jobTemplatesJobTemplatesSurveySpecCreate function', (done) => {
        try {
          assert.equal(true, typeof a.jobTemplatesJobTemplatesSurveySpecCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.jobTemplatesJobTemplatesSurveySpecCreate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-jobTemplatesJobTemplatesSurveySpecCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobTemplatesJobTemplatesSurveySpecDelete - errors', () => {
      it('should have a jobTemplatesJobTemplatesSurveySpecDelete function', (done) => {
        try {
          assert.equal(true, typeof a.jobTemplatesJobTemplatesSurveySpecDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.jobTemplatesJobTemplatesSurveySpecDelete(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-jobTemplatesJobTemplatesSurveySpecDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobsJobsList - errors', () => {
      it('should have a jobsJobsList function', (done) => {
        try {
          assert.equal(true, typeof a.jobsJobsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobsJobsRead0 - errors', () => {
      it('should have a jobsJobsRead0 function', (done) => {
        try {
          assert.equal(true, typeof a.jobsJobsRead0 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.jobsJobsRead0(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-jobsJobsRead0', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobsJobsDelete - errors', () => {
      it('should have a jobsJobsDelete function', (done) => {
        try {
          assert.equal(true, typeof a.jobsJobsDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.jobsJobsDelete(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-jobsJobsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobsJobsActivityStreamList - errors', () => {
      it('should have a jobsJobsActivityStreamList function', (done) => {
        try {
          assert.equal(true, typeof a.jobsJobsActivityStreamList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.jobsJobsActivityStreamList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-jobsJobsActivityStreamListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobsJobsCancelRead - errors', () => {
      it('should have a jobsJobsCancelRead function', (done) => {
        try {
          assert.equal(true, typeof a.jobsJobsCancelRead === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.jobsJobsCancelRead(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-jobsJobsCancelRead', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobsJobsCancelCreate - errors', () => {
      it('should have a jobsJobsCancelCreate function', (done) => {
        try {
          assert.equal(true, typeof a.jobsJobsCancelCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.jobsJobsCancelCreate(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-jobsJobsCancelCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobsJobsCreateScheduleRead - errors', () => {
      it('should have a jobsJobsCreateScheduleRead function', (done) => {
        try {
          assert.equal(true, typeof a.jobsJobsCreateScheduleRead === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.jobsJobsCreateScheduleRead(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-jobsJobsCreateScheduleRead', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobsJobsCreateScheduleCreate - errors', () => {
      it('should have a jobsJobsCreateScheduleCreate function', (done) => {
        try {
          assert.equal(true, typeof a.jobsJobsCreateScheduleCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.jobsJobsCreateScheduleCreate(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-jobsJobsCreateScheduleCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobsJobsCredentialsList - errors', () => {
      it('should have a jobsJobsCredentialsList function', (done) => {
        try {
          assert.equal(true, typeof a.jobsJobsCredentialsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.jobsJobsCredentialsList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-jobsJobsCredentialsListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobsJobsExtraCredentialsList - errors', () => {
      it('should have a jobsJobsExtraCredentialsList function', (done) => {
        try {
          assert.equal(true, typeof a.jobsJobsExtraCredentialsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.jobsJobsExtraCredentialsList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-jobsJobsExtraCredentialsListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobsJobsJobEventsList - errors', () => {
      it('should have a jobsJobsJobEventsList function', (done) => {
        try {
          assert.equal(true, typeof a.jobsJobsJobEventsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.jobsJobsJobEventsList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-jobsJobsJobEventsListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobsJobsJobHostSummariesList - errors', () => {
      it('should have a jobsJobsJobHostSummariesList function', (done) => {
        try {
          assert.equal(true, typeof a.jobsJobsJobHostSummariesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.jobsJobsJobHostSummariesList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-jobsJobsJobHostSummariesListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobsJobsLabelsList - errors', () => {
      it('should have a jobsJobsLabelsList function', (done) => {
        try {
          assert.equal(true, typeof a.jobsJobsLabelsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.jobsJobsLabelsList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-jobsJobsLabelsListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobsJobsNotificationsList - errors', () => {
      it('should have a jobsJobsNotificationsList function', (done) => {
        try {
          assert.equal(true, typeof a.jobsJobsNotificationsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.jobsJobsNotificationsList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-jobsJobsNotificationsListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobsJobsRelaunchRead - errors', () => {
      it('should have a jobsJobsRelaunchRead function', (done) => {
        try {
          assert.equal(true, typeof a.jobsJobsRelaunchRead === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.jobsJobsRelaunchRead(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-jobsJobsRelaunchRead', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobsJobsRelaunchCreate - errors', () => {
      it('should have a jobsJobsRelaunchCreate function', (done) => {
        try {
          assert.equal(true, typeof a.jobsJobsRelaunchCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.jobsJobsRelaunchCreate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-jobsJobsRelaunchCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobsJobsStdoutRead - errors', () => {
      it('should have a jobsJobsStdoutRead function', (done) => {
        try {
          assert.equal(true, typeof a.jobsJobsStdoutRead === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.jobsJobsStdoutRead(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-jobsJobsStdoutRead', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#labelsLabelsList - errors', () => {
      it('should have a labelsLabelsList function', (done) => {
        try {
          assert.equal(true, typeof a.labelsLabelsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#labelsLabelsCreate - errors', () => {
      it('should have a labelsLabelsCreate function', (done) => {
        try {
          assert.equal(true, typeof a.labelsLabelsCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#labelsLabelsRead - errors', () => {
      it('should have a labelsLabelsRead function', (done) => {
        try {
          assert.equal(true, typeof a.labelsLabelsRead === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.labelsLabelsRead(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-labelsLabelsRead', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#labelsLabelsUpdate - errors', () => {
      it('should have a labelsLabelsUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.labelsLabelsUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.labelsLabelsUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-labelsLabelsUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#labelsLabelsPartialUpdate - errors', () => {
      it('should have a labelsLabelsPartialUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.labelsLabelsPartialUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.labelsLabelsPartialUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-labelsLabelsPartialUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#usersMeList - errors', () => {
      it('should have a usersMeList function', (done) => {
        try {
          assert.equal(true, typeof a.usersMeList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#usersUsersList - errors', () => {
      it('should have a usersUsersList function', (done) => {
        try {
          assert.equal(true, typeof a.usersUsersList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#usersUsersCreate - errors', () => {
      it('should have a usersUsersCreate function', (done) => {
        try {
          assert.equal(true, typeof a.usersUsersCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#usersUsersRead - errors', () => {
      it('should have a usersUsersRead function', (done) => {
        try {
          assert.equal(true, typeof a.usersUsersRead === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.usersUsersRead(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-usersUsersRead', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#usersUsersUpdate - errors', () => {
      it('should have a usersUsersUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.usersUsersUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.usersUsersUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-usersUsersUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#usersUsersDelete - errors', () => {
      it('should have a usersUsersDelete function', (done) => {
        try {
          assert.equal(true, typeof a.usersUsersDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.usersUsersDelete(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-usersUsersDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#usersUsersPartialUpdate - errors', () => {
      it('should have a usersUsersPartialUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.usersUsersPartialUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.usersUsersPartialUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-usersUsersPartialUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#usersUsersAccessListList - errors', () => {
      it('should have a usersUsersAccessListList function', (done) => {
        try {
          assert.equal(true, typeof a.usersUsersAccessListList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.usersUsersAccessListList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-usersUsersAccessListListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#usersUsersActivityStreamList - errors', () => {
      it('should have a usersUsersActivityStreamList function', (done) => {
        try {
          assert.equal(true, typeof a.usersUsersActivityStreamList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.usersUsersActivityStreamList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-usersUsersActivityStreamListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#usersUsersAdminOfOrganizationsList - errors', () => {
      it('should have a usersUsersAdminOfOrganizationsList function', (done) => {
        try {
          assert.equal(true, typeof a.usersUsersAdminOfOrganizationsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.usersUsersAdminOfOrganizationsList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-usersUsersAdminOfOrganizationsListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#usersUsersApplicationsList - errors', () => {
      it('should have a usersUsersApplicationsList function', (done) => {
        try {
          assert.equal(true, typeof a.usersUsersApplicationsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.usersUsersApplicationsList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-usersUsersApplicationsListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#usersUsersApplicationsCreate - errors', () => {
      it('should have a usersUsersApplicationsCreate function', (done) => {
        try {
          assert.equal(true, typeof a.usersUsersApplicationsCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.usersUsersApplicationsCreate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-usersUsersApplicationsCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#usersUsersAuthorizedTokensList - errors', () => {
      it('should have a usersUsersAuthorizedTokensList function', (done) => {
        try {
          assert.equal(true, typeof a.usersUsersAuthorizedTokensList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.usersUsersAuthorizedTokensList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-usersUsersAuthorizedTokensListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#usersUsersAuthorizedTokensCreate - errors', () => {
      it('should have a usersUsersAuthorizedTokensCreate function', (done) => {
        try {
          assert.equal(true, typeof a.usersUsersAuthorizedTokensCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.usersUsersAuthorizedTokensCreate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-usersUsersAuthorizedTokensCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#usersUsersCredentialsList - errors', () => {
      it('should have a usersUsersCredentialsList function', (done) => {
        try {
          assert.equal(true, typeof a.usersUsersCredentialsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.usersUsersCredentialsList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-usersUsersCredentialsListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#usersUsersCredentialsCreate - errors', () => {
      it('should have a usersUsersCredentialsCreate function', (done) => {
        try {
          assert.equal(true, typeof a.usersUsersCredentialsCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.usersUsersCredentialsCreate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-usersUsersCredentialsCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#usersUsersOrganizationsList - errors', () => {
      it('should have a usersUsersOrganizationsList function', (done) => {
        try {
          assert.equal(true, typeof a.usersUsersOrganizationsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.usersUsersOrganizationsList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-usersUsersOrganizationsListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#usersUsersPersonalTokensList - errors', () => {
      it('should have a usersUsersPersonalTokensList function', (done) => {
        try {
          assert.equal(true, typeof a.usersUsersPersonalTokensList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.usersUsersPersonalTokensList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-usersUsersPersonalTokensListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#usersUsersPersonalTokensCreate - errors', () => {
      it('should have a usersUsersPersonalTokensCreate function', (done) => {
        try {
          assert.equal(true, typeof a.usersUsersPersonalTokensCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.usersUsersPersonalTokensCreate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-usersUsersPersonalTokensCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#usersUsersProjectsList - errors', () => {
      it('should have a usersUsersProjectsList function', (done) => {
        try {
          assert.equal(true, typeof a.usersUsersProjectsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.usersUsersProjectsList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-usersUsersProjectsListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#usersUsersRolesList - errors', () => {
      it('should have a usersUsersRolesList function', (done) => {
        try {
          assert.equal(true, typeof a.usersUsersRolesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.usersUsersRolesList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-usersUsersRolesListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#usersUsersRolesCreate - errors', () => {
      it('should have a usersUsersRolesCreate function', (done) => {
        try {
          assert.equal(true, typeof a.usersUsersRolesCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.usersUsersRolesCreate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-usersUsersRolesCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#usersUsersTeamsList - errors', () => {
      it('should have a usersUsersTeamsList function', (done) => {
        try {
          assert.equal(true, typeof a.usersUsersTeamsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.usersUsersTeamsList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-usersUsersTeamsListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#usersUsersTokensList - errors', () => {
      it('should have a usersUsersTokensList function', (done) => {
        try {
          assert.equal(true, typeof a.usersUsersTokensList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.usersUsersTokensList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-usersUsersTokensListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#usersUsersTokensCreate - errors', () => {
      it('should have a usersUsersTokensCreate function', (done) => {
        try {
          assert.equal(true, typeof a.usersUsersTokensCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.usersUsersTokensCreate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-usersUsersTokensCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#metricsMetricsList - errors', () => {
      it('should have a metricsMetricsList function', (done) => {
        try {
          assert.equal(true, typeof a.metricsMetricsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#notificationTemplatesNotificationTemplatesList - errors', () => {
      it('should have a notificationTemplatesNotificationTemplatesList function', (done) => {
        try {
          assert.equal(true, typeof a.notificationTemplatesNotificationTemplatesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#notificationTemplatesNotificationTemplatesCreate - errors', () => {
      it('should have a notificationTemplatesNotificationTemplatesCreate function', (done) => {
        try {
          assert.equal(true, typeof a.notificationTemplatesNotificationTemplatesCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#notificationTemplatesNotificationTemplatesRead - errors', () => {
      it('should have a notificationTemplatesNotificationTemplatesRead function', (done) => {
        try {
          assert.equal(true, typeof a.notificationTemplatesNotificationTemplatesRead === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.notificationTemplatesNotificationTemplatesRead(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-notificationTemplatesNotificationTemplatesRead', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#notificationTemplatesNotificationTemplatesUpdate - errors', () => {
      it('should have a notificationTemplatesNotificationTemplatesUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.notificationTemplatesNotificationTemplatesUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.notificationTemplatesNotificationTemplatesUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-notificationTemplatesNotificationTemplatesUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#notificationTemplatesNotificationTemplatesDelete - errors', () => {
      it('should have a notificationTemplatesNotificationTemplatesDelete function', (done) => {
        try {
          assert.equal(true, typeof a.notificationTemplatesNotificationTemplatesDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.notificationTemplatesNotificationTemplatesDelete(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-notificationTemplatesNotificationTemplatesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#notificationTemplatesNotificationTemplatesPartialUpdate - errors', () => {
      it('should have a notificationTemplatesNotificationTemplatesPartialUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.notificationTemplatesNotificationTemplatesPartialUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.notificationTemplatesNotificationTemplatesPartialUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-notificationTemplatesNotificationTemplatesPartialUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#notificationTemplatesNotificationTemplatesCopyList - errors', () => {
      it('should have a notificationTemplatesNotificationTemplatesCopyList function', (done) => {
        try {
          assert.equal(true, typeof a.notificationTemplatesNotificationTemplatesCopyList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.notificationTemplatesNotificationTemplatesCopyList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-notificationTemplatesNotificationTemplatesCopyListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#notificationTemplatesNotificationTemplatesCopyCreate - errors', () => {
      it('should have a notificationTemplatesNotificationTemplatesCopyCreate function', (done) => {
        try {
          assert.equal(true, typeof a.notificationTemplatesNotificationTemplatesCopyCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.notificationTemplatesNotificationTemplatesCopyCreate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-notificationTemplatesNotificationTemplatesCopyCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#notificationTemplatesNotificationTemplatesNotificationsList - errors', () => {
      it('should have a notificationTemplatesNotificationTemplatesNotificationsList function', (done) => {
        try {
          assert.equal(true, typeof a.notificationTemplatesNotificationTemplatesNotificationsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.notificationTemplatesNotificationTemplatesNotificationsList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-notificationTemplatesNotificationTemplatesNotificationsListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#notificationTemplatesNotificationTemplatesTestCreate - errors', () => {
      it('should have a notificationTemplatesNotificationTemplatesTestCreate function', (done) => {
        try {
          assert.equal(true, typeof a.notificationTemplatesNotificationTemplatesTestCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.notificationTemplatesNotificationTemplatesTestCreate(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-notificationTemplatesNotificationTemplatesTestCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#notificationsNotificationsList - errors', () => {
      it('should have a notificationsNotificationsList function', (done) => {
        try {
          assert.equal(true, typeof a.notificationsNotificationsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#notificationsNotificationsRead - errors', () => {
      it('should have a notificationsNotificationsRead function', (done) => {
        try {
          assert.equal(true, typeof a.notificationsNotificationsRead === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.notificationsNotificationsRead(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-notificationsNotificationsRead', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#organizationsOrganizationsList - errors', () => {
      it('should have a organizationsOrganizationsList function', (done) => {
        try {
          assert.equal(true, typeof a.organizationsOrganizationsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#organizationsOrganizationsCreate - errors', () => {
      it('should have a organizationsOrganizationsCreate function', (done) => {
        try {
          assert.equal(true, typeof a.organizationsOrganizationsCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#organizationsOrganizationsRead - errors', () => {
      it('should have a organizationsOrganizationsRead function', (done) => {
        try {
          assert.equal(true, typeof a.organizationsOrganizationsRead === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.organizationsOrganizationsRead(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-organizationsOrganizationsRead', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#organizationsOrganizationsUpdate - errors', () => {
      it('should have a organizationsOrganizationsUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.organizationsOrganizationsUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.organizationsOrganizationsUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-organizationsOrganizationsUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#organizationsOrganizationsDelete - errors', () => {
      it('should have a organizationsOrganizationsDelete function', (done) => {
        try {
          assert.equal(true, typeof a.organizationsOrganizationsDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.organizationsOrganizationsDelete(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-organizationsOrganizationsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#organizationsOrganizationsPartialUpdate - errors', () => {
      it('should have a organizationsOrganizationsPartialUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.organizationsOrganizationsPartialUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.organizationsOrganizationsPartialUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-organizationsOrganizationsPartialUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#organizationsOrganizationsAccessListList - errors', () => {
      it('should have a organizationsOrganizationsAccessListList function', (done) => {
        try {
          assert.equal(true, typeof a.organizationsOrganizationsAccessListList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.organizationsOrganizationsAccessListList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-organizationsOrganizationsAccessListListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#organizationsOrganizationsActivityStreamList - errors', () => {
      it('should have a organizationsOrganizationsActivityStreamList function', (done) => {
        try {
          assert.equal(true, typeof a.organizationsOrganizationsActivityStreamList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.organizationsOrganizationsActivityStreamList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-organizationsOrganizationsActivityStreamListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#organizationsOrganizationsAdminsList - errors', () => {
      it('should have a organizationsOrganizationsAdminsList function', (done) => {
        try {
          assert.equal(true, typeof a.organizationsOrganizationsAdminsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.organizationsOrganizationsAdminsList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-organizationsOrganizationsAdminsListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#organizationsOrganizationsAdminsCreate - errors', () => {
      it('should have a organizationsOrganizationsAdminsCreate function', (done) => {
        try {
          assert.equal(true, typeof a.organizationsOrganizationsAdminsCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.organizationsOrganizationsAdminsCreate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-organizationsOrganizationsAdminsCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#organizationsOrganizationsApplicationsList - errors', () => {
      it('should have a organizationsOrganizationsApplicationsList function', (done) => {
        try {
          assert.equal(true, typeof a.organizationsOrganizationsApplicationsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.organizationsOrganizationsApplicationsList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-organizationsOrganizationsApplicationsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#organizationsOrganizationsApplicationsCreate - errors', () => {
      it('should have a organizationsOrganizationsApplicationsCreate function', (done) => {
        try {
          assert.equal(true, typeof a.organizationsOrganizationsApplicationsCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.organizationsOrganizationsApplicationsCreate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-organizationsOrganizationsApplicationsCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#organizationsOrganizationsCredentialsList - errors', () => {
      it('should have a organizationsOrganizationsCredentialsList function', (done) => {
        try {
          assert.equal(true, typeof a.organizationsOrganizationsCredentialsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.organizationsOrganizationsCredentialsList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-organizationsOrganizationsCredentialsListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#organizationsOrganizationsCredentialsCreate - errors', () => {
      it('should have a organizationsOrganizationsCredentialsCreate function', (done) => {
        try {
          assert.equal(true, typeof a.organizationsOrganizationsCredentialsCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.organizationsOrganizationsCredentialsCreate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-organizationsOrganizationsCredentialsCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#organizationsOrganizationsInstanceGroupsList - errors', () => {
      it('should have a organizationsOrganizationsInstanceGroupsList function', (done) => {
        try {
          assert.equal(true, typeof a.organizationsOrganizationsInstanceGroupsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.organizationsOrganizationsInstanceGroupsList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-organizationsOrganizationsInstanceGroupsListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#organizationsOrganizationsInstanceGroupsCreate - errors', () => {
      it('should have a organizationsOrganizationsInstanceGroupsCreate function', (done) => {
        try {
          assert.equal(true, typeof a.organizationsOrganizationsInstanceGroupsCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.organizationsOrganizationsInstanceGroupsCreate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-organizationsOrganizationsInstanceGroupsCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#organizationsOrganizationsInventoriesList - errors', () => {
      it('should have a organizationsOrganizationsInventoriesList function', (done) => {
        try {
          assert.equal(true, typeof a.organizationsOrganizationsInventoriesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.organizationsOrganizationsInventoriesList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-organizationsOrganizationsInventoriesListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#organizationsOrganizationsNotificationTemplatesList - errors', () => {
      it('should have a organizationsOrganizationsNotificationTemplatesList function', (done) => {
        try {
          assert.equal(true, typeof a.organizationsOrganizationsNotificationTemplatesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.organizationsOrganizationsNotificationTemplatesList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-organizationsOrganizationsNotificationTemplatesListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#organizationsOrganizationsNotificationTemplatesCreate - errors', () => {
      it('should have a organizationsOrganizationsNotificationTemplatesCreate function', (done) => {
        try {
          assert.equal(true, typeof a.organizationsOrganizationsNotificationTemplatesCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.organizationsOrganizationsNotificationTemplatesCreate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-organizationsOrganizationsNotificationTemplatesCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#organizationsOrganizationsNotificationTemplatesErrorList - errors', () => {
      it('should have a organizationsOrganizationsNotificationTemplatesErrorList function', (done) => {
        try {
          assert.equal(true, typeof a.organizationsOrganizationsNotificationTemplatesErrorList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.organizationsOrganizationsNotificationTemplatesErrorList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-organizationsOrganizationsNotificationTemplatesErrorListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#organizationsOrganizationsNotificationTemplatesErrorCreate - errors', () => {
      it('should have a organizationsOrganizationsNotificationTemplatesErrorCreate function', (done) => {
        try {
          assert.equal(true, typeof a.organizationsOrganizationsNotificationTemplatesErrorCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.organizationsOrganizationsNotificationTemplatesErrorCreate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-organizationsOrganizationsNotificationTemplatesErrorCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#organizationsOrganizationsNotificationTemplatesStartedList - errors', () => {
      it('should have a organizationsOrganizationsNotificationTemplatesStartedList function', (done) => {
        try {
          assert.equal(true, typeof a.organizationsOrganizationsNotificationTemplatesStartedList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.organizationsOrganizationsNotificationTemplatesStartedList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-organizationsOrganizationsNotificationTemplatesStartedListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#organizationsOrganizationsNotificationTemplatesStartedCreate - errors', () => {
      it('should have a organizationsOrganizationsNotificationTemplatesStartedCreate function', (done) => {
        try {
          assert.equal(true, typeof a.organizationsOrganizationsNotificationTemplatesStartedCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.organizationsOrganizationsNotificationTemplatesStartedCreate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-organizationsOrganizationsNotificationTemplatesStartedCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#organizationsOrganizationsNotificationTemplatesSuccessList - errors', () => {
      it('should have a organizationsOrganizationsNotificationTemplatesSuccessList function', (done) => {
        try {
          assert.equal(true, typeof a.organizationsOrganizationsNotificationTemplatesSuccessList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.organizationsOrganizationsNotificationTemplatesSuccessList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-organizationsOrganizationsNotificationTemplatesSuccessListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#organizationsOrganizationsNotificationTemplatesSuccessCreate - errors', () => {
      it('should have a organizationsOrganizationsNotificationTemplatesSuccessCreate function', (done) => {
        try {
          assert.equal(true, typeof a.organizationsOrganizationsNotificationTemplatesSuccessCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.organizationsOrganizationsNotificationTemplatesSuccessCreate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-organizationsOrganizationsNotificationTemplatesSuccessCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#organizationsOrganizationsObjectRolesList - errors', () => {
      it('should have a organizationsOrganizationsObjectRolesList function', (done) => {
        try {
          assert.equal(true, typeof a.organizationsOrganizationsObjectRolesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.organizationsOrganizationsObjectRolesList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-organizationsOrganizationsObjectRolesListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#organizationsOrganizationsProjectsList - errors', () => {
      it('should have a organizationsOrganizationsProjectsList function', (done) => {
        try {
          assert.equal(true, typeof a.organizationsOrganizationsProjectsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.organizationsOrganizationsProjectsList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-organizationsOrganizationsProjectsListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#organizationsOrganizationsProjectsCreate - errors', () => {
      it('should have a organizationsOrganizationsProjectsCreate function', (done) => {
        try {
          assert.equal(true, typeof a.organizationsOrganizationsProjectsCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.organizationsOrganizationsProjectsCreate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-organizationsOrganizationsProjectsCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#organizationsOrganizationsTeamsList - errors', () => {
      it('should have a organizationsOrganizationsTeamsList function', (done) => {
        try {
          assert.equal(true, typeof a.organizationsOrganizationsTeamsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.organizationsOrganizationsTeamsList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-organizationsOrganizationsTeamsListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#organizationsOrganizationsTeamsCreate - errors', () => {
      it('should have a organizationsOrganizationsTeamsCreate function', (done) => {
        try {
          assert.equal(true, typeof a.organizationsOrganizationsTeamsCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.organizationsOrganizationsTeamsCreate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-organizationsOrganizationsTeamsCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#organizationsOrganizationsUsersList - errors', () => {
      it('should have a organizationsOrganizationsUsersList function', (done) => {
        try {
          assert.equal(true, typeof a.organizationsOrganizationsUsersList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.organizationsOrganizationsUsersList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-organizationsOrganizationsUsersListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#organizationsOrganizationsUsersCreate - errors', () => {
      it('should have a organizationsOrganizationsUsersCreate function', (done) => {
        try {
          assert.equal(true, typeof a.organizationsOrganizationsUsersCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.organizationsOrganizationsUsersCreate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-organizationsOrganizationsUsersCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#organizationsOrganizationsWorkflowJobTemplatesList - errors', () => {
      it('should have a organizationsOrganizationsWorkflowJobTemplatesList function', (done) => {
        try {
          assert.equal(true, typeof a.organizationsOrganizationsWorkflowJobTemplatesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.organizationsOrganizationsWorkflowJobTemplatesList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-organizationsOrganizationsWorkflowJobTemplatesListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#organizationsOrganizationsWorkflowJobTemplatesCreate - errors', () => {
      it('should have a organizationsOrganizationsWorkflowJobTemplatesCreate function', (done) => {
        try {
          assert.equal(true, typeof a.organizationsOrganizationsWorkflowJobTemplatesCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.organizationsOrganizationsWorkflowJobTemplatesCreate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-organizationsOrganizationsWorkflowJobTemplatesCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#projectUpdatesProjectUpdatesList - errors', () => {
      it('should have a projectUpdatesProjectUpdatesList function', (done) => {
        try {
          assert.equal(true, typeof a.projectUpdatesProjectUpdatesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#projectUpdatesProjectUpdatesRead - errors', () => {
      it('should have a projectUpdatesProjectUpdatesRead function', (done) => {
        try {
          assert.equal(true, typeof a.projectUpdatesProjectUpdatesRead === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.projectUpdatesProjectUpdatesRead(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-projectUpdatesProjectUpdatesRead', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#projectUpdatesProjectUpdatesDelete - errors', () => {
      it('should have a projectUpdatesProjectUpdatesDelete function', (done) => {
        try {
          assert.equal(true, typeof a.projectUpdatesProjectUpdatesDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.projectUpdatesProjectUpdatesDelete(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-projectUpdatesProjectUpdatesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#projectUpdatesProjectUpdatesCancelRead - errors', () => {
      it('should have a projectUpdatesProjectUpdatesCancelRead function', (done) => {
        try {
          assert.equal(true, typeof a.projectUpdatesProjectUpdatesCancelRead === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.projectUpdatesProjectUpdatesCancelRead(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-projectUpdatesProjectUpdatesCancelRead', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#projectUpdatesProjectUpdatesCancelCreate - errors', () => {
      it('should have a projectUpdatesProjectUpdatesCancelCreate function', (done) => {
        try {
          assert.equal(true, typeof a.projectUpdatesProjectUpdatesCancelCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.projectUpdatesProjectUpdatesCancelCreate(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-projectUpdatesProjectUpdatesCancelCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#projectUpdatesProjectUpdatesEventsList - errors', () => {
      it('should have a projectUpdatesProjectUpdatesEventsList function', (done) => {
        try {
          assert.equal(true, typeof a.projectUpdatesProjectUpdatesEventsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.projectUpdatesProjectUpdatesEventsList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-projectUpdatesProjectUpdatesEventsListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#projectUpdatesProjectUpdatesNotificationsList - errors', () => {
      it('should have a projectUpdatesProjectUpdatesNotificationsList function', (done) => {
        try {
          assert.equal(true, typeof a.projectUpdatesProjectUpdatesNotificationsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.projectUpdatesProjectUpdatesNotificationsList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-projectUpdatesProjectUpdatesNotificationsListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#projectUpdatesProjectUpdatesScmInventoryUpdatesList - errors', () => {
      it('should have a projectUpdatesProjectUpdatesScmInventoryUpdatesList function', (done) => {
        try {
          assert.equal(true, typeof a.projectUpdatesProjectUpdatesScmInventoryUpdatesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.projectUpdatesProjectUpdatesScmInventoryUpdatesList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-projectUpdatesProjectUpdatesScmInventoryUpdatesListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#projectUpdatesProjectUpdatesStdoutRead - errors', () => {
      it('should have a projectUpdatesProjectUpdatesStdoutRead function', (done) => {
        try {
          assert.equal(true, typeof a.projectUpdatesProjectUpdatesStdoutRead === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.projectUpdatesProjectUpdatesStdoutRead(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-projectUpdatesProjectUpdatesStdoutRead', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#projectsProjectsList - errors', () => {
      it('should have a projectsProjectsList function', (done) => {
        try {
          assert.equal(true, typeof a.projectsProjectsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#projectsProjectsCreate - errors', () => {
      it('should have a projectsProjectsCreate function', (done) => {
        try {
          assert.equal(true, typeof a.projectsProjectsCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#projectsProjectsRead - errors', () => {
      it('should have a projectsProjectsRead function', (done) => {
        try {
          assert.equal(true, typeof a.projectsProjectsRead === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.projectsProjectsRead(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-projectsProjectsRead', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#projectsProjectsUpdate0 - errors', () => {
      it('should have a projectsProjectsUpdate0 function', (done) => {
        try {
          assert.equal(true, typeof a.projectsProjectsUpdate0 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.projectsProjectsUpdate0(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-projectsProjectsUpdate0', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#projectsProjectsDelete - errors', () => {
      it('should have a projectsProjectsDelete function', (done) => {
        try {
          assert.equal(true, typeof a.projectsProjectsDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.projectsProjectsDelete(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-projectsProjectsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#projectsProjectsPartialUpdate - errors', () => {
      it('should have a projectsProjectsPartialUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.projectsProjectsPartialUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.projectsProjectsPartialUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-projectsProjectsPartialUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#projectsProjectsAccessListList - errors', () => {
      it('should have a projectsProjectsAccessListList function', (done) => {
        try {
          assert.equal(true, typeof a.projectsProjectsAccessListList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.projectsProjectsAccessListList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-projectsProjectsAccessListListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#projectsProjectsActivityStreamList - errors', () => {
      it('should have a projectsProjectsActivityStreamList function', (done) => {
        try {
          assert.equal(true, typeof a.projectsProjectsActivityStreamList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.projectsProjectsActivityStreamList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-projectsProjectsActivityStreamListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#projectsProjectsCopyList - errors', () => {
      it('should have a projectsProjectsCopyList function', (done) => {
        try {
          assert.equal(true, typeof a.projectsProjectsCopyList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.projectsProjectsCopyList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-projectsProjectsCopyListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#projectsProjectsCopyCreate - errors', () => {
      it('should have a projectsProjectsCopyCreate function', (done) => {
        try {
          assert.equal(true, typeof a.projectsProjectsCopyCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.projectsProjectsCopyCreate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-projectsProjectsCopyCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#projectsProjectsInventoriesRead - errors', () => {
      it('should have a projectsProjectsInventoriesRead function', (done) => {
        try {
          assert.equal(true, typeof a.projectsProjectsInventoriesRead === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.projectsProjectsInventoriesRead(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-projectsProjectsInventoriesRead', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#projectsProjectsNotificationTemplatesErrorList - errors', () => {
      it('should have a projectsProjectsNotificationTemplatesErrorList function', (done) => {
        try {
          assert.equal(true, typeof a.projectsProjectsNotificationTemplatesErrorList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.projectsProjectsNotificationTemplatesErrorList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-projectsProjectsNotificationTemplatesErrorListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#projectsProjectsNotificationTemplatesErrorCreate - errors', () => {
      it('should have a projectsProjectsNotificationTemplatesErrorCreate function', (done) => {
        try {
          assert.equal(true, typeof a.projectsProjectsNotificationTemplatesErrorCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.projectsProjectsNotificationTemplatesErrorCreate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-projectsProjectsNotificationTemplatesErrorCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#projectsProjectsNotificationTemplatesStartedList - errors', () => {
      it('should have a projectsProjectsNotificationTemplatesStartedList function', (done) => {
        try {
          assert.equal(true, typeof a.projectsProjectsNotificationTemplatesStartedList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.projectsProjectsNotificationTemplatesStartedList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-projectsProjectsNotificationTemplatesStartedListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#projectsProjectsNotificationTemplatesStartedCreate - errors', () => {
      it('should have a projectsProjectsNotificationTemplatesStartedCreate function', (done) => {
        try {
          assert.equal(true, typeof a.projectsProjectsNotificationTemplatesStartedCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.projectsProjectsNotificationTemplatesStartedCreate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-projectsProjectsNotificationTemplatesStartedCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#projectsProjectsNotificationTemplatesSuccessList - errors', () => {
      it('should have a projectsProjectsNotificationTemplatesSuccessList function', (done) => {
        try {
          assert.equal(true, typeof a.projectsProjectsNotificationTemplatesSuccessList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.projectsProjectsNotificationTemplatesSuccessList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-projectsProjectsNotificationTemplatesSuccessListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#projectsProjectsNotificationTemplatesSuccessCreate - errors', () => {
      it('should have a projectsProjectsNotificationTemplatesSuccessCreate function', (done) => {
        try {
          assert.equal(true, typeof a.projectsProjectsNotificationTemplatesSuccessCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.projectsProjectsNotificationTemplatesSuccessCreate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-projectsProjectsNotificationTemplatesSuccessCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#projectsProjectsObjectRolesList - errors', () => {
      it('should have a projectsProjectsObjectRolesList function', (done) => {
        try {
          assert.equal(true, typeof a.projectsProjectsObjectRolesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.projectsProjectsObjectRolesList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-projectsProjectsObjectRolesListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#projectsProjectsPlaybooksRead - errors', () => {
      it('should have a projectsProjectsPlaybooksRead function', (done) => {
        try {
          assert.equal(true, typeof a.projectsProjectsPlaybooksRead === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.projectsProjectsPlaybooksRead(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-projectsProjectsPlaybooksRead', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#projectsProjectsProjectUpdatesList - errors', () => {
      it('should have a projectsProjectsProjectUpdatesList function', (done) => {
        try {
          assert.equal(true, typeof a.projectsProjectsProjectUpdatesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.projectsProjectsProjectUpdatesList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-projectsProjectsProjectUpdatesListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#projectsProjectsSchedulesList - errors', () => {
      it('should have a projectsProjectsSchedulesList function', (done) => {
        try {
          assert.equal(true, typeof a.projectsProjectsSchedulesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.projectsProjectsSchedulesList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-projectsProjectsSchedulesListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#projectsProjectsSchedulesCreate - errors', () => {
      it('should have a projectsProjectsSchedulesCreate function', (done) => {
        try {
          assert.equal(true, typeof a.projectsProjectsSchedulesCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.projectsProjectsSchedulesCreate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-projectsProjectsSchedulesCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#projectsProjectsScmInventorySourcesList - errors', () => {
      it('should have a projectsProjectsScmInventorySourcesList function', (done) => {
        try {
          assert.equal(true, typeof a.projectsProjectsScmInventorySourcesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.projectsProjectsScmInventorySourcesList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-projectsProjectsScmInventorySourcesListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#projectsProjectsTeamsList - errors', () => {
      it('should have a projectsProjectsTeamsList function', (done) => {
        try {
          assert.equal(true, typeof a.projectsProjectsTeamsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.projectsProjectsTeamsList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-projectsProjectsTeamsListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#projectsProjectsUpdateRead - errors', () => {
      it('should have a projectsProjectsUpdateRead function', (done) => {
        try {
          assert.equal(true, typeof a.projectsProjectsUpdateRead === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.projectsProjectsUpdateRead(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-projectsProjectsUpdateRead', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#projectsProjectsUpdateCreate - errors', () => {
      it('should have a projectsProjectsUpdateCreate function', (done) => {
        try {
          assert.equal(true, typeof a.projectsProjectsUpdateCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.projectsProjectsUpdateCreate(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-projectsProjectsUpdateCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rolesRolesList - errors', () => {
      it('should have a rolesRolesList function', (done) => {
        try {
          assert.equal(true, typeof a.rolesRolesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rolesRolesRead - errors', () => {
      it('should have a rolesRolesRead function', (done) => {
        try {
          assert.equal(true, typeof a.rolesRolesRead === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rolesRolesRead(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-rolesRolesRead', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rolesRolesChildrenList - errors', () => {
      it('should have a rolesRolesChildrenList function', (done) => {
        try {
          assert.equal(true, typeof a.rolesRolesChildrenList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rolesRolesChildrenList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-rolesRolesChildrenListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rolesRolesParentsList - errors', () => {
      it('should have a rolesRolesParentsList function', (done) => {
        try {
          assert.equal(true, typeof a.rolesRolesParentsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rolesRolesParentsList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-rolesRolesParentsListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rolesRolesTeamsList - errors', () => {
      it('should have a rolesRolesTeamsList function', (done) => {
        try {
          assert.equal(true, typeof a.rolesRolesTeamsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rolesRolesTeamsList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-rolesRolesTeamsListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rolesRolesTeamsCreate - errors', () => {
      it('should have a rolesRolesTeamsCreate function', (done) => {
        try {
          assert.equal(true, typeof a.rolesRolesTeamsCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rolesRolesTeamsCreate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-rolesRolesTeamsCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rolesRolesUsersList - errors', () => {
      it('should have a rolesRolesUsersList function', (done) => {
        try {
          assert.equal(true, typeof a.rolesRolesUsersList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rolesRolesUsersList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-rolesRolesUsersListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rolesRolesUsersCreate - errors', () => {
      it('should have a rolesRolesUsersCreate function', (done) => {
        try {
          assert.equal(true, typeof a.rolesRolesUsersCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rolesRolesUsersCreate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-rolesRolesUsersCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#schedulesSchedulesList - errors', () => {
      it('should have a schedulesSchedulesList function', (done) => {
        try {
          assert.equal(true, typeof a.schedulesSchedulesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#schedulesSchedulesCreate - errors', () => {
      it('should have a schedulesSchedulesCreate function', (done) => {
        try {
          assert.equal(true, typeof a.schedulesSchedulesCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#schedulesSchedulesPreviewCreate - errors', () => {
      it('should have a schedulesSchedulesPreviewCreate function', (done) => {
        try {
          assert.equal(true, typeof a.schedulesSchedulesPreviewCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#schedulesSchedulesZoneinfoList - errors', () => {
      it('should have a schedulesSchedulesZoneinfoList function', (done) => {
        try {
          assert.equal(true, typeof a.schedulesSchedulesZoneinfoList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#schedulesSchedulesRead - errors', () => {
      it('should have a schedulesSchedulesRead function', (done) => {
        try {
          assert.equal(true, typeof a.schedulesSchedulesRead === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.schedulesSchedulesRead(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-schedulesSchedulesRead', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#schedulesSchedulesUpdate - errors', () => {
      it('should have a schedulesSchedulesUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.schedulesSchedulesUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.schedulesSchedulesUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-schedulesSchedulesUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#schedulesSchedulesDelete - errors', () => {
      it('should have a schedulesSchedulesDelete function', (done) => {
        try {
          assert.equal(true, typeof a.schedulesSchedulesDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.schedulesSchedulesDelete(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-schedulesSchedulesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#schedulesSchedulesPartialUpdate - errors', () => {
      it('should have a schedulesSchedulesPartialUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.schedulesSchedulesPartialUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.schedulesSchedulesPartialUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-schedulesSchedulesPartialUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#schedulesSchedulesCredentialsList - errors', () => {
      it('should have a schedulesSchedulesCredentialsList function', (done) => {
        try {
          assert.equal(true, typeof a.schedulesSchedulesCredentialsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.schedulesSchedulesCredentialsList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-schedulesSchedulesCredentialsListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#schedulesSchedulesCredentialsCreate - errors', () => {
      it('should have a schedulesSchedulesCredentialsCreate function', (done) => {
        try {
          assert.equal(true, typeof a.schedulesSchedulesCredentialsCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.schedulesSchedulesCredentialsCreate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-schedulesSchedulesCredentialsCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#schedulesSchedulesJobsList - errors', () => {
      it('should have a schedulesSchedulesJobsList function', (done) => {
        try {
          assert.equal(true, typeof a.schedulesSchedulesJobsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.schedulesSchedulesJobsList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-schedulesSchedulesJobsListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#settingsSettingsList - errors', () => {
      it('should have a settingsSettingsList function', (done) => {
        try {
          assert.equal(true, typeof a.settingsSettingsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#settingsSettingsLoggingTestCreate - errors', () => {
      it('should have a settingsSettingsLoggingTestCreate function', (done) => {
        try {
          assert.equal(true, typeof a.settingsSettingsLoggingTestCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#settingsSettingsRead - errors', () => {
      it('should have a settingsSettingsRead function', (done) => {
        try {
          assert.equal(true, typeof a.settingsSettingsRead === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing categorySlug', (done) => {
        try {
          a.settingsSettingsRead(null, (data, error) => {
            try {
              const displayE = 'categorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-settingsSettingsRead', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#settingsSettingsUpdate - errors', () => {
      it('should have a settingsSettingsUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.settingsSettingsUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing categorySlug', (done) => {
        try {
          a.settingsSettingsUpdate(null, null, (data, error) => {
            try {
              const displayE = 'categorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-settingsSettingsUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#settingsSettingsDelete - errors', () => {
      it('should have a settingsSettingsDelete function', (done) => {
        try {
          assert.equal(true, typeof a.settingsSettingsDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing categorySlug', (done) => {
        try {
          a.settingsSettingsDelete(null, (data, error) => {
            try {
              const displayE = 'categorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-settingsSettingsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#settingsSettingsPartialUpdate - errors', () => {
      it('should have a settingsSettingsPartialUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.settingsSettingsPartialUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing categorySlug', (done) => {
        try {
          a.settingsSettingsPartialUpdate(null, null, (data, error) => {
            try {
              const displayE = 'categorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-settingsSettingsPartialUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#systemJobTemplatesSystemJobTemplatesList - errors', () => {
      it('should have a systemJobTemplatesSystemJobTemplatesList function', (done) => {
        try {
          assert.equal(true, typeof a.systemJobTemplatesSystemJobTemplatesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#systemJobTemplatesSystemJobTemplatesRead - errors', () => {
      it('should have a systemJobTemplatesSystemJobTemplatesRead function', (done) => {
        try {
          assert.equal(true, typeof a.systemJobTemplatesSystemJobTemplatesRead === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.systemJobTemplatesSystemJobTemplatesRead(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-systemJobTemplatesSystemJobTemplatesRead', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#systemJobTemplatesSystemJobTemplatesJobsList - errors', () => {
      it('should have a systemJobTemplatesSystemJobTemplatesJobsList function', (done) => {
        try {
          assert.equal(true, typeof a.systemJobTemplatesSystemJobTemplatesJobsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.systemJobTemplatesSystemJobTemplatesJobsList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-systemJobTemplatesSystemJobTemplatesJobsListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#systemJobTemplatesSystemJobTemplatesLaunchList - errors', () => {
      it('should have a systemJobTemplatesSystemJobTemplatesLaunchList function', (done) => {
        try {
          assert.equal(true, typeof a.systemJobTemplatesSystemJobTemplatesLaunchList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.systemJobTemplatesSystemJobTemplatesLaunchList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-systemJobTemplatesSystemJobTemplatesLaunchListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#systemJobTemplatesSystemJobTemplatesLaunchCreate - errors', () => {
      it('should have a systemJobTemplatesSystemJobTemplatesLaunchCreate function', (done) => {
        try {
          assert.equal(true, typeof a.systemJobTemplatesSystemJobTemplatesLaunchCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.systemJobTemplatesSystemJobTemplatesLaunchCreate(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-systemJobTemplatesSystemJobTemplatesLaunchCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#systemJobTemplatesSystemJobTemplatesNotificationTemplatesErrorList - errors', () => {
      it('should have a systemJobTemplatesSystemJobTemplatesNotificationTemplatesErrorList function', (done) => {
        try {
          assert.equal(true, typeof a.systemJobTemplatesSystemJobTemplatesNotificationTemplatesErrorList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.systemJobTemplatesSystemJobTemplatesNotificationTemplatesErrorList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-systemJobTemplatesSystemJobTemplatesNotificationTemplatesErrorListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#systemJobTemplatesSystemJobTemplatesNotificationTemplatesErrorCreate - errors', () => {
      it('should have a systemJobTemplatesSystemJobTemplatesNotificationTemplatesErrorCreate function', (done) => {
        try {
          assert.equal(true, typeof a.systemJobTemplatesSystemJobTemplatesNotificationTemplatesErrorCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.systemJobTemplatesSystemJobTemplatesNotificationTemplatesErrorCreate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-systemJobTemplatesSystemJobTemplatesNotificationTemplatesErrorCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#systemJobTemplatesSystemJobTemplatesNotificationTemplatesStartedList - errors', () => {
      it('should have a systemJobTemplatesSystemJobTemplatesNotificationTemplatesStartedList function', (done) => {
        try {
          assert.equal(true, typeof a.systemJobTemplatesSystemJobTemplatesNotificationTemplatesStartedList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.systemJobTemplatesSystemJobTemplatesNotificationTemplatesStartedList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-systemJobTemplatesSystemJobTemplatesNotificationTemplatesStartedListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#systemJobTemplatesSystemJobTemplatesNotificationTemplatesStartedCreate - errors', () => {
      it('should have a systemJobTemplatesSystemJobTemplatesNotificationTemplatesStartedCreate function', (done) => {
        try {
          assert.equal(true, typeof a.systemJobTemplatesSystemJobTemplatesNotificationTemplatesStartedCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.systemJobTemplatesSystemJobTemplatesNotificationTemplatesStartedCreate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-systemJobTemplatesSystemJobTemplatesNotificationTemplatesStartedCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#systemJobTemplatesSystemJobTemplatesNotificationTemplatesSuccessList - errors', () => {
      it('should have a systemJobTemplatesSystemJobTemplatesNotificationTemplatesSuccessList function', (done) => {
        try {
          assert.equal(true, typeof a.systemJobTemplatesSystemJobTemplatesNotificationTemplatesSuccessList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.systemJobTemplatesSystemJobTemplatesNotificationTemplatesSuccessList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-systemJobTemplatesSystemJobTemplatesNotificationTemplatesSuccessListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#systemJobTemplatesSystemJobTemplatesNotificationTemplatesSuccessCreate - errors', () => {
      it('should have a systemJobTemplatesSystemJobTemplatesNotificationTemplatesSuccessCreate function', (done) => {
        try {
          assert.equal(true, typeof a.systemJobTemplatesSystemJobTemplatesNotificationTemplatesSuccessCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.systemJobTemplatesSystemJobTemplatesNotificationTemplatesSuccessCreate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-systemJobTemplatesSystemJobTemplatesNotificationTemplatesSuccessCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#systemJobTemplatesSystemJobTemplatesSchedulesList - errors', () => {
      it('should have a systemJobTemplatesSystemJobTemplatesSchedulesList function', (done) => {
        try {
          assert.equal(true, typeof a.systemJobTemplatesSystemJobTemplatesSchedulesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.systemJobTemplatesSystemJobTemplatesSchedulesList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-systemJobTemplatesSystemJobTemplatesSchedulesListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#systemJobTemplatesSystemJobTemplatesSchedulesCreate - errors', () => {
      it('should have a systemJobTemplatesSystemJobTemplatesSchedulesCreate function', (done) => {
        try {
          assert.equal(true, typeof a.systemJobTemplatesSystemJobTemplatesSchedulesCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.systemJobTemplatesSystemJobTemplatesSchedulesCreate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-systemJobTemplatesSystemJobTemplatesSchedulesCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#systemJobsSystemJobsList - errors', () => {
      it('should have a systemJobsSystemJobsList function', (done) => {
        try {
          assert.equal(true, typeof a.systemJobsSystemJobsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#systemJobsSystemJobsCreate - errors', () => {
      it('should have a systemJobsSystemJobsCreate function', (done) => {
        try {
          assert.equal(true, typeof a.systemJobsSystemJobsCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#systemJobsSystemJobsRead - errors', () => {
      it('should have a systemJobsSystemJobsRead function', (done) => {
        try {
          assert.equal(true, typeof a.systemJobsSystemJobsRead === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.systemJobsSystemJobsRead(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-systemJobsSystemJobsRead', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#systemJobsSystemJobsDelete - errors', () => {
      it('should have a systemJobsSystemJobsDelete function', (done) => {
        try {
          assert.equal(true, typeof a.systemJobsSystemJobsDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.systemJobsSystemJobsDelete(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-systemJobsSystemJobsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#systemJobsSystemJobsCancelRead - errors', () => {
      it('should have a systemJobsSystemJobsCancelRead function', (done) => {
        try {
          assert.equal(true, typeof a.systemJobsSystemJobsCancelRead === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.systemJobsSystemJobsCancelRead(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-systemJobsSystemJobsCancelRead', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#systemJobsSystemJobsCancelCreate - errors', () => {
      it('should have a systemJobsSystemJobsCancelCreate function', (done) => {
        try {
          assert.equal(true, typeof a.systemJobsSystemJobsCancelCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.systemJobsSystemJobsCancelCreate(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-systemJobsSystemJobsCancelCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#systemJobsSystemJobsEventsList - errors', () => {
      it('should have a systemJobsSystemJobsEventsList function', (done) => {
        try {
          assert.equal(true, typeof a.systemJobsSystemJobsEventsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.systemJobsSystemJobsEventsList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-systemJobsSystemJobsEventsListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#systemJobsSystemJobsNotificationsList - errors', () => {
      it('should have a systemJobsSystemJobsNotificationsList function', (done) => {
        try {
          assert.equal(true, typeof a.systemJobsSystemJobsNotificationsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.systemJobsSystemJobsNotificationsList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-systemJobsSystemJobsNotificationsListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#teamsTeamsList - errors', () => {
      it('should have a teamsTeamsList function', (done) => {
        try {
          assert.equal(true, typeof a.teamsTeamsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#teamsTeamsCreate - errors', () => {
      it('should have a teamsTeamsCreate function', (done) => {
        try {
          assert.equal(true, typeof a.teamsTeamsCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#teamsTeamsRead - errors', () => {
      it('should have a teamsTeamsRead function', (done) => {
        try {
          assert.equal(true, typeof a.teamsTeamsRead === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.teamsTeamsRead(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-teamsTeamsRead', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#teamsTeamsUpdate - errors', () => {
      it('should have a teamsTeamsUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.teamsTeamsUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.teamsTeamsUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-teamsTeamsUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#teamsTeamsDelete - errors', () => {
      it('should have a teamsTeamsDelete function', (done) => {
        try {
          assert.equal(true, typeof a.teamsTeamsDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.teamsTeamsDelete(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-teamsTeamsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#teamsTeamsPartialUpdate - errors', () => {
      it('should have a teamsTeamsPartialUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.teamsTeamsPartialUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.teamsTeamsPartialUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-teamsTeamsPartialUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#teamsTeamsAccessListList - errors', () => {
      it('should have a teamsTeamsAccessListList function', (done) => {
        try {
          assert.equal(true, typeof a.teamsTeamsAccessListList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.teamsTeamsAccessListList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-teamsTeamsAccessListListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#teamsTeamsActivityStreamList - errors', () => {
      it('should have a teamsTeamsActivityStreamList function', (done) => {
        try {
          assert.equal(true, typeof a.teamsTeamsActivityStreamList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.teamsTeamsActivityStreamList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-teamsTeamsActivityStreamListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#teamsTeamsCredentialsList - errors', () => {
      it('should have a teamsTeamsCredentialsList function', (done) => {
        try {
          assert.equal(true, typeof a.teamsTeamsCredentialsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.teamsTeamsCredentialsList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-teamsTeamsCredentialsListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#teamsTeamsCredentialsCreate - errors', () => {
      it('should have a teamsTeamsCredentialsCreate function', (done) => {
        try {
          assert.equal(true, typeof a.teamsTeamsCredentialsCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.teamsTeamsCredentialsCreate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-teamsTeamsCredentialsCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#teamsTeamsObjectRolesList - errors', () => {
      it('should have a teamsTeamsObjectRolesList function', (done) => {
        try {
          assert.equal(true, typeof a.teamsTeamsObjectRolesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.teamsTeamsObjectRolesList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-teamsTeamsObjectRolesListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#teamsTeamsProjectsList - errors', () => {
      it('should have a teamsTeamsProjectsList function', (done) => {
        try {
          assert.equal(true, typeof a.teamsTeamsProjectsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.teamsTeamsProjectsList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-teamsTeamsProjectsListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#teamsTeamsRolesList - errors', () => {
      it('should have a teamsTeamsRolesList function', (done) => {
        try {
          assert.equal(true, typeof a.teamsTeamsRolesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.teamsTeamsRolesList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-teamsTeamsRolesListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#teamsTeamsRolesCreate - errors', () => {
      it('should have a teamsTeamsRolesCreate function', (done) => {
        try {
          assert.equal(true, typeof a.teamsTeamsRolesCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.teamsTeamsRolesCreate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-teamsTeamsRolesCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#teamsTeamsUsersList - errors', () => {
      it('should have a teamsTeamsUsersList function', (done) => {
        try {
          assert.equal(true, typeof a.teamsTeamsUsersList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.teamsTeamsUsersList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-teamsTeamsUsersListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#teamsTeamsUsersCreate - errors', () => {
      it('should have a teamsTeamsUsersCreate function', (done) => {
        try {
          assert.equal(true, typeof a.teamsTeamsUsersCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.teamsTeamsUsersCreate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-teamsTeamsUsersCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#unifiedJobTemplatesUnifiedJobTemplatesList - errors', () => {
      it('should have a unifiedJobTemplatesUnifiedJobTemplatesList function', (done) => {
        try {
          assert.equal(true, typeof a.unifiedJobTemplatesUnifiedJobTemplatesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#unifiedJobsUnifiedJobsList - errors', () => {
      it('should have a unifiedJobsUnifiedJobsList function', (done) => {
        try {
          assert.equal(true, typeof a.unifiedJobsUnifiedJobsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobNodesWorkflowJobNodesList - errors', () => {
      it('should have a workflowJobNodesWorkflowJobNodesList function', (done) => {
        try {
          assert.equal(true, typeof a.workflowJobNodesWorkflowJobNodesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobNodesWorkflowJobNodesRead - errors', () => {
      it('should have a workflowJobNodesWorkflowJobNodesRead function', (done) => {
        try {
          assert.equal(true, typeof a.workflowJobNodesWorkflowJobNodesRead === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.workflowJobNodesWorkflowJobNodesRead(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-workflowJobNodesWorkflowJobNodesRead', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobNodesWorkflowJobNodesAlwaysNodesList - errors', () => {
      it('should have a workflowJobNodesWorkflowJobNodesAlwaysNodesList function', (done) => {
        try {
          assert.equal(true, typeof a.workflowJobNodesWorkflowJobNodesAlwaysNodesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.workflowJobNodesWorkflowJobNodesAlwaysNodesList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-workflowJobNodesWorkflowJobNodesAlwaysNodesListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobNodesWorkflowJobNodesCredentialsList - errors', () => {
      it('should have a workflowJobNodesWorkflowJobNodesCredentialsList function', (done) => {
        try {
          assert.equal(true, typeof a.workflowJobNodesWorkflowJobNodesCredentialsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.workflowJobNodesWorkflowJobNodesCredentialsList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-workflowJobNodesWorkflowJobNodesCredentialsListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobNodesWorkflowJobNodesFailureNodesList - errors', () => {
      it('should have a workflowJobNodesWorkflowJobNodesFailureNodesList function', (done) => {
        try {
          assert.equal(true, typeof a.workflowJobNodesWorkflowJobNodesFailureNodesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.workflowJobNodesWorkflowJobNodesFailureNodesList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-workflowJobNodesWorkflowJobNodesFailureNodesListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobNodesWorkflowJobNodesSuccessNodesList - errors', () => {
      it('should have a workflowJobNodesWorkflowJobNodesSuccessNodesList function', (done) => {
        try {
          assert.equal(true, typeof a.workflowJobNodesWorkflowJobNodesSuccessNodesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.workflowJobNodesWorkflowJobNodesSuccessNodesList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-workflowJobNodesWorkflowJobNodesSuccessNodesListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobTemplateNodesWorkflowJobTemplateNodesList - errors', () => {
      it('should have a workflowJobTemplateNodesWorkflowJobTemplateNodesList function', (done) => {
        try {
          assert.equal(true, typeof a.workflowJobTemplateNodesWorkflowJobTemplateNodesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobTemplateNodesWorkflowJobTemplateNodesCreate - errors', () => {
      it('should have a workflowJobTemplateNodesWorkflowJobTemplateNodesCreate function', (done) => {
        try {
          assert.equal(true, typeof a.workflowJobTemplateNodesWorkflowJobTemplateNodesCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobTemplateNodesWorkflowJobTemplateNodesRead - errors', () => {
      it('should have a workflowJobTemplateNodesWorkflowJobTemplateNodesRead function', (done) => {
        try {
          assert.equal(true, typeof a.workflowJobTemplateNodesWorkflowJobTemplateNodesRead === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.workflowJobTemplateNodesWorkflowJobTemplateNodesRead(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-workflowJobTemplateNodesWorkflowJobTemplateNodesRead', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobTemplateNodesWorkflowJobTemplateNodesUpdate - errors', () => {
      it('should have a workflowJobTemplateNodesWorkflowJobTemplateNodesUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.workflowJobTemplateNodesWorkflowJobTemplateNodesUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.workflowJobTemplateNodesWorkflowJobTemplateNodesUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-workflowJobTemplateNodesWorkflowJobTemplateNodesUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobTemplateNodesWorkflowJobTemplateNodesDelete - errors', () => {
      it('should have a workflowJobTemplateNodesWorkflowJobTemplateNodesDelete function', (done) => {
        try {
          assert.equal(true, typeof a.workflowJobTemplateNodesWorkflowJobTemplateNodesDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.workflowJobTemplateNodesWorkflowJobTemplateNodesDelete(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-workflowJobTemplateNodesWorkflowJobTemplateNodesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobTemplateNodesWorkflowJobTemplateNodesPartialUpdate - errors', () => {
      it('should have a workflowJobTemplateNodesWorkflowJobTemplateNodesPartialUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.workflowJobTemplateNodesWorkflowJobTemplateNodesPartialUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.workflowJobTemplateNodesWorkflowJobTemplateNodesPartialUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-workflowJobTemplateNodesWorkflowJobTemplateNodesPartialUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobTemplateNodesWorkflowJobTemplateNodesAlwaysNodesList - errors', () => {
      it('should have a workflowJobTemplateNodesWorkflowJobTemplateNodesAlwaysNodesList function', (done) => {
        try {
          assert.equal(true, typeof a.workflowJobTemplateNodesWorkflowJobTemplateNodesAlwaysNodesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.workflowJobTemplateNodesWorkflowJobTemplateNodesAlwaysNodesList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-workflowJobTemplateNodesWorkflowJobTemplateNodesAlwaysNodesListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobTemplateNodesWorkflowJobTemplateNodesAlwaysNodesCreate - errors', () => {
      it('should have a workflowJobTemplateNodesWorkflowJobTemplateNodesAlwaysNodesCreate function', (done) => {
        try {
          assert.equal(true, typeof a.workflowJobTemplateNodesWorkflowJobTemplateNodesAlwaysNodesCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.workflowJobTemplateNodesWorkflowJobTemplateNodesAlwaysNodesCreate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-workflowJobTemplateNodesWorkflowJobTemplateNodesAlwaysNodesCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobTemplateNodesWorkflowJobTemplateNodesCredentialsList - errors', () => {
      it('should have a workflowJobTemplateNodesWorkflowJobTemplateNodesCredentialsList function', (done) => {
        try {
          assert.equal(true, typeof a.workflowJobTemplateNodesWorkflowJobTemplateNodesCredentialsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.workflowJobTemplateNodesWorkflowJobTemplateNodesCredentialsList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-workflowJobTemplateNodesWorkflowJobTemplateNodesCredentialsListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobTemplateNodesWorkflowJobTemplateNodesCredentialsCreate - errors', () => {
      it('should have a workflowJobTemplateNodesWorkflowJobTemplateNodesCredentialsCreate function', (done) => {
        try {
          assert.equal(true, typeof a.workflowJobTemplateNodesWorkflowJobTemplateNodesCredentialsCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.workflowJobTemplateNodesWorkflowJobTemplateNodesCredentialsCreate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-workflowJobTemplateNodesWorkflowJobTemplateNodesCredentialsCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobTemplateNodesWorkflowJobTemplateNodesFailureNodesList - errors', () => {
      it('should have a workflowJobTemplateNodesWorkflowJobTemplateNodesFailureNodesList function', (done) => {
        try {
          assert.equal(true, typeof a.workflowJobTemplateNodesWorkflowJobTemplateNodesFailureNodesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.workflowJobTemplateNodesWorkflowJobTemplateNodesFailureNodesList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-workflowJobTemplateNodesWorkflowJobTemplateNodesFailureNodesListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobTemplateNodesWorkflowJobTemplateNodesFailureNodesCreate - errors', () => {
      it('should have a workflowJobTemplateNodesWorkflowJobTemplateNodesFailureNodesCreate function', (done) => {
        try {
          assert.equal(true, typeof a.workflowJobTemplateNodesWorkflowJobTemplateNodesFailureNodesCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.workflowJobTemplateNodesWorkflowJobTemplateNodesFailureNodesCreate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-workflowJobTemplateNodesWorkflowJobTemplateNodesFailureNodesCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobTemplateNodesWorkflowJobTemplateNodesSuccessNodesList - errors', () => {
      it('should have a workflowJobTemplateNodesWorkflowJobTemplateNodesSuccessNodesList function', (done) => {
        try {
          assert.equal(true, typeof a.workflowJobTemplateNodesWorkflowJobTemplateNodesSuccessNodesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.workflowJobTemplateNodesWorkflowJobTemplateNodesSuccessNodesList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-workflowJobTemplateNodesWorkflowJobTemplateNodesSuccessNodesListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobTemplateNodesWorkflowJobTemplateNodesSuccessNodesCreate - errors', () => {
      it('should have a workflowJobTemplateNodesWorkflowJobTemplateNodesSuccessNodesCreate function', (done) => {
        try {
          assert.equal(true, typeof a.workflowJobTemplateNodesWorkflowJobTemplateNodesSuccessNodesCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.workflowJobTemplateNodesWorkflowJobTemplateNodesSuccessNodesCreate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-workflowJobTemplateNodesWorkflowJobTemplateNodesSuccessNodesCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobTemplatesWorkflowJobTemplatesList - errors', () => {
      it('should have a workflowJobTemplatesWorkflowJobTemplatesList function', (done) => {
        try {
          assert.equal(true, typeof a.workflowJobTemplatesWorkflowJobTemplatesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobTemplatesWorkflowJobTemplatesCreate - errors', () => {
      it('should have a workflowJobTemplatesWorkflowJobTemplatesCreate function', (done) => {
        try {
          assert.equal(true, typeof a.workflowJobTemplatesWorkflowJobTemplatesCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobTemplatesWorkflowJobTemplatesRead - errors', () => {
      it('should have a workflowJobTemplatesWorkflowJobTemplatesRead function', (done) => {
        try {
          assert.equal(true, typeof a.workflowJobTemplatesWorkflowJobTemplatesRead === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.workflowJobTemplatesWorkflowJobTemplatesRead(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-workflowJobTemplatesWorkflowJobTemplatesRead', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobTemplatesWorkflowJobTemplatesUpdate - errors', () => {
      it('should have a workflowJobTemplatesWorkflowJobTemplatesUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.workflowJobTemplatesWorkflowJobTemplatesUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.workflowJobTemplatesWorkflowJobTemplatesUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-workflowJobTemplatesWorkflowJobTemplatesUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobTemplatesWorkflowJobTemplatesDelete - errors', () => {
      it('should have a workflowJobTemplatesWorkflowJobTemplatesDelete function', (done) => {
        try {
          assert.equal(true, typeof a.workflowJobTemplatesWorkflowJobTemplatesDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.workflowJobTemplatesWorkflowJobTemplatesDelete(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-workflowJobTemplatesWorkflowJobTemplatesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobTemplatesWorkflowJobTemplatesPartialUpdate - errors', () => {
      it('should have a workflowJobTemplatesWorkflowJobTemplatesPartialUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.workflowJobTemplatesWorkflowJobTemplatesPartialUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.workflowJobTemplatesWorkflowJobTemplatesPartialUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-workflowJobTemplatesWorkflowJobTemplatesPartialUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobTemplatesWorkflowJobTemplatesAccessListList - errors', () => {
      it('should have a workflowJobTemplatesWorkflowJobTemplatesAccessListList function', (done) => {
        try {
          assert.equal(true, typeof a.workflowJobTemplatesWorkflowJobTemplatesAccessListList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.workflowJobTemplatesWorkflowJobTemplatesAccessListList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-workflowJobTemplatesWorkflowJobTemplatesAccessListListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobTemplatesWorkflowJobTemplatesActivityStreamList - errors', () => {
      it('should have a workflowJobTemplatesWorkflowJobTemplatesActivityStreamList function', (done) => {
        try {
          assert.equal(true, typeof a.workflowJobTemplatesWorkflowJobTemplatesActivityStreamList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.workflowJobTemplatesWorkflowJobTemplatesActivityStreamList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-workflowJobTemplatesWorkflowJobTemplatesActivityStreamListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobTemplatesWorkflowJobTemplatesCopyList - errors', () => {
      it('should have a workflowJobTemplatesWorkflowJobTemplatesCopyList function', (done) => {
        try {
          assert.equal(true, typeof a.workflowJobTemplatesWorkflowJobTemplatesCopyList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.workflowJobTemplatesWorkflowJobTemplatesCopyList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-workflowJobTemplatesWorkflowJobTemplatesCopyListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobTemplatesWorkflowJobTemplatesCopyCreate - errors', () => {
      it('should have a workflowJobTemplatesWorkflowJobTemplatesCopyCreate function', (done) => {
        try {
          assert.equal(true, typeof a.workflowJobTemplatesWorkflowJobTemplatesCopyCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.workflowJobTemplatesWorkflowJobTemplatesCopyCreate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-workflowJobTemplatesWorkflowJobTemplatesCopyCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobTemplatesWorkflowJobTemplatesLabelsList - errors', () => {
      it('should have a workflowJobTemplatesWorkflowJobTemplatesLabelsList function', (done) => {
        try {
          assert.equal(true, typeof a.workflowJobTemplatesWorkflowJobTemplatesLabelsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.workflowJobTemplatesWorkflowJobTemplatesLabelsList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-workflowJobTemplatesWorkflowJobTemplatesLabelsListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobTemplatesWorkflowJobTemplatesLabelsCreate - errors', () => {
      it('should have a workflowJobTemplatesWorkflowJobTemplatesLabelsCreate function', (done) => {
        try {
          assert.equal(true, typeof a.workflowJobTemplatesWorkflowJobTemplatesLabelsCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.workflowJobTemplatesWorkflowJobTemplatesLabelsCreate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-workflowJobTemplatesWorkflowJobTemplatesLabelsCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobTemplatesWorkflowJobTemplatesLaunchRead - errors', () => {
      it('should have a workflowJobTemplatesWorkflowJobTemplatesLaunchRead function', (done) => {
        try {
          assert.equal(true, typeof a.workflowJobTemplatesWorkflowJobTemplatesLaunchRead === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.workflowJobTemplatesWorkflowJobTemplatesLaunchRead(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-workflowJobTemplatesWorkflowJobTemplatesLaunchRead', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobTemplatesWorkflowJobTemplatesLaunchCreate - errors', () => {
      it('should have a workflowJobTemplatesWorkflowJobTemplatesLaunchCreate function', (done) => {
        try {
          assert.equal(true, typeof a.workflowJobTemplatesWorkflowJobTemplatesLaunchCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.workflowJobTemplatesWorkflowJobTemplatesLaunchCreate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-workflowJobTemplatesWorkflowJobTemplatesLaunchCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobTemplatesWorkflowJobTemplatesNotificationTemplatesErrorList - errors', () => {
      it('should have a workflowJobTemplatesWorkflowJobTemplatesNotificationTemplatesErrorList function', (done) => {
        try {
          assert.equal(true, typeof a.workflowJobTemplatesWorkflowJobTemplatesNotificationTemplatesErrorList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.workflowJobTemplatesWorkflowJobTemplatesNotificationTemplatesErrorList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-workflowJobTemplatesWorkflowJobTemplatesNotificationTemplatesErrorListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobTemplatesWorkflowJobTemplatesNotificationTemplatesErrorCreate - errors', () => {
      it('should have a workflowJobTemplatesWorkflowJobTemplatesNotificationTemplatesErrorCreate function', (done) => {
        try {
          assert.equal(true, typeof a.workflowJobTemplatesWorkflowJobTemplatesNotificationTemplatesErrorCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.workflowJobTemplatesWorkflowJobTemplatesNotificationTemplatesErrorCreate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-workflowJobTemplatesWorkflowJobTemplatesNotificationTemplatesErrorCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobTemplatesWorkflowJobTemplatesNotificationTemplatesStartedList - errors', () => {
      it('should have a workflowJobTemplatesWorkflowJobTemplatesNotificationTemplatesStartedList function', (done) => {
        try {
          assert.equal(true, typeof a.workflowJobTemplatesWorkflowJobTemplatesNotificationTemplatesStartedList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.workflowJobTemplatesWorkflowJobTemplatesNotificationTemplatesStartedList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-workflowJobTemplatesWorkflowJobTemplatesNotificationTemplatesStartedListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobTemplatesWorkflowJobTemplatesNotificationTemplatesStartedCreate - errors', () => {
      it('should have a workflowJobTemplatesWorkflowJobTemplatesNotificationTemplatesStartedCreate function', (done) => {
        try {
          assert.equal(true, typeof a.workflowJobTemplatesWorkflowJobTemplatesNotificationTemplatesStartedCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.workflowJobTemplatesWorkflowJobTemplatesNotificationTemplatesStartedCreate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-workflowJobTemplatesWorkflowJobTemplatesNotificationTemplatesStartedCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobTemplatesWorkflowJobTemplatesNotificationTemplatesSuccessList - errors', () => {
      it('should have a workflowJobTemplatesWorkflowJobTemplatesNotificationTemplatesSuccessList function', (done) => {
        try {
          assert.equal(true, typeof a.workflowJobTemplatesWorkflowJobTemplatesNotificationTemplatesSuccessList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.workflowJobTemplatesWorkflowJobTemplatesNotificationTemplatesSuccessList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-workflowJobTemplatesWorkflowJobTemplatesNotificationTemplatesSuccessListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobTemplatesWorkflowJobTemplatesNotificationTemplatesSuccessCreate - errors', () => {
      it('should have a workflowJobTemplatesWorkflowJobTemplatesNotificationTemplatesSuccessCreate function', (done) => {
        try {
          assert.equal(true, typeof a.workflowJobTemplatesWorkflowJobTemplatesNotificationTemplatesSuccessCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.workflowJobTemplatesWorkflowJobTemplatesNotificationTemplatesSuccessCreate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-workflowJobTemplatesWorkflowJobTemplatesNotificationTemplatesSuccessCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobTemplatesWorkflowJobTemplatesObjectRolesList - errors', () => {
      it('should have a workflowJobTemplatesWorkflowJobTemplatesObjectRolesList function', (done) => {
        try {
          assert.equal(true, typeof a.workflowJobTemplatesWorkflowJobTemplatesObjectRolesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.workflowJobTemplatesWorkflowJobTemplatesObjectRolesList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-workflowJobTemplatesWorkflowJobTemplatesObjectRolesListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobTemplatesWorkflowJobTemplatesSchedulesList - errors', () => {
      it('should have a workflowJobTemplatesWorkflowJobTemplatesSchedulesList function', (done) => {
        try {
          assert.equal(true, typeof a.workflowJobTemplatesWorkflowJobTemplatesSchedulesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.workflowJobTemplatesWorkflowJobTemplatesSchedulesList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-workflowJobTemplatesWorkflowJobTemplatesSchedulesListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobTemplatesWorkflowJobTemplatesSchedulesCreate - errors', () => {
      it('should have a workflowJobTemplatesWorkflowJobTemplatesSchedulesCreate function', (done) => {
        try {
          assert.equal(true, typeof a.workflowJobTemplatesWorkflowJobTemplatesSchedulesCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.workflowJobTemplatesWorkflowJobTemplatesSchedulesCreate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-workflowJobTemplatesWorkflowJobTemplatesSchedulesCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobTemplatesWorkflowJobTemplatesSurveySpecList - errors', () => {
      it('should have a workflowJobTemplatesWorkflowJobTemplatesSurveySpecList function', (done) => {
        try {
          assert.equal(true, typeof a.workflowJobTemplatesWorkflowJobTemplatesSurveySpecList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.workflowJobTemplatesWorkflowJobTemplatesSurveySpecList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-workflowJobTemplatesWorkflowJobTemplatesSurveySpecListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobTemplatesWorkflowJobTemplatesSurveySpecCreate - errors', () => {
      it('should have a workflowJobTemplatesWorkflowJobTemplatesSurveySpecCreate function', (done) => {
        try {
          assert.equal(true, typeof a.workflowJobTemplatesWorkflowJobTemplatesSurveySpecCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.workflowJobTemplatesWorkflowJobTemplatesSurveySpecCreate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-workflowJobTemplatesWorkflowJobTemplatesSurveySpecCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobTemplatesWorkflowJobTemplatesSurveySpecDelete - errors', () => {
      it('should have a workflowJobTemplatesWorkflowJobTemplatesSurveySpecDelete function', (done) => {
        try {
          assert.equal(true, typeof a.workflowJobTemplatesWorkflowJobTemplatesSurveySpecDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.workflowJobTemplatesWorkflowJobTemplatesSurveySpecDelete(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-workflowJobTemplatesWorkflowJobTemplatesSurveySpecDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobTemplatesWorkflowJobTemplatesWorkflowJobsList - errors', () => {
      it('should have a workflowJobTemplatesWorkflowJobTemplatesWorkflowJobsList function', (done) => {
        try {
          assert.equal(true, typeof a.workflowJobTemplatesWorkflowJobTemplatesWorkflowJobsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.workflowJobTemplatesWorkflowJobTemplatesWorkflowJobsList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-workflowJobTemplatesWorkflowJobTemplatesWorkflowJobsListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobTemplatesWorkflowJobTemplatesWorkflowNodesList - errors', () => {
      it('should have a workflowJobTemplatesWorkflowJobTemplatesWorkflowNodesList function', (done) => {
        try {
          assert.equal(true, typeof a.workflowJobTemplatesWorkflowJobTemplatesWorkflowNodesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.workflowJobTemplatesWorkflowJobTemplatesWorkflowNodesList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-workflowJobTemplatesWorkflowJobTemplatesWorkflowNodesListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobTemplatesWorkflowJobTemplatesWorkflowNodesCreate - errors', () => {
      it('should have a workflowJobTemplatesWorkflowJobTemplatesWorkflowNodesCreate function', (done) => {
        try {
          assert.equal(true, typeof a.workflowJobTemplatesWorkflowJobTemplatesWorkflowNodesCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.workflowJobTemplatesWorkflowJobTemplatesWorkflowNodesCreate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-workflowJobTemplatesWorkflowJobTemplatesWorkflowNodesCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobsWorkflowJobsList - errors', () => {
      it('should have a workflowJobsWorkflowJobsList function', (done) => {
        try {
          assert.equal(true, typeof a.workflowJobsWorkflowJobsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobsWorkflowJobsCreate - errors', () => {
      it('should have a workflowJobsWorkflowJobsCreate function', (done) => {
        try {
          assert.equal(true, typeof a.workflowJobsWorkflowJobsCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobsWorkflowJobsRead - errors', () => {
      it('should have a workflowJobsWorkflowJobsRead function', (done) => {
        try {
          assert.equal(true, typeof a.workflowJobsWorkflowJobsRead === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.workflowJobsWorkflowJobsRead(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-workflowJobsWorkflowJobsRead', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobsWorkflowJobsDelete - errors', () => {
      it('should have a workflowJobsWorkflowJobsDelete function', (done) => {
        try {
          assert.equal(true, typeof a.workflowJobsWorkflowJobsDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.workflowJobsWorkflowJobsDelete(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-workflowJobsWorkflowJobsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobsWorkflowJobsActivityStreamList - errors', () => {
      it('should have a workflowJobsWorkflowJobsActivityStreamList function', (done) => {
        try {
          assert.equal(true, typeof a.workflowJobsWorkflowJobsActivityStreamList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.workflowJobsWorkflowJobsActivityStreamList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-workflowJobsWorkflowJobsActivityStreamListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobsWorkflowJobsCancelRead - errors', () => {
      it('should have a workflowJobsWorkflowJobsCancelRead function', (done) => {
        try {
          assert.equal(true, typeof a.workflowJobsWorkflowJobsCancelRead === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.workflowJobsWorkflowJobsCancelRead(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-workflowJobsWorkflowJobsCancelRead', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobsWorkflowJobsCancelCreate - errors', () => {
      it('should have a workflowJobsWorkflowJobsCancelCreate function', (done) => {
        try {
          assert.equal(true, typeof a.workflowJobsWorkflowJobsCancelCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.workflowJobsWorkflowJobsCancelCreate(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-workflowJobsWorkflowJobsCancelCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobsWorkflowJobsLabelsList - errors', () => {
      it('should have a workflowJobsWorkflowJobsLabelsList function', (done) => {
        try {
          assert.equal(true, typeof a.workflowJobsWorkflowJobsLabelsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.workflowJobsWorkflowJobsLabelsList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-workflowJobsWorkflowJobsLabelsListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobsWorkflowJobsNotificationsList - errors', () => {
      it('should have a workflowJobsWorkflowJobsNotificationsList function', (done) => {
        try {
          assert.equal(true, typeof a.workflowJobsWorkflowJobsNotificationsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.workflowJobsWorkflowJobsNotificationsList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-workflowJobsWorkflowJobsNotificationsListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobsWorkflowJobsRelaunchList - errors', () => {
      it('should have a workflowJobsWorkflowJobsRelaunchList function', (done) => {
        try {
          assert.equal(true, typeof a.workflowJobsWorkflowJobsRelaunchList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.workflowJobsWorkflowJobsRelaunchList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-workflowJobsWorkflowJobsRelaunchListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobsWorkflowJobsRelaunchCreate - errors', () => {
      it('should have a workflowJobsWorkflowJobsRelaunchCreate function', (done) => {
        try {
          assert.equal(true, typeof a.workflowJobsWorkflowJobsRelaunchCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.workflowJobsWorkflowJobsRelaunchCreate(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-workflowJobsWorkflowJobsRelaunchCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobsWorkflowJobsWorkflowNodesList - errors', () => {
      it('should have a workflowJobsWorkflowJobsWorkflowNodesList function', (done) => {
        try {
          assert.equal(true, typeof a.workflowJobsWorkflowJobsWorkflowNodesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.workflowJobsWorkflowJobsWorkflowNodesList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ansible_tower-adapter-workflowJobsWorkflowJobsWorkflowNodesListWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
