## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Ansible Tower. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Ansible Tower.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Ansible Tower. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">versioningList(callback)</td>
    <td style="padding:15px">List supported API versions</td>
    <td style="padding:15px">{base_path}/{version}/api/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">versioningRead(callback)</td>
    <td style="padding:15px">List top level resources</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">authenticationOList(callback)</td>
    <td style="padding:15px">Token Handling using OAuth2</td>
    <td style="padding:15px">{base_path}/{version}/api/o/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">authenticationApplicationsList0(page, pageSize, search, callback)</td>
    <td style="padding:15px">List Applications</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/applications/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">authenticationApplicationsCreate0(data, callback)</td>
    <td style="padding:15px">Create an Application</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/applications/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">authenticationApplicationsRead0(id, search, callback)</td>
    <td style="padding:15px">Retrieve an Application</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/applications/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">authenticationApplicationsUpdate0(id, search, data, callback)</td>
    <td style="padding:15px">Update an Application</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/applications/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">authenticationApplicationsDelete0(id, search, callback)</td>
    <td style="padding:15px">Delete an Application</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/applications/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">authenticationApplicationsPartialUpdate0(id, search, data, callback)</td>
    <td style="padding:15px">Update an Application</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/applications/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">authenticationApplicationsActivityStreamList(id, page, pageSize, search, callback)</td>
    <td style="padding:15px">List Activity Streams for an Application</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/applications/{pathv1}/activity_stream/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">authenticationApplicationsTokensList0(id, page, pageSize, search, callback)</td>
    <td style="padding:15px">List Access Tokens for an Application</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/applications/{pathv1}/tokens/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">authenticationApplicationsTokensCreate0(id, data, callback)</td>
    <td style="padding:15px">Create an Access Token for an Application</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/applications/{pathv1}/tokens/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">authenticationTokensList0(page, pageSize, search, callback)</td>
    <td style="padding:15px">List Access Tokens</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/tokens/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">authenticationTokensCreate0(data, callback)</td>
    <td style="padding:15px">Create an Access Token</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/tokens/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">authenticationTokensRead(id, search, callback)</td>
    <td style="padding:15px">Retrieve an Access Token</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/tokens/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">authenticationTokensUpdate(id, search, data, callback)</td>
    <td style="padding:15px">Update an Access Token</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/tokens/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">authenticationTokensDelete(id, search, callback)</td>
    <td style="padding:15px">Delete an Access Token</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/tokens/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">authenticationTokensPartialUpdate(id, search, data, callback)</td>
    <td style="padding:15px">Update an Access Token</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/tokens/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">authenticationTokensActivityStreamList(id, page, pageSize, search, callback)</td>
    <td style="padding:15px">List Activity Streams for an Access Token</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/tokens/{pathv1}/activity_stream/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">activityStreamsActivityStreamListWithOptions(page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Activity Streams</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/activity_stream/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">activityStreamsActivityStreamRead(id, search, callback)</td>
    <td style="padding:15px">Retrieve an Activity Stream</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/activity_stream/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">adHocCommandEventsAdHocCommandEventsListWithOptions(page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Ad Hoc Command Events</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/ad_hoc_command_events/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">adHocCommandEventsAdHocCommandEventsRead(id, search, callback)</td>
    <td style="padding:15px">Retrieve an Ad Hoc Command Event</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/ad_hoc_command_events/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">adHocCommandsAdHocCommandsListWithOptions(page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Ad Hoc Commands</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/ad_hoc_commands/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">adHocCommandsAdHocCommandsCreate(data, callback)</td>
    <td style="padding:15px">Create an Ad Hoc Command</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/ad_hoc_commands/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">adHocCommandsAdHocCommandsRead(id, search, callback)</td>
    <td style="padding:15px">Retrieve an Ad Hoc Command</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/ad_hoc_commands/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">adHocCommandsAdHocCommandsDelete(id, search, callback)</td>
    <td style="padding:15px">Delete an Ad Hoc Command</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/ad_hoc_commands/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">adHocCommandsAdHocCommandsActivityStreamListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Activity Streams for an Ad Hoc Command</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/ad_hoc_commands/{pathv1}/activity_stream/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">adHocCommandsAdHocCommandsCancelRead(id, search, callback)</td>
    <td style="padding:15px">Retrieve an Ad Hoc Command</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/ad_hoc_commands/{pathv1}/cancel/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">adHocCommandsAdHocCommandsCancelCreate(id, callback)</td>
    <td style="padding:15px">Retrieve an Ad Hoc Command</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/ad_hoc_commands/{pathv1}/cancel/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">adHocCommandsAdHocCommandsEventsListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Ad Hoc Command Events for an Ad Hoc Command</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/ad_hoc_commands/{pathv1}/events/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">adHocCommandsAdHocCommandsNotificationsListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Notifications for an Ad Hoc Command</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/ad_hoc_commands/{pathv1}/notifications/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">adHocCommandsAdHocCommandsRelaunchListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">Relaunch an Ad Hoc Command</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/ad_hoc_commands/{pathv1}/relaunch/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">adHocCommandsAdHocCommandsRelaunchCreate(id, callback)</td>
    <td style="padding:15px">Relaunch an Ad Hoc Command</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/ad_hoc_commands/{pathv1}/relaunch/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">adHocCommandsAdHocCommandsStdoutRead(id, callback)</td>
    <td style="padding:15px">Retrieve Ad Hoc Command Stdout</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/ad_hoc_commands/{pathv1}/stdout/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">systemConfigurationAuthList(callback)</td>
    <td style="padding:15px">List enabled single-sign-on endpoints</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/auth/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">systemConfigurationConfigList(callback)</td>
    <td style="padding:15px">Return various sitewide configuration settings</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/config/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">systemConfigurationConfigCreate(callback)</td>
    <td style="padding:15px">System Configuration_config_create</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/config/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">systemConfigurationConfigDelete(callback)</td>
    <td style="padding:15px">System Configuration_config_delete</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/config/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">systemConfigurationPingList(callback)</td>
    <td style="padding:15px">Return some basic information about this instance</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/ping/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">credentialInputSourcesCredentialInputSourcesListWithOptions(page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Credential Input Sources</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/credential_input_sources/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">credentialInputSourcesCredentialInputSourcesCreate(data, callback)</td>
    <td style="padding:15px">Create a Credential Input Source</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/credential_input_sources/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">credentialInputSourcesCredentialInputSourcesRead(id, search, callback)</td>
    <td style="padding:15px">Retrieve a Credential Input Source</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/credential_input_sources/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">credentialInputSourcesCredentialInputSourcesUpdate(id, search, data, callback)</td>
    <td style="padding:15px">Update a Credential Input Source</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/credential_input_sources/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">credentialInputSourcesCredentialInputSourcesDelete(id, search, callback)</td>
    <td style="padding:15px">Delete a Credential Input Source</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/credential_input_sources/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">credentialInputSourcesCredentialInputSourcesPartialUpdate(id, search, data, callback)</td>
    <td style="padding:15px">Update a Credential Input Source</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/credential_input_sources/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">credentialTypesCredentialTypesListWithOptions(page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Credential Types</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/credential_types/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">credentialTypesCredentialTypesCreate(data, callback)</td>
    <td style="padding:15px">Create a Credential Type</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/credential_types/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">credentialTypesCredentialTypesRead(id, search, callback)</td>
    <td style="padding:15px">Retrieve a Credential Type</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/credential_types/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">credentialTypesCredentialTypesUpdate(id, search, data, callback)</td>
    <td style="padding:15px">Update a Credential Type</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/credential_types/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">credentialTypesCredentialTypesDelete(id, search, callback)</td>
    <td style="padding:15px">Delete a Credential Type</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/credential_types/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">credentialTypesCredentialTypesPartialUpdate(id, search, data, callback)</td>
    <td style="padding:15px">Update a Credential Type</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/credential_types/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">credentialTypesCredentialTypesActivityStreamListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Activity Streams for a Credential Type</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/credential_types/{pathv1}/activity_stream/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">credentialTypesCredentialTypesCredentialsListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Credentials for a Credential Type</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/credential_types/{pathv1}/credentials/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">credentialTypesCredentialTypesCredentialsCreate(id, data, callback)</td>
    <td style="padding:15px">Create a Credential for a Credential Type</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/credential_types/{pathv1}/credentials/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">credentialTypesCredentialTypesTestRead(id, search, callback)</td>
    <td style="padding:15px">Retrieve a Credential Type</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/credential_types/{pathv1}/test/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">credentialTypesCredentialTypesTestCreate(id, callback)</td>
    <td style="padding:15px">Retrieve a Credential Type</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/credential_types/{pathv1}/test/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">credentialsCredentialsListWithOptions(page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Credentials</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/credentials/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">credentialsCredentialsCreate(data, callback)</td>
    <td style="padding:15px">Create a Credential</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/credentials/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">credentialsCredentialsRead(id, search, callback)</td>
    <td style="padding:15px">Retrieve a Credential</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/credentials/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">credentialsCredentialsUpdate(id, search, data, callback)</td>
    <td style="padding:15px">Update a Credential</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/credentials/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">credentialsCredentialsDelete(id, search, callback)</td>
    <td style="padding:15px">Delete a Credential</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/credentials/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">credentialsCredentialsPartialUpdate(id, search, data, callback)</td>
    <td style="padding:15px">Update a Credential</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/credentials/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">credentialsCredentialsAccessListListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Users</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/credentials/{pathv1}/access_list/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">credentialsCredentialsActivityStreamListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Activity Streams for a Credential</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/credentials/{pathv1}/activity_stream/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">credentialsCredentialsCopyListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">Credentials_credentials_copy_list</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/credentials/{pathv1}/copy/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">credentialsCredentialsCopyCreate(id, data, callback)</td>
    <td style="padding:15px">Credentials_credentials_copy_create</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/credentials/{pathv1}/copy/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">credentialsCredentialsInputSourcesListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Credential Input Sources for a Credential</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/credentials/{pathv1}/input_sources/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">credentialsCredentialsInputSourcesCreate(id, data, callback)</td>
    <td style="padding:15px">Create a Credential Input Source for a Credential</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/credentials/{pathv1}/input_sources/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">credentialsCredentialsObjectRolesListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Roles for a Credential</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/credentials/{pathv1}/object_roles/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">credentialsCredentialsOwnerTeamsListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Teams for a Credential</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/credentials/{pathv1}/owner_teams/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">credentialsCredentialsOwnerUsersListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Users for a Credential</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/credentials/{pathv1}/owner_users/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">credentialsCredentialsTestRead(id, search, callback)</td>
    <td style="padding:15px">Retrieve a Credential</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/credentials/{pathv1}/test/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">credentialsCredentialsTestCreate(id, callback)</td>
    <td style="padding:15px">Retrieve a Credential</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/credentials/{pathv1}/test/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dashboardDashboardList(callback)</td>
    <td style="padding:15px">Show Dashboard Details</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/dashboard/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dashboardDashboardGraphsJobsList(callback)</td>
    <td style="padding:15px">View Statistics for Job Runs</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/dashboard/graphs/jobs/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">groupsGroupsListWithOptions(page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Groups</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/groups/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">groupsGroupsCreate(data, callback)</td>
    <td style="padding:15px">Create a Group</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/groups/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">groupsGroupsRead(id, search, callback)</td>
    <td style="padding:15px">Retrieve a Group</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/groups/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">groupsGroupsUpdate(id, search, data, callback)</td>
    <td style="padding:15px">Update a Group</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/groups/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">groupsGroupsDelete(id, search, callback)</td>
    <td style="padding:15px">Delete a Group</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/groups/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">groupsGroupsPartialUpdate(id, search, data, callback)</td>
    <td style="padding:15px">Update a Group</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/groups/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">groupsGroupsActivityStreamListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Activity Streams for a Group</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/groups/{pathv1}/activity_stream/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">groupsGroupsAdHocCommandsListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Ad Hoc Commands for a Group</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/groups/{pathv1}/ad_hoc_commands/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">groupsGroupsAdHocCommandsCreate(id, data, callback)</td>
    <td style="padding:15px">Create an Ad Hoc Command for a Group</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/groups/{pathv1}/ad_hoc_commands/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">groupsGroupsAllHostsListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List All Hosts for a Group</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/groups/{pathv1}/all_hosts/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">groupsGroupsChildrenListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Groups for a Group</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/groups/{pathv1}/children/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">groupsGroupsChildrenCreate(id, data, callback)</td>
    <td style="padding:15px">Create a Group for a Group</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/groups/{pathv1}/children/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">groupsGroupsHostsListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Hosts for a Group</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/groups/{pathv1}/hosts/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">groupsGroupsHostsCreate(id, data, callback)</td>
    <td style="padding:15px">Create a Host for a Group</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/groups/{pathv1}/hosts/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">groupsGroupsInventorySourcesListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Inventory Sources for a Group</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/groups/{pathv1}/inventory_sources/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">groupsGroupsJobEventsListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Job Events for a Group</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/groups/{pathv1}/job_events/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">groupsGroupsJobHostSummariesListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Job Host Summaries for a Group</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/groups/{pathv1}/job_host_summaries/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">groupsGroupsPotentialChildrenListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Potential Child Groups for a Group</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/groups/{pathv1}/potential_children/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">groupsGroupsVariableDataRead(id, search, callback)</td>
    <td style="padding:15px">Retrieve Group Variable Data</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/groups/{pathv1}/variable_data/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">groupsGroupsVariableDataUpdate(id, search, data, callback)</td>
    <td style="padding:15px">Update Group Variable Data</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/groups/{pathv1}/variable_data/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">groupsGroupsVariableDataPartialUpdate(id, search, data, callback)</td>
    <td style="padding:15px">Update Group Variable Data</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/groups/{pathv1}/variable_data/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">hostsHostsListWithOptions(page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Hosts</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/hosts/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">hostsHostsCreate(data, callback)</td>
    <td style="padding:15px">Create a host</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/hosts/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">hostsHostsRead(id, search, callback)</td>
    <td style="padding:15px">Retrieve a Host</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/hosts/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">hostsHostsUpdate(id, search, data, callback)</td>
    <td style="padding:15px">Update a Host</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/hosts/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">hostsHostsDelete(id, search, callback)</td>
    <td style="padding:15px">Delete a Host</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/hosts/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">hostsHostsPartialUpdate(id, search, data, callback)</td>
    <td style="padding:15px">Update a Host</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/hosts/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">hostsHostsActivityStreamListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Activity Streams for a Host</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/hosts/{pathv1}/activity_stream/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">hostsHostsAdHocCommandEventsListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Ad Hoc Command Events for a Host</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/hosts/{pathv1}/ad_hoc_command_events/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">hostsHostsAdHocCommandsListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Ad Hoc Commands for a Host</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/hosts/{pathv1}/ad_hoc_commands/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">hostsHostsAdHocCommandsCreate(id, data, callback)</td>
    <td style="padding:15px">Create an Ad Hoc Command for a Host</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/hosts/{pathv1}/ad_hoc_commands/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">hostsHostsAllGroupsListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List All Groups for a Host</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/hosts/{pathv1}/all_groups/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">hostsHostsAnsibleFactsRead(id, search, callback)</td>
    <td style="padding:15px">Retrieve a Host</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/hosts/{pathv1}/ansible_facts/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">hostsHostsGroupsListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Groups for a Host</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/hosts/{pathv1}/groups/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">hostsHostsGroupsCreate(id, data, callback)</td>
    <td style="padding:15px">Create a Group for a Host</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/hosts/{pathv1}/groups/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">hostsHostsInsightsListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Red Hat Insights for a Host</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/hosts/{pathv1}/insights/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">hostsHostsInventorySourcesListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Inventory Sources for a Host</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/hosts/{pathv1}/inventory_sources/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">hostsHostsJobEventsListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Job Events for a Host</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/hosts/{pathv1}/job_events/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">hostsHostsJobHostSummariesListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Job Host Summaries for a Host</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/hosts/{pathv1}/job_host_summaries/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">hostsHostsSmartInventoriesListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Inventories for a Host</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/hosts/{pathv1}/smart_inventories/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">hostsHostsVariableDataRead(id, search, callback)</td>
    <td style="padding:15px">Retrieve Host Variable Data</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/hosts/{pathv1}/variable_data/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">hostsHostsVariableDataUpdate(id, search, data, callback)</td>
    <td style="padding:15px">Update Host Variable Data</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/hosts/{pathv1}/variable_data/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">hostsHostsVariableDataPartialUpdate(id, search, data, callback)</td>
    <td style="padding:15px">Update Host Variable Data</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/hosts/{pathv1}/variable_data/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">instanceGroupsInstanceGroupsListWithOptions(page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Instance Groups</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/instance_groups/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">instanceGroupsInstanceGroupsCreate(data, callback)</td>
    <td style="padding:15px">Create an Instance Group</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/instance_groups/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">instanceGroupsInstanceGroupsRead(id, search, callback)</td>
    <td style="padding:15px">Retrieve an Instance Group</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/instance_groups/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">instanceGroupsInstanceGroupsUpdate(id, search, data, callback)</td>
    <td style="padding:15px">Update an Instance Group</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/instance_groups/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">instanceGroupsInstanceGroupsDelete(id, search, callback)</td>
    <td style="padding:15px">Delete an Instance Group</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/instance_groups/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">instanceGroupsInstanceGroupsPartialUpdate(id, search, data, callback)</td>
    <td style="padding:15px">Update an Instance Group</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/instance_groups/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">instanceGroupsInstanceGroupsInstancesListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Instances for an Instance Group</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/instance_groups/{pathv1}/instances/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">instanceGroupsInstanceGroupsInstancesCreate(id, data, callback)</td>
    <td style="padding:15px">Create an Instance for an Instance Group</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/instance_groups/{pathv1}/instances/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">instanceGroupsInstanceGroupsJobsListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Unified Jobs for an Instance Group</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/instance_groups/{pathv1}/jobs/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">instancesInstancesListWithOptions(page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Instances</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/instances/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">instancesInstancesRead(id, search, callback)</td>
    <td style="padding:15px">Retrieve an Instance</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/instances/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">instancesInstancesUpdate(id, search, data, callback)</td>
    <td style="padding:15px">Update an Instance</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/instances/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">instancesInstancesPartialUpdate(id, search, data, callback)</td>
    <td style="padding:15px">Update an Instance</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/instances/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">instancesInstancesInstanceGroupsListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Instance Groups for an Instance</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/instances/{pathv1}/instance_groups/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">instancesInstancesInstanceGroupsCreate(id, data, callback)</td>
    <td style="padding:15px">Create an Instance Group for an Instance</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/instances/{pathv1}/instance_groups/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">instancesInstancesJobsListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Unified Jobs for an Instance</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/instances/{pathv1}/jobs/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inventoriesInventoriesListWithOptions(page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Inventories</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/inventories/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inventoriesInventoriesCreate(data, callback)</td>
    <td style="padding:15px">Create an Inventory</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/inventories/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inventoriesInventoriesRead(id, search, callback)</td>
    <td style="padding:15px">Retrieve an Inventory</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/inventories/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inventoriesInventoriesUpdate(id, search, data, callback)</td>
    <td style="padding:15px">Update an Inventory</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/inventories/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inventoriesInventoriesDelete(id, search, callback)</td>
    <td style="padding:15px">Delete an Inventory</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/inventories/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inventoriesInventoriesPartialUpdate(id, search, data, callback)</td>
    <td style="padding:15px">Update an Inventory</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/inventories/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inventoriesInventoriesAccessListListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Users</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/inventories/{pathv1}/access_list/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inventoriesInventoriesActivityStreamListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Activity Streams for an Inventory</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/inventories/{pathv1}/activity_stream/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inventoriesInventoriesAdHocCommandsListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Ad Hoc Commands for an Inventory</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/inventories/{pathv1}/ad_hoc_commands/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inventoriesInventoriesAdHocCommandsCreate(id, data, callback)</td>
    <td style="padding:15px">Create an Ad Hoc Command for an Inventory</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/inventories/{pathv1}/ad_hoc_commands/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inventoriesInventoriesCopyListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">Inventories_inventories_copy_list</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/inventories/{pathv1}/copy/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inventoriesInventoriesCopyCreate(id, data, callback)</td>
    <td style="padding:15px">Inventories_inventories_copy_create</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/inventories/{pathv1}/copy/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inventoriesInventoriesGroupsListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Groups for an Inventory</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/inventories/{pathv1}/groups/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inventoriesInventoriesGroupsCreate(id, data, callback)</td>
    <td style="padding:15px">Create a Group for an Inventory</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/inventories/{pathv1}/groups/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inventoriesInventoriesHostsListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Hosts for an Inventory</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/inventories/{pathv1}/hosts/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inventoriesInventoriesHostsCreate(id, data, callback)</td>
    <td style="padding:15px">Create a Host for an Inventory</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/inventories/{pathv1}/hosts/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inventoriesInventoriesInstanceGroupsListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Instance Groups for an Inventory</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/inventories/{pathv1}/instance_groups/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inventoriesInventoriesInstanceGroupsCreate(id, data, callback)</td>
    <td style="padding:15px">Create an Instance Group for an Inventory</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/inventories/{pathv1}/instance_groups/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inventoriesInventoriesInventorySourcesListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Inventory Sources for an Inventory</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/inventories/{pathv1}/inventory_sources/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inventoriesInventoriesInventorySourcesCreate(id, data, callback)</td>
    <td style="padding:15px">Create an Inventory Source for an Inventory</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/inventories/{pathv1}/inventory_sources/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inventoriesInventoriesJobTemplatesListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Job Templates for an Inventory</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/inventories/{pathv1}/job_templates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inventoriesInventoriesObjectRolesListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Roles for an Inventory</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/inventories/{pathv1}/object_roles/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inventoriesInventoriesRootGroupsListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Root Groups for an Inventory</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/inventories/{pathv1}/root_groups/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inventoriesInventoriesRootGroupsCreate(id, data, callback)</td>
    <td style="padding:15px">Inventories_inventories_root_groups_create</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/inventories/{pathv1}/root_groups/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inventoriesInventoriesScriptRead(id, callback)</td>
    <td style="padding:15px">Generate inventory group and host data as needed for an inventory script.</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/inventories/{pathv1}/script/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inventoriesInventoriesTreeRead(id, callback)</td>
    <td style="padding:15px">Group Tree for an Inventory</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/inventories/{pathv1}/tree/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inventoriesInventoriesUpdateInventorySourcesRead(id, search, callback)</td>
    <td style="padding:15px">Update Inventory Sources</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/inventories/{pathv1}/update_inventory_sources/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inventoriesInventoriesUpdateInventorySourcesCreate(id, callback)</td>
    <td style="padding:15px">Update Inventory Sources</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/inventories/{pathv1}/update_inventory_sources/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inventoriesInventoriesVariableDataRead(id, search, callback)</td>
    <td style="padding:15px">Retrieve Inventory Variable Data</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/inventories/{pathv1}/variable_data/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inventoriesInventoriesVariableDataUpdate(id, search, data, callback)</td>
    <td style="padding:15px">Update Inventory Variable Data</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/inventories/{pathv1}/variable_data/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inventoriesInventoriesVariableDataPartialUpdate(id, search, data, callback)</td>
    <td style="padding:15px">Update Inventory Variable Data</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/inventories/{pathv1}/variable_data/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">customInventoryScriptsInventoryScriptsListWithOptions(page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Custom Inventory Scripts</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/inventory_scripts/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">customInventoryScriptsInventoryScriptsCreate(data, callback)</td>
    <td style="padding:15px">Create a Custom Inventory Script</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/inventory_scripts/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">customInventoryScriptsInventoryScriptsRead(id, search, callback)</td>
    <td style="padding:15px">Retrieve a Custom Inventory Script</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/inventory_scripts/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">customInventoryScriptsInventoryScriptsUpdate(id, search, data, callback)</td>
    <td style="padding:15px">Update a Custom Inventory Script</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/inventory_scripts/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">customInventoryScriptsInventoryScriptsDelete(id, search, callback)</td>
    <td style="padding:15px">Delete a Custom Inventory Script</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/inventory_scripts/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">customInventoryScriptsInventoryScriptsPartialUpdate(id, search, data, callback)</td>
    <td style="padding:15px">Update a Custom Inventory Script</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/inventory_scripts/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">customInventoryScriptsInventoryScriptsCopyListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">Custom Inventory Scripts_inventory_scripts_copy_list</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/inventory_scripts/{pathv1}/copy/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">customInventoryScriptsInventoryScriptsCopyCreate(id, data, callback)</td>
    <td style="padding:15px">Custom Inventory Scripts_inventory_scripts_copy_create</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/inventory_scripts/{pathv1}/copy/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">customInventoryScriptsInventoryScriptsObjectRolesListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Roles for a Custom Inventory Script</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/inventory_scripts/{pathv1}/object_roles/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inventorySourcesInventorySourcesListWithOptions(page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Inventory Sources</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/inventory_sources/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inventorySourcesInventorySourcesCreate(data, callback)</td>
    <td style="padding:15px">Create an Inventory Source</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/inventory_sources/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inventorySourcesInventorySourcesRead(id, search, callback)</td>
    <td style="padding:15px">Retrieve an Inventory Source</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/inventory_sources/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inventorySourcesInventorySourcesUpdate0(id, search, data, callback)</td>
    <td style="padding:15px">Update an Inventory Source</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/inventory_sources/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inventorySourcesInventorySourcesDelete(id, search, callback)</td>
    <td style="padding:15px">Delete an Inventory Source</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/inventory_sources/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inventorySourcesInventorySourcesPartialUpdate(id, search, data, callback)</td>
    <td style="padding:15px">Update an Inventory Source</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/inventory_sources/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inventorySourcesInventorySourcesActivityStreamListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Activity Streams for an Inventory Source</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/inventory_sources/{pathv1}/activity_stream/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inventorySourcesInventorySourcesCredentialsListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Credentials for an Inventory Source</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/inventory_sources/{pathv1}/credentials/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inventorySourcesInventorySourcesCredentialsCreate(id, data, callback)</td>
    <td style="padding:15px">Create a Credential for an Inventory Source</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/inventory_sources/{pathv1}/credentials/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inventorySourcesInventorySourcesGroupsListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Groups for an Inventory Source</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/inventory_sources/{pathv1}/groups/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inventorySourcesInventorySourcesGroupsDelete(id, search, callback)</td>
    <td style="padding:15px">Create a Group for an Inventory Source</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/inventory_sources/{pathv1}/groups/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inventorySourcesInventorySourcesHostsListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Hosts for an Inventory Source</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/inventory_sources/{pathv1}/hosts/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inventorySourcesInventorySourcesHostsDelete(id, search, callback)</td>
    <td style="padding:15px">Create a Host for an Inventory Source</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/inventory_sources/{pathv1}/hosts/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inventorySourcesInventorySourcesInventoryUpdatesListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Inventory Updates for an Inventory Source</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/inventory_sources/{pathv1}/inventory_updates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inventorySourcesInventorySourcesNotificationTemplatesErrorListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Notification Templates for an Inventory Source</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/inventory_sources/{pathv1}/notification_templates_error/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inventorySourcesInventorySourcesNotificationTemplatesErrorCreate(id, data, callback)</td>
    <td style="padding:15px">Create a Notification Template for an Inventory Source</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/inventory_sources/{pathv1}/notification_templates_error/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inventorySourcesInventorySourcesNotificationTemplatesStartedListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Notification Templates for an Inventory Source</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/inventory_sources/{pathv1}/notification_templates_started/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inventorySourcesInventorySourcesNotificationTemplatesStartedCreate(id, data, callback)</td>
    <td style="padding:15px">Create a Notification Template for an Inventory Source</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/inventory_sources/{pathv1}/notification_templates_started/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inventorySourcesInventorySourcesNotificationTemplatesSuccessListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Notification Templates for an Inventory Source</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/inventory_sources/{pathv1}/notification_templates_success/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inventorySourcesInventorySourcesNotificationTemplatesSuccessCreate(id, data, callback)</td>
    <td style="padding:15px">Create a Notification Template for an Inventory Source</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/inventory_sources/{pathv1}/notification_templates_success/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inventorySourcesInventorySourcesSchedulesListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Schedules for an Inventory Source</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/inventory_sources/{pathv1}/schedules/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inventorySourcesInventorySourcesSchedulesCreate(id, data, callback)</td>
    <td style="padding:15px">Create a Schedule for an Inventory Source</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/inventory_sources/{pathv1}/schedules/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inventorySourcesInventorySourcesUpdateRead(id, search, callback)</td>
    <td style="padding:15px">Update Inventory Source</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/inventory_sources/{pathv1}/update/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inventorySourcesInventorySourcesUpdateCreate(id, callback)</td>
    <td style="padding:15px">Update Inventory Source</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/inventory_sources/{pathv1}/update/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inventoryUpdatesInventoryUpdatesListWithOptions(page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Inventory Updates</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/inventory_updates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inventoryUpdatesInventoryUpdatesRead(id, search, callback)</td>
    <td style="padding:15px">Retrieve an Inventory Update</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/inventory_updates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inventoryUpdatesInventoryUpdatesDelete(id, search, callback)</td>
    <td style="padding:15px">Delete an Inventory Update</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/inventory_updates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inventoryUpdatesInventoryUpdatesCancelRead(id, search, callback)</td>
    <td style="padding:15px">Retrieve an Inventory Update</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/inventory_updates/{pathv1}/cancel/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inventoryUpdatesInventoryUpdatesCancelCreate(id, callback)</td>
    <td style="padding:15px">Retrieve an Inventory Update</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/inventory_updates/{pathv1}/cancel/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inventoryUpdatesInventoryUpdatesCredentialsListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Credentials for an Inventory Update</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/inventory_updates/{pathv1}/credentials/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inventoryUpdatesInventoryUpdatesEventsListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Inventory Update Events for an Inventory Update</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/inventory_updates/{pathv1}/events/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inventoryUpdatesInventoryUpdatesNotificationsListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Notifications for an Inventory Update</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/inventory_updates/{pathv1}/notifications/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inventoryUpdatesInventoryUpdatesStdoutRead(id, callback)</td>
    <td style="padding:15px">Retrieve Inventory Update Stdout</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/inventory_updates/{pathv1}/stdout/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobEventsJobEventsListWithOptions(page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Job Events</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/job_events/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobEventsJobEventsRead(id, search, callback)</td>
    <td style="padding:15px">Retrieve a Job Event</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/job_events/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobEventsJobEventsChildrenListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Job Events for a Job Event</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/job_events/{pathv1}/children/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobEventsJobEventsHostsListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Hosts for a Job Event</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/job_events/{pathv1}/hosts/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobHostSummariesJobHostSummariesRead(id, search, callback)</td>
    <td style="padding:15px">Retrieve a Job Host Summary</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/job_host_summaries/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobTemplatesJobTemplatesListWithOptions(page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Job Templates</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/job_templates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobTemplatesJobTemplatesCreate(data, callback)</td>
    <td style="padding:15px">Create a Job Template</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/job_templates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobTemplatesJobTemplatesRead(id, search, callback)</td>
    <td style="padding:15px">Retrieve a Job Template</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/job_templates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobTemplatesJobTemplatesUpdate(id, search, data, callback)</td>
    <td style="padding:15px">Update a Job Template</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/job_templates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobTemplatesJobTemplatesDelete(id, search, callback)</td>
    <td style="padding:15px">Delete a Job Template</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/job_templates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobTemplatesJobTemplatesPartialUpdate(id, search, data, callback)</td>
    <td style="padding:15px">Update a Job Template</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/job_templates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobTemplatesJobTemplatesAccessListListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Users</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/job_templates/{pathv1}/access_list/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobTemplatesJobTemplatesActivityStreamListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Activity Streams for a Job Template</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/job_templates/{pathv1}/activity_stream/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobTemplatesJobTemplatesCallbackListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">The job template callback allows for ephemeral hosts to launch a new job.</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/job_templates/{pathv1}/callback/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobTemplatesJobTemplatesCallbackCreate(id, data, callback)</td>
    <td style="padding:15px">The job template callback allows for ephemeral hosts to launch a new job.</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/job_templates/{pathv1}/callback/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobTemplatesJobTemplatesCopyListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">Job Templates_job_templates_copy_list</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/job_templates/{pathv1}/copy/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobTemplatesJobTemplatesCopyCreate(id, data, callback)</td>
    <td style="padding:15px">Job Templates_job_templates_copy_create</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/job_templates/{pathv1}/copy/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobTemplatesJobTemplatesCredentialsListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Credentials for a Job Template</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/job_templates/{pathv1}/credentials/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobTemplatesJobTemplatesCredentialsCreate(id, data, callback)</td>
    <td style="padding:15px">Create a Credential for a Job Template</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/job_templates/{pathv1}/credentials/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobTemplatesJobTemplatesExtraCredentialsListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Credentials for a Job Template</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/job_templates/{pathv1}/extra_credentials/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobTemplatesJobTemplatesExtraCredentialsCreate(id, data, callback)</td>
    <td style="padding:15px">Create a Credential for a Job Template</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/job_templates/{pathv1}/extra_credentials/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobTemplatesJobTemplatesInstanceGroupsListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Instance Groups for a Job Template</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/job_templates/{pathv1}/instance_groups/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobTemplatesJobTemplatesInstanceGroupsCreate(id, data, callback)</td>
    <td style="padding:15px">Create an Instance Group for a Job Template</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/job_templates/{pathv1}/instance_groups/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobTemplatesJobTemplatesJobsListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Jobs for a Job Template</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/job_templates/{pathv1}/jobs/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobTemplatesJobTemplatesLabelsListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Labels for a Job Template</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/job_templates/{pathv1}/labels/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobTemplatesJobTemplatesLabelsCreate(id, data, callback)</td>
    <td style="padding:15px">Create a Label for a Job Template</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/job_templates/{pathv1}/labels/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobTemplatesJobTemplatesLaunchRead(id, search, callback)</td>
    <td style="padding:15px">Launch a Job Template</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/job_templates/{pathv1}/launch/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobTemplatesJobTemplatesLaunchCreate(id, data, callback)</td>
    <td style="padding:15px">Launch a Job Template</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/job_templates/{pathv1}/launch/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobTemplatesJobTemplatesNotificationTemplatesErrorListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Notification Templates for a Job Template</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/job_templates/{pathv1}/notification_templates_error/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobTemplatesJobTemplatesNotificationTemplatesErrorCreate(id, data, callback)</td>
    <td style="padding:15px">Create a Notification Template for a Job Template</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/job_templates/{pathv1}/notification_templates_error/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobTemplatesJobTemplatesNotificationTemplatesStartedListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Notification Templates for a Job Template</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/job_templates/{pathv1}/notification_templates_started/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobTemplatesJobTemplatesNotificationTemplatesStartedCreate(id, data, callback)</td>
    <td style="padding:15px">Create a Notification Template for a Job Template</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/job_templates/{pathv1}/notification_templates_started/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobTemplatesJobTemplatesNotificationTemplatesSuccessListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Notification Templates for a Job Template</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/job_templates/{pathv1}/notification_templates_success/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobTemplatesJobTemplatesNotificationTemplatesSuccessCreate(id, data, callback)</td>
    <td style="padding:15px">Create a Notification Template for a Job Template</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/job_templates/{pathv1}/notification_templates_success/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobTemplatesJobTemplatesObjectRolesListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Roles for a Job Template</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/job_templates/{pathv1}/object_roles/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobTemplatesJobTemplatesSchedulesListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Schedules for a Job Template</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/job_templates/{pathv1}/schedules/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobTemplatesJobTemplatesSchedulesCreate(id, data, callback)</td>
    <td style="padding:15px">Create a Schedule for a Job Template</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/job_templates/{pathv1}/schedules/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobTemplatesJobTemplatesSliceWorkflowJobsListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Workflow Jobs for a Job Template</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/job_templates/{pathv1}/slice_workflow_jobs/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobTemplatesJobTemplatesSliceWorkflowJobsCreate(id, data, callback)</td>
    <td style="padding:15px">Create a Workflow Job for a Job Template</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/job_templates/{pathv1}/slice_workflow_jobs/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobTemplatesJobTemplatesSurveySpecListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">POST requests to this resource should include the full specification for a Job Template's Survey</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/job_templates/{pathv1}/survey_spec/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobTemplatesJobTemplatesSurveySpecCreate(id, data, callback)</td>
    <td style="padding:15px">POST requests to this resource should include the full specification for a Job Template's Survey</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/job_templates/{pathv1}/survey_spec/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobTemplatesJobTemplatesSurveySpecDelete(id, search, callback)</td>
    <td style="padding:15px">POST requests to this resource should include the full specification for a Job Template's Survey</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/job_templates/{pathv1}/survey_spec/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobsJobsListWithOptions(page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Jobs</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/jobs/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobsJobsRead0(id, search, callback)</td>
    <td style="padding:15px">Retrieve a Job Host Summary</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/jobs/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobsJobsDelete(id, search, callback)</td>
    <td style="padding:15px">Delete a Job</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/jobs/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobsJobsActivityStreamListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Activity Streams for a Job</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/jobs/{pathv1}/activity_stream/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobsJobsCancelRead(id, search, callback)</td>
    <td style="padding:15px">Determine if a Job can be cancelled</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/jobs/{pathv1}/cancel/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobsJobsCancelCreate(id, callback)</td>
    <td style="padding:15px">Cancel a Job</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/jobs/{pathv1}/cancel/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobsJobsCreateScheduleRead(id, search, callback)</td>
    <td style="padding:15px">Create a schedule based on a job</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/jobs/{pathv1}/create_schedule/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobsJobsCreateScheduleCreate(id, callback)</td>
    <td style="padding:15px">Create a schedule based on a job</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/jobs/{pathv1}/create_schedule/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobsJobsCredentialsListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Credentials for a Job</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/jobs/{pathv1}/credentials/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobsJobsExtraCredentialsListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Credentials for a Job</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/jobs/{pathv1}/extra_credentials/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobsJobsJobEventsListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Job Events for a Job</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/jobs/{pathv1}/job_events/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobsJobsJobHostSummariesListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Job Host Summaries for a Job</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/jobs/{pathv1}/job_host_summaries/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobsJobsLabelsListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Labels for a Job</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/jobs/{pathv1}/labels/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobsJobsNotificationsListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Notifications for a Job</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/jobs/{pathv1}/notifications/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobsJobsRelaunchRead(id, search, callback)</td>
    <td style="padding:15px">Relaunch a Job</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/jobs/{pathv1}/relaunch/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobsJobsRelaunchCreate(id, data, callback)</td>
    <td style="padding:15px">Relaunch a Job</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/jobs/{pathv1}/relaunch/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobsJobsStdoutRead(id, callback)</td>
    <td style="padding:15px">Retrieve Job Stdout</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/jobs/{pathv1}/stdout/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">labelsLabelsListWithOptions(page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Labels</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/labels/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">labelsLabelsCreate(data, callback)</td>
    <td style="padding:15px">Create a Label</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/labels/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">labelsLabelsRead(id, search, callback)</td>
    <td style="padding:15px">Retrieve a Label</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/labels/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">labelsLabelsUpdate(id, search, data, callback)</td>
    <td style="padding:15px">Update a Label</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/labels/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">labelsLabelsPartialUpdate(id, search, data, callback)</td>
    <td style="padding:15px">Update a Label</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/labels/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersMeListWithOptions(page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">Retrieve Information about the current User</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/me/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersUsersListWithOptions(page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Users</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/users/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersUsersCreate(data, callback)</td>
    <td style="padding:15px">Create a User</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/users/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersUsersRead(id, search, callback)</td>
    <td style="padding:15px">Retrieve a User</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/users/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersUsersUpdate(id, search, data, callback)</td>
    <td style="padding:15px">Update a User</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/users/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersUsersDelete(id, search, callback)</td>
    <td style="padding:15px">Delete a User</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/users/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersUsersPartialUpdate(id, search, data, callback)</td>
    <td style="padding:15px">Update a User</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/users/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersUsersAccessListListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Users</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/users/{pathv1}/access_list/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersUsersActivityStreamListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Activity Streams for a User</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/users/{pathv1}/activity_stream/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersUsersAdminOfOrganizationsListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Organizations Administered by this User</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/users/{pathv1}/admin_of_organizations/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersUsersApplicationsListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Applications</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/users/{pathv1}/applications/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersUsersApplicationsCreate(id, data, callback)</td>
    <td style="padding:15px">Create an Application</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/users/{pathv1}/applications/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersUsersAuthorizedTokensListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Access Tokens for a User</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/users/{pathv1}/authorized_tokens/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersUsersAuthorizedTokensCreate(id, data, callback)</td>
    <td style="padding:15px">Create an Access Token for a User</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/users/{pathv1}/authorized_tokens/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersUsersCredentialsListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Credentials for a User</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/users/{pathv1}/credentials/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersUsersCredentialsCreate(id, data, callback)</td>
    <td style="padding:15px">Create a Credential for a User</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/users/{pathv1}/credentials/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersUsersOrganizationsListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Organizations for a User</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/users/{pathv1}/organizations/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersUsersPersonalTokensListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Access Tokens for a User</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/users/{pathv1}/personal_tokens/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersUsersPersonalTokensCreate(id, data, callback)</td>
    <td style="padding:15px">Create an Access Token for a User</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/users/{pathv1}/personal_tokens/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersUsersProjectsListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Projects for a User</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/users/{pathv1}/projects/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersUsersRolesListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Roles for a User</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/users/{pathv1}/roles/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersUsersRolesCreate(id, data, callback)</td>
    <td style="padding:15px">List Roles for a User</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/users/{pathv1}/roles/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersUsersTeamsListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Teams for a User</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/users/{pathv1}/teams/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersUsersTokensListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Access Tokens for a User</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/users/{pathv1}/tokens/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersUsersTokensCreate(id, data, callback)</td>
    <td style="padding:15px">Create an Access Token for a User</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/users/{pathv1}/tokens/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">metricsMetricsList(callback)</td>
    <td style="padding:15px">Show Metrics Details</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/metrics/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">notificationTemplatesNotificationTemplatesListWithOptions(page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Notification Templates</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/notification_templates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">notificationTemplatesNotificationTemplatesCreate(data, callback)</td>
    <td style="padding:15px">Create a Notification Template</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/notification_templates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">notificationTemplatesNotificationTemplatesRead(id, search, callback)</td>
    <td style="padding:15px">Retrieve a Notification Template</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/notification_templates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">notificationTemplatesNotificationTemplatesUpdate(id, search, data, callback)</td>
    <td style="padding:15px">Update a Notification Template</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/notification_templates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">notificationTemplatesNotificationTemplatesDelete(id, search, callback)</td>
    <td style="padding:15px">Delete a Notification Template</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/notification_templates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">notificationTemplatesNotificationTemplatesPartialUpdate(id, search, data, callback)</td>
    <td style="padding:15px">Update a Notification Template</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/notification_templates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">notificationTemplatesNotificationTemplatesCopyListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">Notification Templates_notification_templates_copy_list</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/notification_templates/{pathv1}/copy/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">notificationTemplatesNotificationTemplatesCopyCreate(id, data, callback)</td>
    <td style="padding:15px">Notification Templates_notification_templates_copy_create</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/notification_templates/{pathv1}/copy/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">notificationTemplatesNotificationTemplatesNotificationsListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Notifications for a Notification Template</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/notification_templates/{pathv1}/notifications/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">notificationTemplatesNotificationTemplatesTestCreate(id, callback)</td>
    <td style="padding:15px">Test a Notification Template</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/notification_templates/{pathv1}/test/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">notificationsNotificationsListWithOptions(page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Notifications</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/notifications/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">notificationsNotificationsRead(id, search, callback)</td>
    <td style="padding:15px">Retrieve a Notification</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/notifications/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">organizationsOrganizationsListWithOptions(page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Organizations</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/organizations/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">organizationsOrganizationsCreate(data, callback)</td>
    <td style="padding:15px">Create an Organization</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/organizations/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">organizationsOrganizationsRead(id, search, callback)</td>
    <td style="padding:15px">Retrieve an Organization</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/organizations/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">organizationsOrganizationsUpdate(id, search, data, callback)</td>
    <td style="padding:15px">Update an Organization</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/organizations/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">organizationsOrganizationsDelete(id, search, callback)</td>
    <td style="padding:15px">Delete an Organization</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/organizations/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">organizationsOrganizationsPartialUpdate(id, search, data, callback)</td>
    <td style="padding:15px">Update an Organization</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/organizations/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">organizationsOrganizationsAccessListListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Users</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/organizations/{pathv1}/access_list/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">organizationsOrganizationsActivityStreamListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Activity Streams for an Organization</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/organizations/{pathv1}/activity_stream/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">organizationsOrganizationsAdminsListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Admin Users for an Organization</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/organizations/{pathv1}/admins/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">organizationsOrganizationsAdminsCreate(id, data, callback)</td>
    <td style="padding:15px">Create an Admin User for an Organization</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/organizations/{pathv1}/admins/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">organizationsOrganizationsApplicationsListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Applications for an Organization</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/organizations/{pathv1}/applications/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">organizationsOrganizationsApplicationsCreate(id, data, callback)</td>
    <td style="padding:15px">Create an Application for an Organization</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/organizations/{pathv1}/applications/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">organizationsOrganizationsCredentialsListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Credentials for an Organization</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/organizations/{pathv1}/credentials/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">organizationsOrganizationsCredentialsCreate(id, data, callback)</td>
    <td style="padding:15px">Create a Credential for an Organization</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/organizations/{pathv1}/credentials/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">organizationsOrganizationsInstanceGroupsListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Instance Groups for an Organization</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/organizations/{pathv1}/instance_groups/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">organizationsOrganizationsInstanceGroupsCreate(id, data, callback)</td>
    <td style="padding:15px">Create an Instance Group for an Organization</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/organizations/{pathv1}/instance_groups/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">organizationsOrganizationsInventoriesListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Inventories for an Organization</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/organizations/{pathv1}/inventories/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">organizationsOrganizationsNotificationTemplatesListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Notification Templates for an Organization</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/organizations/{pathv1}/notification_templates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">organizationsOrganizationsNotificationTemplatesCreate(id, data, callback)</td>
    <td style="padding:15px">Create a Notification Template for an Organization</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/organizations/{pathv1}/notification_templates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">organizationsOrganizationsNotificationTemplatesErrorListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Notification Templates for an Organization</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/organizations/{pathv1}/notification_templates_error/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">organizationsOrganizationsNotificationTemplatesErrorCreate(id, data, callback)</td>
    <td style="padding:15px">Create a Notification Template for an Organization</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/organizations/{pathv1}/notification_templates_error/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">organizationsOrganizationsNotificationTemplatesStartedListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Notification Templates for an Organization</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/organizations/{pathv1}/notification_templates_started/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">organizationsOrganizationsNotificationTemplatesStartedCreate(id, data, callback)</td>
    <td style="padding:15px">Create a Notification Template for an Organization</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/organizations/{pathv1}/notification_templates_started/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">organizationsOrganizationsNotificationTemplatesSuccessListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Notification Templates for an Organization</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/organizations/{pathv1}/notification_templates_success/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">organizationsOrganizationsNotificationTemplatesSuccessCreate(id, data, callback)</td>
    <td style="padding:15px">Create a Notification Template for an Organization</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/organizations/{pathv1}/notification_templates_success/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">organizationsOrganizationsObjectRolesListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Roles for an Organization</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/organizations/{pathv1}/object_roles/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">organizationsOrganizationsProjectsListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Projects for an Organization</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/organizations/{pathv1}/projects/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">organizationsOrganizationsProjectsCreate(id, data, callback)</td>
    <td style="padding:15px">Create a Project for an Organization</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/organizations/{pathv1}/projects/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">organizationsOrganizationsTeamsListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Teams for an Organization</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/organizations/{pathv1}/teams/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">organizationsOrganizationsTeamsCreate(id, data, callback)</td>
    <td style="padding:15px">Create a Team for an Organization</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/organizations/{pathv1}/teams/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">organizationsOrganizationsUsersListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Users for an Organization</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/organizations/{pathv1}/users/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">organizationsOrganizationsUsersCreate(id, data, callback)</td>
    <td style="padding:15px">Create a User for an Organization</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/organizations/{pathv1}/users/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">organizationsOrganizationsWorkflowJobTemplatesListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Workflow Job Templates for an Organization</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/organizations/{pathv1}/workflow_job_templates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">organizationsOrganizationsWorkflowJobTemplatesCreate(id, data, callback)</td>
    <td style="padding:15px">Create a Workflow Job Template for an Organization</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/organizations/{pathv1}/workflow_job_templates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">projectUpdatesProjectUpdatesListWithOptions(page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Project Updates</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/project_updates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">projectUpdatesProjectUpdatesRead(id, search, callback)</td>
    <td style="padding:15px">Retrieve a Project Update</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/project_updates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">projectUpdatesProjectUpdatesDelete(id, search, callback)</td>
    <td style="padding:15px">Delete a Project Update</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/project_updates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">projectUpdatesProjectUpdatesCancelRead(id, search, callback)</td>
    <td style="padding:15px">Cancel Project Update</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/project_updates/{pathv1}/cancel/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">projectUpdatesProjectUpdatesCancelCreate(id, callback)</td>
    <td style="padding:15px">Cancel Project Update</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/project_updates/{pathv1}/cancel/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">projectUpdatesProjectUpdatesEventsListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Project Update Events for a Project Update</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/project_updates/{pathv1}/events/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">projectUpdatesProjectUpdatesNotificationsListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Notifications for a Project Update</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/project_updates/{pathv1}/notifications/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">projectUpdatesProjectUpdatesScmInventoryUpdatesListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Inventory Updates for a Project Update</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/project_updates/{pathv1}/scm_inventory_updates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">projectUpdatesProjectUpdatesStdoutRead(id, callback)</td>
    <td style="padding:15px">Retrieve Project Update Stdout</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/project_updates/{pathv1}/stdout/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">projectsProjectsListWithOptions(page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Projects</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/projects/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">projectsProjectsCreate(data, callback)</td>
    <td style="padding:15px">Create a Project</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/projects/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">projectsProjectsRead(id, search, callback)</td>
    <td style="padding:15px">Retrieve a Project</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/projects/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">projectsProjectsUpdate0(id, search, data, callback)</td>
    <td style="padding:15px">Update a Project</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/projects/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">projectsProjectsDelete(id, search, callback)</td>
    <td style="padding:15px">Delete a Project</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/projects/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">projectsProjectsPartialUpdate(id, search, data, callback)</td>
    <td style="padding:15px">Update a Project</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/projects/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">projectsProjectsAccessListListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Users</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/projects/{pathv1}/access_list/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">projectsProjectsActivityStreamListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Activity Streams for a Project</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/projects/{pathv1}/activity_stream/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">projectsProjectsCopyListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">Projects_projects_copy_list</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/projects/{pathv1}/copy/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">projectsProjectsCopyCreate(id, data, callback)</td>
    <td style="padding:15px">Projects_projects_copy_create</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/projects/{pathv1}/copy/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">projectsProjectsInventoriesRead(id, search, callback)</td>
    <td style="padding:15px">Retrieve a Project</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/projects/{pathv1}/inventories/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">projectsProjectsNotificationTemplatesErrorListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Notification Templates for a Project</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/projects/{pathv1}/notification_templates_error/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">projectsProjectsNotificationTemplatesErrorCreate(id, data, callback)</td>
    <td style="padding:15px">Create a Notification Template for a Project</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/projects/{pathv1}/notification_templates_error/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">projectsProjectsNotificationTemplatesStartedListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Notification Templates for a Project</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/projects/{pathv1}/notification_templates_started/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">projectsProjectsNotificationTemplatesStartedCreate(id, data, callback)</td>
    <td style="padding:15px">Create a Notification Template for a Project</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/projects/{pathv1}/notification_templates_started/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">projectsProjectsNotificationTemplatesSuccessListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Notification Templates for a Project</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/projects/{pathv1}/notification_templates_success/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">projectsProjectsNotificationTemplatesSuccessCreate(id, data, callback)</td>
    <td style="padding:15px">Create a Notification Template for a Project</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/projects/{pathv1}/notification_templates_success/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">projectsProjectsObjectRolesListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Roles for a Project</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/projects/{pathv1}/object_roles/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">projectsProjectsPlaybooksRead(id, search, callback)</td>
    <td style="padding:15px">Retrieve Project Playbooks</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/projects/{pathv1}/playbooks/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">projectsProjectsProjectUpdatesListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Project Updates for a Project</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/projects/{pathv1}/project_updates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">projectsProjectsSchedulesListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Schedules for a Project</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/projects/{pathv1}/schedules/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">projectsProjectsSchedulesCreate(id, data, callback)</td>
    <td style="padding:15px">Create a Schedule for a Project</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/projects/{pathv1}/schedules/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">projectsProjectsScmInventorySourcesListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Inventory Sources for a Project</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/projects/{pathv1}/scm_inventory_sources/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">projectsProjectsTeamsListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Teams</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/projects/{pathv1}/teams/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">projectsProjectsUpdateRead(id, search, callback)</td>
    <td style="padding:15px">Update Project</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/projects/{pathv1}/update/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">projectsProjectsUpdateCreate(id, callback)</td>
    <td style="padding:15px">Update Project</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/projects/{pathv1}/update/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rolesRolesListWithOptions(page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Roles</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/roles/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rolesRolesRead(id, search, callback)</td>
    <td style="padding:15px">Retrieve a Role</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/roles/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rolesRolesChildrenListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Roles for a Role</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/roles/{pathv1}/children/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rolesRolesParentsListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Roles for a Role</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/roles/{pathv1}/parents/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rolesRolesTeamsListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Teams for a Role</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/roles/{pathv1}/teams/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rolesRolesTeamsCreate(id, data, callback)</td>
    <td style="padding:15px">Create a Team for a Role</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/roles/{pathv1}/teams/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rolesRolesUsersListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Users for a Role</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/roles/{pathv1}/users/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rolesRolesUsersCreate(id, data, callback)</td>
    <td style="padding:15px">Create a User for a Role</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/roles/{pathv1}/users/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">schedulesSchedulesListWithOptions(page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Schedules</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/schedules/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">schedulesSchedulesCreate(data, callback)</td>
    <td style="padding:15px">Schedule Details</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/schedules/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">schedulesSchedulesPreviewCreate(data, callback)</td>
    <td style="padding:15px">Schedules_schedules_preview_create</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/schedules/preview/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">schedulesSchedulesZoneinfoList(callback)</td>
    <td style="padding:15px">Schedules_schedules_zoneinfo_list</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/schedules/zoneinfo/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">schedulesSchedulesRead(id, search, callback)</td>
    <td style="padding:15px">Retrieve a Schedule</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/schedules/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">schedulesSchedulesUpdate(id, search, data, callback)</td>
    <td style="padding:15px">Update a Schedule</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/schedules/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">schedulesSchedulesDelete(id, search, callback)</td>
    <td style="padding:15px">Delete a Schedule</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/schedules/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">schedulesSchedulesPartialUpdate(id, search, data, callback)</td>
    <td style="padding:15px">Update a Schedule</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/schedules/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">schedulesSchedulesCredentialsListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Credentials for a Schedule</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/schedules/{pathv1}/credentials/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">schedulesSchedulesCredentialsCreate(id, data, callback)</td>
    <td style="padding:15px">Create a Credential for a Schedule</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/schedules/{pathv1}/credentials/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">schedulesSchedulesJobsListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Unified Jobs for a Schedule</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/schedules/{pathv1}/jobs/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">settingsSettingsList(page, pageSize, callback)</td>
    <td style="padding:15px">List Settings</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/settings/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">settingsSettingsLoggingTestCreate(data, callback)</td>
    <td style="padding:15px">Test Logging Configuration</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/settings/logging/test/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">settingsSettingsRead(categorySlug, callback)</td>
    <td style="padding:15px">Retrieve a Setting</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/settings/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">settingsSettingsUpdate(categorySlug, data, callback)</td>
    <td style="padding:15px">Update a Setting</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/settings/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">settingsSettingsDelete(categorySlug, callback)</td>
    <td style="padding:15px">Delete a Setting</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/settings/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">settingsSettingsPartialUpdate(categorySlug, data, callback)</td>
    <td style="padding:15px">Update a Setting</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/settings/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">systemJobTemplatesSystemJobTemplatesListWithOptions(page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List System Job Templates</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/system_job_templates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">systemJobTemplatesSystemJobTemplatesRead(id, search, callback)</td>
    <td style="padding:15px">Retrieve a System Job Template</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/system_job_templates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">systemJobTemplatesSystemJobTemplatesJobsListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List System Jobs for a System Job Template</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/system_job_templates/{pathv1}/jobs/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">systemJobTemplatesSystemJobTemplatesLaunchListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">Launch a Job Template</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/system_job_templates/{pathv1}/launch/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">systemJobTemplatesSystemJobTemplatesLaunchCreate(id, callback)</td>
    <td style="padding:15px">Launch a Job Template</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/system_job_templates/{pathv1}/launch/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">systemJobTemplatesSystemJobTemplatesNotificationTemplatesErrorListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Notification Templates for a System Job Template</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/system_job_templates/{pathv1}/notification_templates_error/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">systemJobTemplatesSystemJobTemplatesNotificationTemplatesErrorCreate(id, data, callback)</td>
    <td style="padding:15px">Create a Notification Template for a System Job Template</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/system_job_templates/{pathv1}/notification_templates_error/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">systemJobTemplatesSystemJobTemplatesNotificationTemplatesStartedListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Notification Templates for a System Job Template</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/system_job_templates/{pathv1}/notification_templates_started/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">systemJobTemplatesSystemJobTemplatesNotificationTemplatesStartedCreate(id, data, callback)</td>
    <td style="padding:15px">Create a Notification Template for a System Job Template</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/system_job_templates/{pathv1}/notification_templates_started/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">systemJobTemplatesSystemJobTemplatesNotificationTemplatesSuccessListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Notification Templates for a System Job Template</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/system_job_templates/{pathv1}/notification_templates_success/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">systemJobTemplatesSystemJobTemplatesNotificationTemplatesSuccessCreate(id, data, callback)</td>
    <td style="padding:15px">Create a Notification Template for a System Job Template</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/system_job_templates/{pathv1}/notification_templates_success/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">systemJobTemplatesSystemJobTemplatesSchedulesListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Schedules for a System Job Template</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/system_job_templates/{pathv1}/schedules/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">systemJobTemplatesSystemJobTemplatesSchedulesCreate(id, data, callback)</td>
    <td style="padding:15px">Create a Schedule for a System Job Template</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/system_job_templates/{pathv1}/schedules/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">systemJobsSystemJobsListWithOptions(page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List System Jobs</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/system_jobs/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">systemJobsSystemJobsCreate(data, callback)</td>
    <td style="padding:15px">Create a System Job</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/system_jobs/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">systemJobsSystemJobsRead(id, search, callback)</td>
    <td style="padding:15px">Retrieve a System Job</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/system_jobs/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">systemJobsSystemJobsDelete(id, search, callback)</td>
    <td style="padding:15px">Delete a System Job</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/system_jobs/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">systemJobsSystemJobsCancelRead(id, search, callback)</td>
    <td style="padding:15px">Retrieve a System Job</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/system_jobs/{pathv1}/cancel/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">systemJobsSystemJobsCancelCreate(id, callback)</td>
    <td style="padding:15px">Retrieve a System Job</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/system_jobs/{pathv1}/cancel/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">systemJobsSystemJobsEventsListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List System Job Events for a System Job</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/system_jobs/{pathv1}/events/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">systemJobsSystemJobsNotificationsListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Notifications for a System Job</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/system_jobs/{pathv1}/notifications/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">teamsTeamsListWithOptions(page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Teams</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/teams/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">teamsTeamsCreate(data, callback)</td>
    <td style="padding:15px">Create a Team</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/teams/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">teamsTeamsRead(id, search, callback)</td>
    <td style="padding:15px">Retrieve a Team</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/teams/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">teamsTeamsUpdate(id, search, data, callback)</td>
    <td style="padding:15px">Update a Team</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/teams/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">teamsTeamsDelete(id, search, callback)</td>
    <td style="padding:15px">Delete a Team</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/teams/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">teamsTeamsPartialUpdate(id, search, data, callback)</td>
    <td style="padding:15px">Update a Team</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/teams/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">teamsTeamsAccessListListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Users</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/teams/{pathv1}/access_list/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">teamsTeamsActivityStreamListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Activity Streams for a Team</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/teams/{pathv1}/activity_stream/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">teamsTeamsCredentialsListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Credentials for a Team</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/teams/{pathv1}/credentials/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">teamsTeamsCredentialsCreate(id, data, callback)</td>
    <td style="padding:15px">Create a Credential for a Team</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/teams/{pathv1}/credentials/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">teamsTeamsObjectRolesListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Roles for a Team</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/teams/{pathv1}/object_roles/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">teamsTeamsProjectsListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Projects for a Team</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/teams/{pathv1}/projects/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">teamsTeamsRolesListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Roles for a Team</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/teams/{pathv1}/roles/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">teamsTeamsRolesCreate(id, data, callback)</td>
    <td style="padding:15px">List Roles for a Team</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/teams/{pathv1}/roles/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">teamsTeamsUsersListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Users for a Team</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/teams/{pathv1}/users/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">teamsTeamsUsersCreate(id, data, callback)</td>
    <td style="padding:15px">Create a User for a Team</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/teams/{pathv1}/users/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unifiedJobTemplatesUnifiedJobTemplatesListWithOptions(page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Unified Job Templates</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/unified_job_templates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unifiedJobsUnifiedJobsListWithOptions(page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Unified Jobs</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/unified_jobs/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">workflowJobNodesWorkflowJobNodesListWithOptions(page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Workflow Job Nodes</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/workflow_job_nodes/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">workflowJobNodesWorkflowJobNodesRead(id, search, callback)</td>
    <td style="padding:15px">Retrieve a Workflow Job Node</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/workflow_job_nodes/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">workflowJobNodesWorkflowJobNodesAlwaysNodesListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Workflow Job Nodes for a Workflow Job Node</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/workflow_job_nodes/{pathv1}/always_nodes/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">workflowJobNodesWorkflowJobNodesCredentialsListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Credentials for a Workflow Job Node</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/workflow_job_nodes/{pathv1}/credentials/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">workflowJobNodesWorkflowJobNodesFailureNodesListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Workflow Job Nodes for a Workflow Job Node</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/workflow_job_nodes/{pathv1}/failure_nodes/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">workflowJobNodesWorkflowJobNodesSuccessNodesListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Workflow Job Nodes for a Workflow Job Node</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/workflow_job_nodes/{pathv1}/success_nodes/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">workflowJobTemplateNodesWorkflowJobTemplateNodesListWithOptions(page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Workflow Job Template Nodes</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/workflow_job_template_nodes/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">workflowJobTemplateNodesWorkflowJobTemplateNodesCreate(data, callback)</td>
    <td style="padding:15px">Create a Workflow Job Template Node</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/workflow_job_template_nodes/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">workflowJobTemplateNodesWorkflowJobTemplateNodesRead(id, search, callback)</td>
    <td style="padding:15px">Retrieve a Workflow Job Template Node</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/workflow_job_template_nodes/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">workflowJobTemplateNodesWorkflowJobTemplateNodesUpdate(id, search, data, callback)</td>
    <td style="padding:15px">Update a Workflow Job Template Node</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/workflow_job_template_nodes/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">workflowJobTemplateNodesWorkflowJobTemplateNodesDelete(id, search, callback)</td>
    <td style="padding:15px">Delete a Workflow Job Template Node</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/workflow_job_template_nodes/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">workflowJobTemplateNodesWorkflowJobTemplateNodesPartialUpdate(id, search, data, callback)</td>
    <td style="padding:15px">Update a Workflow Job Template Node</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/workflow_job_template_nodes/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">workflowJobTemplateNodesWorkflowJobTemplateNodesAlwaysNodesListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Workflow Job Template Nodes for a Workflow Job Template Node</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/workflow_job_template_nodes/{pathv1}/always_nodes/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">workflowJobTemplateNodesWorkflowJobTemplateNodesAlwaysNodesCreate(id, data, callback)</td>
    <td style="padding:15px">Create a Workflow Job Template Node for a Workflow Job Template Node</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/workflow_job_template_nodes/{pathv1}/always_nodes/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">workflowJobTemplateNodesWorkflowJobTemplateNodesCredentialsListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Credentials for a Workflow Job Template Node</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/workflow_job_template_nodes/{pathv1}/credentials/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">workflowJobTemplateNodesWorkflowJobTemplateNodesCredentialsCreate(id, data, callback)</td>
    <td style="padding:15px">Create a Credential for a Workflow Job Template Node</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/workflow_job_template_nodes/{pathv1}/credentials/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">workflowJobTemplateNodesWorkflowJobTemplateNodesFailureNodesListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Workflow Job Template Nodes for a Workflow Job Template Node</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/workflow_job_template_nodes/{pathv1}/failure_nodes/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">workflowJobTemplateNodesWorkflowJobTemplateNodesFailureNodesCreate(id, data, callback)</td>
    <td style="padding:15px">Create a Workflow Job Template Node for a Workflow Job Template Node</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/workflow_job_template_nodes/{pathv1}/failure_nodes/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">workflowJobTemplateNodesWorkflowJobTemplateNodesSuccessNodesListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Workflow Job Template Nodes for a Workflow Job Template Node</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/workflow_job_template_nodes/{pathv1}/success_nodes/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">workflowJobTemplateNodesWorkflowJobTemplateNodesSuccessNodesCreate(id, data, callback)</td>
    <td style="padding:15px">Create a Workflow Job Template Node for a Workflow Job Template Node</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/workflow_job_template_nodes/{pathv1}/success_nodes/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">workflowJobTemplatesWorkflowJobTemplatesListWithOptions(page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Workflow Job Templates</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/workflow_job_templates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">workflowJobTemplatesWorkflowJobTemplatesCreate(data, callback)</td>
    <td style="padding:15px">Create a Workflow Job Template</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/workflow_job_templates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">workflowJobTemplatesWorkflowJobTemplatesRead(id, search, callback)</td>
    <td style="padding:15px">Retrieve a Workflow Job Template</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/workflow_job_templates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">workflowJobTemplatesWorkflowJobTemplatesUpdate(id, search, data, callback)</td>
    <td style="padding:15px">Update a Workflow Job Template</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/workflow_job_templates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">workflowJobTemplatesWorkflowJobTemplatesDelete(id, search, callback)</td>
    <td style="padding:15px">Delete a Workflow Job Template</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/workflow_job_templates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">workflowJobTemplatesWorkflowJobTemplatesPartialUpdate(id, search, data, callback)</td>
    <td style="padding:15px">Update a Workflow Job Template</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/workflow_job_templates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">workflowJobTemplatesWorkflowJobTemplatesAccessListListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Users</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/workflow_job_templates/{pathv1}/access_list/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">workflowJobTemplatesWorkflowJobTemplatesActivityStreamListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Activity Streams for a Workflow Job Template</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/workflow_job_templates/{pathv1}/activity_stream/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">workflowJobTemplatesWorkflowJobTemplatesCopyListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">Copy a Workflow Job Template</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/workflow_job_templates/{pathv1}/copy/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">workflowJobTemplatesWorkflowJobTemplatesCopyCreate(id, data, callback)</td>
    <td style="padding:15px">Copy a Workflow Job Template</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/workflow_job_templates/{pathv1}/copy/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">workflowJobTemplatesWorkflowJobTemplatesLabelsListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Labels for a Workflow Job Template</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/workflow_job_templates/{pathv1}/labels/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">workflowJobTemplatesWorkflowJobTemplatesLabelsCreate(id, data, callback)</td>
    <td style="padding:15px">Create a Label for a Workflow Job Template</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/workflow_job_templates/{pathv1}/labels/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">workflowJobTemplatesWorkflowJobTemplatesLaunchRead(id, search, callback)</td>
    <td style="padding:15px">Launch a Workflow Job Template</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/workflow_job_templates/{pathv1}/launch/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">workflowJobTemplatesWorkflowJobTemplatesLaunchCreate(id, data, callback)</td>
    <td style="padding:15px">Launch a Workflow Job Template</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/workflow_job_templates/{pathv1}/launch/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">workflowJobTemplatesWorkflowJobTemplatesNotificationTemplatesErrorListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Notification Templates for a Workflow Job Template</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/workflow_job_templates/{pathv1}/notification_templates_error/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">workflowJobTemplatesWorkflowJobTemplatesNotificationTemplatesErrorCreate(id, data, callback)</td>
    <td style="padding:15px">Create a Notification Template for a Workflow Job Template</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/workflow_job_templates/{pathv1}/notification_templates_error/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">workflowJobTemplatesWorkflowJobTemplatesNotificationTemplatesStartedListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Notification Templates for a Workflow Job Template</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/workflow_job_templates/{pathv1}/notification_templates_started/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">workflowJobTemplatesWorkflowJobTemplatesNotificationTemplatesStartedCreate(id, data, callback)</td>
    <td style="padding:15px">Create a Notification Template for a Workflow Job Template</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/workflow_job_templates/{pathv1}/notification_templates_started/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">workflowJobTemplatesWorkflowJobTemplatesNotificationTemplatesSuccessListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Notification Templates for a Workflow Job Template</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/workflow_job_templates/{pathv1}/notification_templates_success/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">workflowJobTemplatesWorkflowJobTemplatesNotificationTemplatesSuccessCreate(id, data, callback)</td>
    <td style="padding:15px">Create a Notification Template for a Workflow Job Template</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/workflow_job_templates/{pathv1}/notification_templates_success/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">workflowJobTemplatesWorkflowJobTemplatesObjectRolesListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Roles for a Workflow Job Template</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/workflow_job_templates/{pathv1}/object_roles/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">workflowJobTemplatesWorkflowJobTemplatesSchedulesListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Schedules for a Workflow Job Template</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/workflow_job_templates/{pathv1}/schedules/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">workflowJobTemplatesWorkflowJobTemplatesSchedulesCreate(id, data, callback)</td>
    <td style="padding:15px">Create a Schedule for a Workflow Job Template</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/workflow_job_templates/{pathv1}/schedules/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">workflowJobTemplatesWorkflowJobTemplatesSurveySpecListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">POST requests to this resource should include the full specification for a Workflow Job Template's Survey</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/workflow_job_templates/{pathv1}/survey_spec/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">workflowJobTemplatesWorkflowJobTemplatesSurveySpecCreate(id, data, callback)</td>
    <td style="padding:15px">POST requests to this resource should include the full specification for a Workflow Job Template's Survey</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/workflow_job_templates/{pathv1}/survey_spec/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">workflowJobTemplatesWorkflowJobTemplatesSurveySpecDelete(id, search, callback)</td>
    <td style="padding:15px">POST requests to this resource should include the full specification for a Workflow Job Template's Survey</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/workflow_job_templates/{pathv1}/survey_spec/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">workflowJobTemplatesWorkflowJobTemplatesWorkflowJobsListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Workflow Jobs for a Workflow Job Template</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/workflow_job_templates/{pathv1}/workflow_jobs/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">workflowJobTemplatesWorkflowJobTemplatesWorkflowNodesListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">Workflow Job Template Workflow Node List</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/workflow_job_templates/{pathv1}/workflow_nodes/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">workflowJobTemplatesWorkflowJobTemplatesWorkflowNodesCreate(id, data, callback)</td>
    <td style="padding:15px">Workflow Job Template Workflow Node List</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/workflow_job_templates/{pathv1}/workflow_nodes/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">workflowJobsWorkflowJobsListWithOptions(page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Workflow Jobs</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/workflow_jobs/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">workflowJobsWorkflowJobsCreate(data, callback)</td>
    <td style="padding:15px">Create a Workflow Job</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/workflow_jobs/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">workflowJobsWorkflowJobsRead(id, search, callback)</td>
    <td style="padding:15px">Retrieve a Workflow Job</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/workflow_jobs/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">workflowJobsWorkflowJobsDelete(id, search, callback)</td>
    <td style="padding:15px">Delete a Workflow Job</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/workflow_jobs/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">workflowJobsWorkflowJobsActivityStreamListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Activity Streams for a Workflow Job</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/workflow_jobs/{pathv1}/activity_stream/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">workflowJobsWorkflowJobsCancelRead(id, search, callback)</td>
    <td style="padding:15px">Cancel Workflow Job</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/workflow_jobs/{pathv1}/cancel/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">workflowJobsWorkflowJobsCancelCreate(id, callback)</td>
    <td style="padding:15px">Cancel Workflow Job</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/workflow_jobs/{pathv1}/cancel/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">workflowJobsWorkflowJobsLabelsListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Labels for a Workflow Job</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/workflow_jobs/{pathv1}/labels/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">workflowJobsWorkflowJobsNotificationsListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Notifications for a Workflow Job</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/workflow_jobs/{pathv1}/notifications/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">workflowJobsWorkflowJobsRelaunchListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">Relaunch a workflow job</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/workflow_jobs/{pathv1}/relaunch/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">workflowJobsWorkflowJobsRelaunchCreate(id, callback)</td>
    <td style="padding:15px">Relaunch a workflow job</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/workflow_jobs/{pathv1}/relaunch/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">workflowJobsWorkflowJobsWorkflowNodesListWithOptions(id, page, pageSize, search, iapMetadata, callback)</td>
    <td style="padding:15px">List Workflow Job Nodes for a Workflow Job</td>
    <td style="padding:15px">{base_path}/{version}/api/v2/workflow_jobs/{pathv1}/workflow_nodes/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
